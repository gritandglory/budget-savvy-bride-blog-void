<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);
    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}, 100);

// Update CSS within in Admin

//add_action('admin_enqueue_scripts', function () {
//    wp_enqueue_style('admin-styles', asset_path('styles/admin/admin.css'));
//}, 100);

/**
 * Remove gutenberg block library css
 */
//add_action('wp_enqueue_scripts', function () {
//   wp_dequeue_style('wp-block-library');
//}, 100);

/**
 * Remove jquery
 */

add_action('wp_enqueue_scripts', function () {
//    wp_deregister_script('jquery');
});

/**
 * Theme setup
 */
add_action(
    'after_setup_theme',
    function () {
        /**
         * Enable features from Soil when plugin is activated
         * @link https://roots.io/plugins/soil/
         */
        add_theme_support('soil-clean-up');
        add_theme_support('soil-jquery-cdn');
        add_theme_support('soil-nav-walker');
        add_theme_support('soil-nice-search');
        add_theme_support('soil-relative-urls');
    //        add_theme_support('editor-styles');
        add_editor_style(asset_path('styles/editor.css'));

        /**
         * Enable plugins to manage the document title
         * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
         */
        add_theme_support('title-tag');

        /**
         * Register navigation menus
         * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
         */
        register_nav_menus([
            'primary_navigation' => __('Primary Navigation', 'sage')
        ]);

        /**
         * Enable post-single thumbnails
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
    //        add_theme_support('post-single-thumbnails');

        add_theme_support('post-thumbnails');
        add_image_size('sm-square', 200, 200, array( 'center', 'center' ));
        add_image_size('md-square', 300, 300, array( 'center', 'center' ));

        /**
         * Enable HTML5 markup support
         * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
         */
        add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

        /**
         * Enable selective refresh for widgets in customizer
         * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
         */
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Use main stylesheet for visual editor
         * @see resources/assets/styles/layouts/_tinymce.scss
         */
        add_editor_style(asset_path('styles/main.css'));
    },
    20
);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '<span class="border-b border-gray-200 flex my-6"></span> </section>',
        'before_title' => '<h3 class="font-medium">',
        'after_title' => '</h3>'
    ];
    register_sidebar([
            'name' => __('Primary', 'sage'),
            'id' => 'sidebar-primary'
        ] + $config);
    register_sidebar([
            'name' => __('Footer', 'sage'),
            'id' => 'sidebar-footer'
        ] + $config);
});

/**
 * Updates the `$post-single` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post-single', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

/**
 * Initialize ACF Builder
 * First check to see if ACF class exists, ie has been activated.
 */

include_once(ABSPATH . 'wp-admin/includes/plugin.php');
//plugin is activated
if (is_plugin_active('advanced-custom-fields/acf.php')) {
    add_action('init', function () {
        collect(glob(config('theme.dir') . '/app/Fields/*.php'))->map(function ($field) {
            return require_once($field);
        })->map(function ($field) {
            if ($field instanceof FieldsBuilder) {
                acf_add_local_field_group($field->build());
            }
        });
    });
}
