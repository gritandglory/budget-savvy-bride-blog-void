<?php
function test_pagination($pages = '', $range = 4)
{
    $showitems = ($range * 2) + 1;
    global $paged;

    if (empty($paged)) {
        $paged = 1;
    }

    if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }

    $link_classes = 'flex justify-center items-center border-2 rounded w-8 h-8 text-base font-medium mx-2 ';
    $inactive_classes = 'border-gray-200';
    $active_classes = 'border-coral-500 bg-coral-100 pointer-events-none';

    if (1 != $pages) {
        echo "<div class='flex justify-center items-center'>";

        // Shows which page you're out of the total number of pages
//        echo "<span>Page ".$paged." of ".$pages."</span>";

        echo "<div class=\"previous-page\">";
        // Shows link to First page of posts, disabled if you're already on the first page
        if ($paged < 2) {
            echo "<span class='disabled'>&laquo; First</span>";
        }
        if ($paged > 1) {
            echo "<a href='" . get_pagenum_link(1) . "'>&laquo; First</a>";
        }

        // Shows link to Previous page of posts, disabled if you're already on the first page
        if ($paged < 2) {
            echo "<span class='disabled'>&lsaquo; Previous</span>";
        }
        if ($paged > 1 && $showitems < $pages) {
            echo "<a href='" . get_pagenum_link($paged - 1) . "'>&lsaquo; Previous</a>";
        }
        echo "</div>"; // End previous page

        echo "<div class='flex justify-center items-center'>";
        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                echo ($paged == $i) ? "<span class='$link_classes $active_classes'>" . $i . "</span>" : "<a href='" . get_pagenum_link($i) . "' class='$link_classes $inactive_classes'>" . $i . "</a>";
            }
        }
        echo "</div>"; // End numbered

        echo "<div class=\"next-page\">";
        if ($paged < $pages && $showitems < $pages) {
            echo "<a href=\"" . get_pagenum_link($paged + 1) . "\">Next &rsaquo;</a>";
        }
        if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages) {
            echo "<a href='" . get_pagenum_link($pages) . "'>Last &raquo;</a>";
        }
        echo "</div>"; // End next page

        echo "</div>\n"; // End pagination
    }
}

function pagination()
{
    $next_text = '<div class="ml-2 bg-coral-50 flex h-6 w-6 items-center justify-center rounded-full transition duration-300 ease-in-out hover:bg-coral-100">
                    <svg class="h-5 w-5 text-coral-500" fill="currentColor" viewBox="0 0 20 20">
                          <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0
                          011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd">
                        </path>
                    </svg>
                    </div>';

    $prev_text = '<div class="mr-2 bg-coral-50 flex h-6 w-6 items-center justify-center rounded-full w-6 transition duration-300 ease-in-out hover:bg-coral-100">
                        <svg class="h-5 w-5 text-coral-500"  fill="currentColor" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414
                        1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd">
                        </path>
                    </svg>
                 </div>';

    $link_classes = 'flex justify-center items-center h-8 w-8 border-2 rounded';
    $inactive_classes = 'border-gray-200';
    $active_classes = 'border-coral-500 bg-coral-100 pointer-events-none';

    global $wp_query;
    $big = 999999999; // This needs to be an unlikely integer
    // For more options and info view the docs for paginate_links()
    // http://codex.wordpress.org/Function_Reference/paginate_links
    $paginate_links = paginate_links(array(
        'base' => str_replace($big, '%#%', html_entity_decode(get_pagenum_link($big))),
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'mid_size' => 5,
        'prev_next' => true,
        'prev_text' => __($prev_text, 'bsb'),
        'next_text' => __($next_text, 'bsb'),
        'type' => 'list',
    ));

    // set list style
    $paginate_links = str_replace("<ul class='page-numbers'>", "<ul class=' flex items-center justify-center'>", $paginate_links);

    $paginate_links = str_replace('<li>', '<li class="mx-1 font-medium text-base">', $paginate_links);
    // set each of the list links with the style
    $paginate_links = str_replace('class="page-numbers"', 'class="flex justify-center items-center h-8 w-8 rounded  transition duration-300 ease-in-out hover:bg-coral-100"', $paginate_links);
    $paginate_links = str_replace('class="page-numbers current"', "class='flex justify-center items-center h-8 w-8 border-2 rounded border-coral-200 pointer-events-none'", $paginate_links);

//    $paginate_links = str_replace('</span>', '</a>', $paginate_links);
//    $paginate_links = str_replace("<li><a href='#'>&hellip;</a></li>", "<li><span class='dots'>&hellip;</span></li>", $paginate_links);
//    $paginate_links = preg_replace('/\s*page-numbers/', '', $paginate_links);

    // Display the pagination if more than one page is found.
    if ($paginate_links) {
        echo $paginate_links;
    }
}
