<?php

if (! function_exists('add_contact_fields')) : // Check if the function add_custom_fields does not exist
    function add_contact_fields($fields)
    {
        // Add contact fields to WordPress
        $fields['pinterest'] = 'Pinterest Username';
        $fields['googleplus'] = 'Google+ Profile URL';
        $fields['instagram'] = 'Instagram Username';
        $fields['twitter'] = 'Twitter Username';
        $fields['facebook_url'] = 'Facebook Username';
        $fields['blog_lovin'] = 'Blog Lovin URL';
        $fields['youtube'] = 'YouTube Channel';
        $fields['rss_feed'] = 'RSS Feed URL';
        $fields['blog_lovin'] = 'Blog Lovin URL';

        return $fields;
    }
// Apply the add_contact_fields function to user_contactmethods

    add_filter('user_contactmethods', 'add_contact_fields');

endif;
