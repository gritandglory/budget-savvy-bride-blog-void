<?php
function allowed_block_types($allowed_block_types, $post)
{

    return array(

        // Common
        'core/paragraph',
        'core/heading',
        'core/image',
        'core/quote',
        'core/list',

        // Formatting
        'core/html',

        'bsb-blocks/editorial',
        'bsb-blocks/pull-quote',
        'bsb-blocks/image-two-column',
        'bsb-blocks/list-grid',
        'bsb-blocks/list-grid-slider',

        'bsb-blocks/accordion-block',
        'bsb-blocks/how-to-block',
        'bsb-blocks/image-grid-block',
        'bsb-blocks/product-item',
        'bsb-blocks/product-wrapper',

        'schema-blocks/how-to'

    );
}

//add_filter('allowed_block_types', 'allowed_block_types', 10, 2);

function disable_gutenberg_colour_settings()
{
    add_theme_support('disable-custom-colors');
    add_theme_support('editor-color-palette');
}
add_action('after_setup_theme', 'disable_gutenberg_colour_settings');


function disable_gutenberg_font_settings()
{
    add_theme_support('disable-custom-font-sizes');
    add_theme_support('editor-font-sizes', array());
    add_theme_support('disable-drop-cap');
}
add_action('after_setup_theme', 'disable_gutenberg_font_settings');


function custom_admin_css()
{
    echo '<style type="text/css">
/* Main column width */
.wp-block {
    max-width: 42rem;
}

/* Width of "wide" blocks */
     .wp-block[data-align="wide"] {
     max-width: 1080px;
}

/* Width of "full-wide" blocks */
.wp-block[data-align="full"] {
    max-width: none;
}
</style>';
}

add_action('admin_head', 'custom_admin_css');
