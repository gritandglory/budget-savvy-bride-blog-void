<?php

/**
 * Reorder the columns in the real wedding state taxonomy
 *
 * @param $columns
 * @return array
 */
function deal_category_reorder_column($columns)
{

    $columns = array(
        'cb' => $columns['cb'],
        'name' => __('Name'),
        'description' => __('Description'),
        'slug' => __('Slug'),
        'featured' => __('Featured'),
        'posts' => __('Count'),
    );

    return $columns;

}

add_filter('manage_edit-deal-category_columns', 'deal_category_reorder_column');



/**
 * Add the new fields to the real wedding season columns
 *
 * @param $value
 * @param $column_name
 * @param $term_id
 * @return string
 */
function featured_column_to_deal_taxonomy($value, $column_name, $term_id)
{
    $issue = get_term($term_id, 'deal-category');

    switch ($column_name) {
        case 'featured':
            $value = get_field('is_featured_home', $issue) ? 'Featured' : '';
            break;
        default:
            break;
    }
    return $value;
}

add_filter('manage_deal-category_custom_column', 'featured_column_to_deal_taxonomy', 10, 3);

//
