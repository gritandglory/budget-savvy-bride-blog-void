<?php

/**
 * All functions associate with the real wedding post type
 * @param $query
 */
function real_wedding_posts_per_page($query)
{
    if (is_admin() || !$query->is_main_query()) {
        return;
    }

    if (is_post_type_archive('real_wedding')) {
        $query->set('posts_per_page', 16);
    }
}

add_filter('pre_get_posts', 'real_wedding_posts_per_page');

/**
 * Reorder the columns in the real wedding color taxonomy
 *
 * @param $columns
 * @return array
 */
function real_wedding_color_reorder_column($columns)
{

    $columns = array(
        'cb' => $columns['cb'],
        'name' => __('Name'),
        'description' => __('Description'),
        'slug' => __('Slug'),
        'order' => __('Order'),
        'hex_color' => __('Hex'),
        'posts' => __('Count'),
    );

    return $columns;

}

add_filter('manage_edit-real_wedding_color_columns', 'real_wedding_color_reorder_column');

/**
 * Add the new fields to the real wedding color columns
 *
 * @param $value
 * @param $column_name
 * @param $term_id
 * @return string
 */
function real_wedding_color_add_column($value, $column_name, $term_id)
{
    $issue = get_term($term_id, 'real_wedding_color');

    $hex = get_field('hex_color', $issue);

    switch ($column_name) {
        case 'hex_color':
            $value = '<span style="
            display: flex;
            color: #fff;
            padding: 8px 4px;
            border-radius: 0.25rem;
            text-align: center;
            align-items: center;
            justify-content: center;
             background-color:' . $hex . '"' .
                '> ' . $hex . ' </span>';
            break;
        case 'order':
            $value = get_field('order', $issue);
            break;
        default:
            break;
    }
    return $value;
}

add_filter('manage_real_wedding_color_custom_column', 'real_wedding_color_add_column', 10, 3);



/**
 * Reorder the columns in the real wedding budget taxonomy
 *
 * @param $columns
 * @return array
 */
function real_wedding_budget_reorder_column($columns)
{

    $columns = array(
        'cb' => $columns['cb'],
        'name' => __('Name'),
        'description' => __('Description'),
        'slug' => __('Slug'),
        'order' => __('Order'),
        'posts' => __('Count'),
    );

    return $columns;

}

add_filter('manage_edit-real_wedding_budget_columns', 'real_wedding_budget_reorder_column');

/**
 * Add the new fields to the real wedding budget columns
 *
 * @param $value
 * @param $column_name
 * @param $term_id
 * @return string
 */
function real_wedding_budget_add_column($value, $column_name, $term_id)
{
    $issue = get_term($term_id, 'real_wedding_budget');


    switch ($column_name) {
        case 'order':
            $value = get_field('order', $issue);
            break;
        default:
            break;
    }
    return $value;
}

add_filter('manage_real_wedding_budget_custom_column', 'real_wedding_budget_add_column', 10, 3);

/**
 * Reorder the columns in the real wedding season taxonomy
 *
 * @param $columns
 * @return array
 */

function real_wedding_season_reorder_column($columns)
{

    $columns = array(
        'cb' => $columns['cb'],
        'name' => __('Name'),
        'description' => __('Description'),
        'slug' => __('Slug'),
        'order' => __('Order'),
        'posts' => __('Count'),
    );

    return $columns;

}

add_filter('manage_edit-real_wedding_season_columns', 'real_wedding_season_reorder_column');

/**
 * Add the new fields to the real wedding season columns
 *
 * @param $value
 * @param $column_name
 * @param $term_id
 * @return string
 */
function real_wedding_season_add_column($value, $column_name, $term_id)
{
    $issue = get_term($term_id, 'real_wedding_season');

    switch ($column_name) {
        case 'order':
            $value = get_field('order', $issue);
            break;
        default:
            break;
    }
    return $value;
}

add_filter('manage_real_wedding_season_custom_column', 'real_wedding_season_add_column', 10, 3);


/**
 * Reorder the columns in the real wedding state taxonomy
 *
 * @param $columns
 * @return array
 */
function real_wedding_state_reorder_column($columns)
{

    $columns = array(
        'cb' => $columns['cb'],
        'name' => __('Name'),
        'description' => __('Description'),
        'slug' => __('Slug'),
        'abbreviation' => __('Abbreviation'),
        'posts' => __('Count'),
    );

    return $columns;

}

add_filter('manage_edit-real_wedding_state_columns', 'real_wedding_state_reorder_column');


/**
 * Add the new fields to the real wedding state columns
 *
 * @param $value
 * @param $column_name
 * @param $term_id
 * @return string
 */
function real_wedding_state_add_column($value, $column_name, $term_id)
{
    $issue = get_term($term_id, 'real_wedding_state');

    switch ($column_name) {
        case 'abbreviation':
            $value = get_field('abbreviation', $issue);
            break;
        default:
            break;
    }
    return $value;
}

add_filter('manage_real_wedding_state_custom_column', 'real_wedding_state_add_column', 10, 3);
