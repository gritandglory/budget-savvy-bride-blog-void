<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class RealWeddingArchive extends Controller
{

    protected $acf = true;

    protected $template = 'archive-real_wedding';

    /**
     * Real wedding subtitle, on archive page
     * @return mixed
     */
    public function optionTitle()
    {
        $option = get_option('home_box_option');
        return $option['rw_home_title'];
    }

    /**
     * Real wedding subtitle, on archive page
     * @return mixed
     */
    public function optionSubtitle()
    {
        $option = get_option('home_box_option');
        return $option['rw_home_subtitle'];
    }

    /**
     * Real wedding seo content, on archive page
     * @return mixed
     */
    public function optionContent()
    {
        $option = get_option('home_box_option');
        return $option['rw_home_seo_content'];
    }

    /**
     * Return the selected terms from each option
     *
     * @return array
     */
    public function termDescriptions()
    {
        $request = $_GET;

        $array = [];

        if (isset($request['real_wedding_budget']) && $request['real_wedding_budget'] !== '') {
            $array['budget'] = array(
                'slug' => '$'. ucfirst($request['real_wedding_budget']),
                'description' => $this->getTermDescription(
                    $request['real_wedding_budget'],
                    'real_wedding_budget'
                )
            );
        }
        if (isset($request['real_wedding_color']) && $request['real_wedding_color'] !== '') {
            $array['color'] = array(
                'slug' => ucfirst($request['real_wedding_color']),
                'description' => $this->getTermDescription(
                    $request['real_wedding_color'],
                    'real_wedding_color'
                )
            );
        }
        if (isset($request['real_wedding_state']) && $request['real_wedding_state'] !== '') {
            $array['state'] = array(
                'slug' => ucfirst($request['real_wedding_state']),
                'description' => $this->getTermDescription(
                    $request['real_wedding_state'],
                    'real_wedding_state'
                )
            );
        }
        if (isset($request['real_wedding_season']) && $request['real_wedding_season'] !== '') {
            $array['season'] = array(
                'slug' => ucfirst($request['real_wedding_season']),
                'description' => $this->getTermDescription(
                    $request['real_wedding_season'],
                    'real_wedding_season'
                )
            );
        }
        return $array;
    }

    /**
     * Return the taxonomy term description
     * @param $value
     * @param $taxonomy
     * @return mixed
     */
    protected function getTermDescription($value, $taxonomy)
    {
        $term = get_term_by('slug', $value, $taxonomy);

        return $term->description;
    }

    /**
     * @return array
     */
    public static function hasBudget()
    {

        $terms = get_the_terms(get_the_ID(), 'real_wedding_budget');

        if ($terms) {
            $array = array();
            foreach ($terms as $term) {
                $array[] = array(
                    'name' => $term->name,
                );
            }
            return $array;
        }
    }

    /**
     * @return array
     */
    public static function hasColor()
    {
        $terms = get_the_terms(get_the_ID(), 'real_wedding_color');

        if ($terms) {
            $array = array();
            foreach ($terms as $term) {
                $array[] = array(
                    'slug' => $term->slug,
                    'hex' => get_field('hex_color', $term->taxonomy . '_' . $term->term_id)
                );
            }
            return $array;
        }
    }

    /**
     *
     * @return array
     */
    public static function hasState()
    {

        $terms = get_the_terms(get_the_ID(), 'real_wedding_state');

        if ($terms) {
            $array = array();
            foreach ($terms as $term) {
                $array[] = array(
                    'name' => $term->name,
                    'slug' => $term->slug,
                );
            }
            return $array;
        }
    }

    public static function hasSeason()
    {

        $terms = get_the_terms(get_the_ID(), 'real_wedding_season');

        if ($terms) {
            $array = array();
            foreach ($terms as $term) {
                $array[] = array(
                    'name' => $term->name,
                    'slug' => $term->slug,
                );
            }
            return $array;
        }
    }

    public static function title()
    {
        return get_post()->post_title;
    }

    public static function permalink()
    {
        return the_permalink();
    }

    public static function id()
    {
        return 'wp-image-' . get_post_thumbnail_id(get_the_ID());
    }

    public static function src()
    {
        return wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'thumbnail', true);
    }

    public static function alt()
    {
        return get_post_meta(get_post_thumbnail_id(get_the_ID()), '_wp_attachment_image_alt', true);
    }

    public static function srcset()
    {
        return wp_get_attachment_image_srcset(get_post_thumbnail_id(get_the_ID()), 'thumbnail');
    }
}
