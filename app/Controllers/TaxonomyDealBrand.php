<?php


namespace App\Controllers;

use App\Controllers\Partials\DateFormatted;
use App\Controllers\Partials\Deal\DealArrayFormat;
use App\Controllers\Partials\Deal\TaxonomyDeal;
use App\Controllers\Partials\Deal\TaxonomyDealLoop;
use App\Controllers\Partials\TaxonomyQuery;
use Sober\Controller\Controller;

class TaxonomyDealBrand extends Controller
{
    use DateFormatted, TaxonomyQuery, TaxonomyDeal, DealArrayFormat, TaxonomyDealLoop;

}
