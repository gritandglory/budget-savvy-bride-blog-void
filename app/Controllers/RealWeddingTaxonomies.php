<?php


namespace App\Controllers;

use Sober\Controller\Controller;

class RealWeddingTaxonomies extends Controller
{
    public static function budgetTerms()
    {
        $terms = get_terms(array(
            'taxonomy' => 'real_wedding_budget',
            'hide_empty' => false,
        ));

        $array = array();

        foreach ($terms as $term) {
            $array[] = array(
                'name' => $term->name,
                'slug' => $term->slug,
                'order' => get_field('order', $term->taxonomy . '_' . $term->term_id)
            );
        }

        // reorder the array based upon order field
        usort($array, function ($a, $b) {
            return strnatcmp($a['order'], $b['order']);
        });

        return $array;
    }

    public static function colorTerms()
    {
        $terms = get_terms(array(
            'taxonomy' => 'real_wedding_color',
            'hide_empty' => false,
        ));

        $array = array();

        foreach ($terms as $term) {
            $array[] = array(
                'name' => $term->name,
                'slug' => $term->slug,
                'hex' => get_field('hex_color', $term->taxonomy . '_' . $term->term_id),
                'order' => get_field('order', $term->taxonomy . '_' . $term->term_id)
            );
        }

        // reorder the array based upon order field
        usort($array, function ($a, $b) {
            return strnatcmp($a['order'], $b['order']);
        });

        return $array;
    }

    public static function stateTerms()
    {
        $terms = get_terms(array(
            'taxonomy' => 'real_wedding_state',
            'hide_empty' => false,
            'orderby' => 'slug',

        ));

        $array = array();

        foreach ($terms as $term) {
            $array[] = array(
                'name' => $term->name,
                'slug' => $term->slug,
            );
        }

        return $array;
    }

    public static function seasonTerms()
    {
        $terms = get_terms(array(
            'taxonomy' => 'real_wedding_season',
            'hide_empty' => false,
        ));

        $array = array();

        foreach ($terms as $term) {
            $order = get_field('order', $term->taxonomy . '_' . $term->term_id);
            $array[] = array(
                'name' => $term->name,
                'slug' => $term->slug,
                'order' => $order
            );
        }

        // reorder the array based upon order field
        usort($array, function ($a, $b) {
            return strnatcmp($a['order'], $b['order']);
        });

        return $array;
    }
}
