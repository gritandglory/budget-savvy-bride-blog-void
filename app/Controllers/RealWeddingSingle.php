<?php

namespace App\Controllers;

use App\Controllers\Partials\Author;
use App\Controllers\Partials\Excerpt;
use App\Controllers\Partials\Meta;
use App\Controllers\Partials\ShareIcons;
use App\Controllers\Partials\Sponsored;
use App\Controllers\Partials\Title;
use DateTime;
use Sober\Controller\Controller;

class RealWeddingSingle extends Controller
{
    use ShareIcons, Sponsored, Title, Author, Meta, Excerpt;

    protected $acf = true;

    protected $template = 'single-real_wedding';

    /**
     * @return string
     */
    public function thumbnailCreditName()
    {
        return trim(get_post_meta(get_post_thumbnail_id(), 'photographer_name', true));
    }

    /**
     * @return string
     */
    public function thumbnailCreditUrl()
    {
        return trim(get_post_meta(get_post_thumbnail_id(), 'photographer_url', true));
    }

    /**
     * @return mixed
     */
    public function coupleNameFormatted()
    {
        return get_field('couple_name');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function weddingDateFormatted()
    {
        // the false is removing any formatting.
        $date = get_field('wedding_date', false, false);
        // need to convert the format
        $date = new DateTime($date);

        return $date->format('F j, Y');
    }

    /**
     * @return mixed
     */
    public function totalBudget()
    {
        return '$' . number_format(get_field('total_budget'));
    }

    /**
     * @return mixed
     */
    public function locationCity()
    {
        return get_field('wedding_location_city');
    }

    /**
     * @return mixed
     */
    public function locationState()
    {
        return get_field('wedding_location_state');
    }


    public function catering()
    {
        return $this->setVendor('catering');
    }

    public function alcoholBar()
    {
        return $this->setVendor('alcohol_bar');
    }

    public function bakery()
    {
        return $this->setVendor('bakery');
    }

    public function flowers()
    {
        return $this->setVendor('flowers');
    }

    public function rentalDecor()
    {
        return $this->setVendor('rental_decor');
    }

    public function planner()
    {
        return $this->setVendor('planner');
    }

    public function musicEntertainment()
    {
        return $this->setVendor('music_entertainment');
    }

    public function photographer()
    {
        return $this->setVendor('photographer');
    }

    public function weddingDress()
    {
        return $this->setVendor('wedding_dress');
    }

    public function stationery()
    {
        return $this->setVendor('stationery');
    }

    public function hairMakeup()
    {
        return $this->setVendor('hair_makeup');
    }

    public function venueOnly()
    {

        return $this->setVendor('venue');
    }

    public function ceremonyVenue()
    {
        return $this->setVendor('ceremony_venue');
    }

    public function receptionVenue()
    {
        return $this->setVendor('reception_venue');
    }

    public function officiant()
    {
        return $this->setVendor('officiant');
    }

    public function transportation()
    {
        return $this->setVendor('transportation');
    }

    public function videography()
    {
        return $this->setVendor('videography');
    }

    public function miscellaneous()
    {
        return $this->setVendor('miscellaneous');
    }

    public function setVendors()
    {
        $fields = array(
            ['name' => 'alcohol_bar', 'title' => 'Alcohol / Bar'],
            ['name' => 'bakery', 'title' => 'Bakery'],
            ['name' => 'beauty', 'title' => 'Beauty'],
            ['name' => 'catering', 'title' => 'Catering'],

            ['name' => 'flowers', 'title' => 'Flowers'],
            ['name' => 'groom_attire', 'title' => 'Groom Attire'],
            ['name' => 'hair', 'title' => 'Hair'],
            ['name' => 'makeup', 'title' => 'Makeup'],
            ['name' => 'music_entertainment', 'title' => 'Music & Entertainment'],
            ['name' => 'officiant', 'title' => 'Officiant'],


            ['name' => 'photographer', 'title' => 'Photographer'],
            ['name' => 'planner', 'title' => 'Planner'],

            ['name' => 'stationery', 'title' => 'Stationery'],


            ['name' => 'rental_decor', 'title' => 'Rentals & Decor'],
            ['name' => 'ceremony_venue', 'title' => 'Ceremony venue'],
            ['name' => 'reception_venue', 'title' => 'Reception venue'],
            ['name' => 'venue', 'title' => 'Venue'],
            ['name' => 'transportation', 'title' => 'Transportation'],

            ['name' => 'videography', 'title' => 'Videography'],
            ['name' => 'wedding_dress', 'title' => 'Wedding dress'],
            ['name' => 'miscellaneous', 'title' => 'Miscellaneous'],
        );

        $array = [];
        $count = 0;
        foreach ($fields as $field) {
            $data = $this->setVendor($field['name']);


            if (isset($data)) {
                if ($data['amount_spent'] > 0) {
                    $count++;
                }

                $array['items'][] = array(
                    'group_name' => ucwords(str_replace("_", " / ", $field['name'])),
                    'title' => $field['title'],
                    'name' => $data['name'],
                    'url' => $data['url'],
                    'directory_url' => $data['directory_url'],
                    'amount_spent' => $data['amount_spent'] ? '$' . number_format($data['amount_spent']) : 0,
                    'percentage_budget' => (float)round($this->getPercentage($data['amount_spent']))
                );
            }
            $array['count'] = $count;
        }


        return $array;
    }

    /**
     * This function is searching all the real weddings fields from app/Fields/real-wedding-fields.php
     *
     * first it checks to see if the has_* is true, this is the 'addTrueFalse'.
     * If this is true then it retrieves the data from the *_group
     * @param $var
     * @return void
     */
    protected function setVendor($var)
    {

        if (get_field('has_' . $var)) {
            return get_field($var . '_group');
        }
        return;
    }

    protected function getPercentage($val)
    {
        if ($val) {
            $total_budget = get_field('total_budget');

            return ($val * 100) / $total_budget;
        }
    }
}
