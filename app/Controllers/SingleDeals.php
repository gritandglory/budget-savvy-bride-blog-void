<?php

namespace App\Controllers;

use App\Controllers\Partials\DateFormatted;
use App\Controllers\Partials\Deal\DealArrayFormat;
use App\Controllers\Partials\Deal\DealBreadcrumb;
use App\Controllers\Partials\Deal\TaxonomyDeal;
use App\Controllers\Partials\ShareIcons;
use Sober\Controller\Controller;

class SingleDeals extends Controller
{

    use ShareIcons, DealArrayFormat, TaxonomyDeal,  DateFormatted, DealBreadcrumb;

    protected $acf = true;

    public function deal()
    {
        return $this->objectFormat(get_post());
    }
}

