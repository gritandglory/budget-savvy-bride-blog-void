<?php

namespace App\Controllers;

use App\Controllers\Partials\DateFormatted;
use App\Controllers\Partials\Deal\DealArrayFormat;
use App\Controllers\Partials\Deal\TaxonomyDeal;
use App\Controllers\Partials\ShareIcons;
use DateTime;
use Decimal\Decimal;
use Sober\Controller\Controller;

class ArchiveDeals extends Controller
{
    use ShareIcons, DealArrayFormat, TaxonomyDeal, DateFormatted;

    protected $acf = true;

    /**
     * @return array[]
     */
    public function topDealLoop()
    {
        $args = array('post_type' => 'deals', 'posts_per_page' => 4,
            'tax_query' => array(
                array(
                    'taxonomy' => 'deal-spotlight',
                    'field' => 'slug',
                    'terms' => 'top-wedding-deals'
                ))
        );

        return $this->arrayFormat(get_posts($args));
    }

    /**
     * @return array[]
     */
    public function topDealPermalink()
    {
        $term = get_term_by('slug', 'top-wedding-deals', 'deal-spotlight');

        return get_term_link($term);
    }

    /**
     * @return array[]
     */
    public function categoryLoop()
    {

        $taxonomies = get_object_taxonomies(array('post_type' => 'deals'));

        foreach ($taxonomies as $taxonomy) {
            $terms = get_terms(['taxonomy' => $taxonomy, 'hide_empty' => true]);

            return array_map(function ($category) {
                return [
                    'id' => $category->ID,
                    'name' => wp_specialchars_decode($category->name),
                    'slug' => $category->slug
                ];
            }, $terms);
        }
    }

    /**
     * @return array[]
     */
    public function exclusiveLoop()
    {
        $args = array(
            'post_type' => 'deals',
            'posts_per_page' => 4,
            'meta_key' => 'exclusive',
            'meta_value' => true,
        );

        return $this->arrayFormat(get_posts($args));
    }


    /**
     * @return array[]
     */
    public function exclusivePermalink()
    {
        $term = get_term_by('slug', 'exclusive-wedding-deals', 'deal-spotlight');

        return get_term_link($term);
    }

    /**
     * @return array[]
     */
    public function mostRecentLoop()
    {
        $args = array('post_type' => 'deals', 'posts_per_page' => 4);

        return $this->arrayFormat(get_posts($args));
    }

    public function featuredLoop()
    {

        $terms = get_terms(array(
            'taxonomy' => 'deal-category',
            'hide_empty' => false,
        ));


        $featured = [];

        foreach ($terms as $term) {
            if (get_field('is_featured_home', $term)) {
                $featured[] = array(
                    'id' => $term->term_id,
                    'name' => $term->name
                );
            }
        }



        foreach ($featured as $feature) {
            $args = array('post_type' => 'deals', 'posts_per_page' => 4,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'deal-category',
                        'field' => 'id',
                        'terms' => $feature['id']
                    ))
            );

            $array[] = array(
                'id' => $feature['id'],
                'name' =>  wp_specialchars_decode($feature['name']),
                'permalink' =>  get_term_link($feature['id']),
                'posts' => $this->arrayFormat(get_posts($args)),
            );

        }

        return $array;

    }
}
