<?php


namespace App\Controllers;

use App\Controllers\Partials\RealWeddingTaxonomies;
use Sober\Controller\Controller;

class RealWeddingPage extends Controller
{
    protected $acf = true;

    public static function subTitle()
    {
        return get_field('subtitle');
    }
}
