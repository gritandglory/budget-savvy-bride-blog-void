<?php


namespace App\Controllers\Partials;


trait TaxonomyQuery
{
    /**
     * @return mixed
     */
    protected function taxonomy()
    {
        return get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
    }

    /**
     * @return mixed
     */
    public function taxonomyName()
    {
        $taxonomy = $this->taxonomy();

        return wp_specialchars_decode(  $taxonomy->name);
    }

}
