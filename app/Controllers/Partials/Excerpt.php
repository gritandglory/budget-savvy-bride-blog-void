<?php

namespace App\Controllers\Partials;

trait Excerpt
{
    /**
     * @return mixed
     */
    public function excerpt()
    {
        return get_the_excerpt();
    }
}
