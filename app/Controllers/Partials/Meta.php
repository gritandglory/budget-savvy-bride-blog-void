<?php


namespace App\Controllers\Partials;

trait Meta
{
    /**
     * @return false|string
     */
    public function published()
    {
        return get_the_date('F j, Y');
    }

    /**
     * @return false|string
     */
    public function modified()
    {
        $published = strtotime(get_the_date());
        $updated = strtotime(get_the_modified_date());

        if ($published < $updated) {
            return get_the_modified_date('F j, Y');
        }
        return false;

    }
}
