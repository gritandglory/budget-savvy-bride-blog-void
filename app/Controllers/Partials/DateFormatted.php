<?php


namespace App\Controllers\Partials;


use DateTime;

trait DateFormatted
{
    /**
     * @param $field
     * @return string
     * @throws \Exception
     */
    protected function formattedDate($field)
    {
//        dd($field);
        if (strlen($field) > 0 && $field != ' ') {
            $date = new DateTime($field);
            return $date->format('F, j Y');
        }
    }
}
