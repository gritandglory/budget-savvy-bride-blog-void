<?php


namespace App\Controllers\Partials;

trait Title
{

    /**
     * @return mixed
     */
    public function title()
    {
        return get_post()->post_title;
    }
}
