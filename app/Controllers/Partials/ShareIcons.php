<?php


namespace App\Controllers\Partials;

trait ShareIcons
{


    protected function postTitle()
    {
        return get_post()->post_title;
    }

    protected function permalink()
    {
        return urlencode(get_permalink());
    }

    /**
     * @return string
     */
    public function shareFacebook()
    {
        return 'https://www.facebook.com/sharer/sharer.php?u=' . $this->permalink();
    }

    /**
     * @return string
     */
    public function shareTwitter()
    {
        return 'https://twitter.com/intent/tweet?text=' . $this->postTitle() . '&amp;url=' . $this->permalink();
    }

    /**
     * @return string
     */
    public function sharePinterest()
    {
        return 'https://pinterest.com/pin/create/button/?url=' . $this->permalink() ./*'&amp;media='.$postThumbnail[0].*/
        '&amp;description=' . $this->postTitle();
    }

    protected function type()
    {
        if (is_page()) {
            return 'page';
        }
        if (is_single()) {
            return 'single';
        }
    }

    protected function encode()
    {
        return htmlspecialchars(
            urlencode(
                html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')
            ),
            ENT_COMPAT,
            'UTF-8'
        );
    }
}
