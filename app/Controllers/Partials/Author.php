<?php


namespace App\Controllers\Partials;


trait Author
{
    /**
     * @return mixed
     */
    public function authorName()
    {
        return get_the_author_meta('display_name', get_post()->post_author);
    }

    /**
     * @return mixed
     */
    public function authorDescription()
    {
        return get_the_author_meta('description', get_post()->post_author);
    }

    /**
     * @return mixed
     */
    public function authorProfileImage()
    {

        return get_field('user_profile', 'user_' . get_post()->post_author);
    }

    /**
     * @return mixed
     */
    public function authorSocial()
    {

        return array(
            'pinterest'     =>      get_the_author_meta('pinterest', get_post()->post_author),
            'googleplus'    =>      get_the_author_meta('googleplus', get_post()->post_author),
            'instagram'     =>      get_the_author_meta('instagram', get_post()->post_author),
            'twitter'       =>      get_the_author_meta('twitter', get_post()->post_author),
            'facebook_url'  =>      get_the_author_meta('facebook_url', get_post()->post_author),
            'bloglovin'     =>      get_the_author_meta('blog_lovin', get_post()->post_author),
        );
    }
}
