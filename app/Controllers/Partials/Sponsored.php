<?php


namespace App\Controllers\Partials;

trait Sponsored
{
    /**
     * @return mixed
     */
    public function sponsored()
    {
        return get_field('is_sponsored_post');
    }
}
