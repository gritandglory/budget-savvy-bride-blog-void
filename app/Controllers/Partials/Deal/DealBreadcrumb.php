<?php


namespace App\Controllers\Partials\Deal;

trait DealBreadcrumb
{

    public function allCategoriesPermalink()
    {
        $term = get_term_by('slug', 'all-deals', 'deal-spotlight');

        return get_term_link($term);

    }
}
