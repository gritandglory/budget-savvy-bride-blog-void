<?php


namespace App\Controllers\Partials\Deal;


trait TaxonomyDealLoop
{

    /**
     * @return array
     */
    public function loop()
    {

        $taxonomy = $this->taxonomy();

        $args = array(
            'post_type' => 'deals',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => $taxonomy->taxonomy, // taxonomy name
                    'field' => 'term_id',
                    'terms' => $taxonomy->term_id, /// Where term_id of Term 1 is "1".
                    'include_children' => false
                )
            )
        );
        return $this->arrayFormat(get_posts($args));
    }

}
