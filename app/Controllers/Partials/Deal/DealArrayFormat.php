<?php


namespace App\Controllers\Partials\Deal;

trait DealArrayFormat
{

    /**
     * @param $deals
     * @return array[]
     */
    protected function arrayFormat($deals)
    {
        return array_map(function ($deal) {
            return $this->objectFormat($deal);
        }, $deals);
    }

    /**
     * @param $deal
     * @return array
     */
    protected function objectFormat($deal)
    {
        $brand = $this->hasBrand($deal);
        return [
            'id' => $deal->ID,
            'permalink' => get_permalink($deal->ID),
            'brand_id' => isset($brand->id) ? $brand->id : 0,
            'brand_name' => wp_specialchars_decode(isset($brand->name) ? $brand->name : ''),
            'brand_logo' => get_field('brand_logo', $brand),
            'image' => $this->imageArray($deal),
            'title' => $deal->post_title,
            'the_offer' => get_field('the_offer', $deal),
            'offer_title' => $this->convertHeading(get_field('offer_title', $deal)),
            'text' => get_field('offer_text', $deal),
            'type' => get_field('offer_type', $deal),
            'code_trimmed' => substr(get_field('offer_code', $deal), -3),
            'code' => get_field('offer_code', $deal),
            'exclusive' => get_field('exclusive', $deal),
            'url' => get_field('offer_url', $deal),
            'comment' => get_field('offer_comment', $deal),
            'expire' => $this->formattedDate(get_field('offer_expire', $deal, false)),
            'terms' => $this->getTerm($deal),
        ];
    }

    protected function convertHeading($string)
    {

        $string = str_replace('<p>', '<h2>', $string);
        $string = str_replace('</p>', '</h2>', $string);
        return $string;
    }

    protected function imageArray($deal)
    {
        $thumbnail_id = get_post_thumbnail_id($deal->ID);
        $alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
        return [
            'id' => $thumbnail_id,
            'size' => array(
                'card' => array(
                    'url' => get_the_post_thumbnail_url($deal->ID, 'medium'),
                    'alt' => $alt
                ),
                'large' => array(
                    'url' => get_the_post_thumbnail_url($deal->ID, 'large'),
                    'alt' => $alt
                )
            )
        ];
    }

    /**
     * @param $deal
     * @return mixed
     */
    protected function hasBrand($deal)
    {
        $terms = get_the_terms($deal->ID, 'deal-brand');

        if ($terms && !is_wp_error($terms)) {
            return get_term($terms[0], 'deal-brand');
        }
    }

    protected function getTerm($deal)
    {

        $terms = get_the_terms($deal, 'deal-category');
        $array = [];
        foreach ($terms as $term) {

            $array[] = array(
                'name' => wp_specialchars_decode($term->name),
                'slug' => $term->slug
            );

        }
        return $array;
    }
}
