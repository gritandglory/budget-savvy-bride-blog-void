<?php


namespace App\Controllers\Partials\Deal;

trait TaxonomyDeal
{
    /**
     * @return array
     */
    public function categories()
    {

        $terms = get_terms(['taxonomy' => 'deal-category', 'hide_empty' => true]);
        $array = [];
        foreach ($terms as $term) {
                $array[] = array(
                    'id' => $term->ID,
                    'name' => wp_specialchars_decode($term->name),
                    'permalink' => get_term_link($term),
                );
        }
        return $array;
    }

    /**
     * @return array
     */
    public function brands()
    {
        $brands = get_terms(['taxonomy' => 'deal-brand', 'hide_empty' => true]);
        $array = [];
        foreach ($brands as $brand) {
            $array[] = array(
                'name' => wp_specialchars_decode($brand->name),
                'permalink' => get_term_link($brand),
            );
        }
        return $array;
    }
}
