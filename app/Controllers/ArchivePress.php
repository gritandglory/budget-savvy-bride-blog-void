<?php

namespace App\Controllers;

use DateTime;
use Sober\Controller\Controller;

class ArchivePress extends Controller
{

    protected $acf = true;

    public function pressLoop()
    {
        $press_items = get_posts([
            'post_type' => 'press',
            'numberposts'       => -1,
        ]);

        return array_map(function ($post) {
            return [
                'id' => $post->ID,
                'title' => $post->post_title,
                'thumbnail' => get_the_post_thumbnail($post->ID, 'large'),
                'press_image_url' => get_field('press_image_url', $post),
                'press_link_text' => get_field('press_link_text', $post),
                'date' => $this->formattedDate(get_field('press_date', $post)),

            ];
        }, $press_items);
    }

    protected function formattedDate($field)
    {
        $date = new DateTime($field);
        return $date->format('F, Y');
    }
}
