<?php

/**
 * Real wedding setting page.
 * These settings are used on the main real wedding archive page and for each of its taxonomies.
 */

/**
 * Register submenu if is 'administrator'
 */
if (current_user_can('administrator')) {
    add_action('admin_menu', 'real_wedding_sub_menu');
}

add_action('admin_init', 'register_options');

function real_wedding_sub_menu()
{
    add_submenu_page(
        'edit.php?post_type=real_wedding', // top level menu page
        'My Settings Page', // title of the settings page
        'Settings', // title of the submenu
        'manage_options', // capability of the user to see this page
        'real-wedding-settings', // slug of the settings page
        'render_callback' // callback function to be called when rendering the page
    );
}

/**
 * @return array[]
 */
function settings()
{
    return array(
        'setting_1_id' => array(
            'title' => 'Main Page Settings',
            'page' => 'home_box_option',
            'fields' => array(
                array(
                    'id' => 'rw_home_title',
                    'title' => 'Title',
                    'callback' => 'text_callback'
                ),
                array(
                    'id' => 'rw_home_subtitle',
                    'title' => 'Sub title',
                    'callback' => 'textarea_callback'
                ),
                array(
                    'id' => 'rw_home_seo_content',
                    'title' => 'SEO content',
                    'callback' => 'wp_editor_callback'
                ),
            )
        )
    );
}

/**
 * Loop through all of the options and register them
 */
function register_options()
{

    foreach (settings() as $id => $values) {
        add_settings_section(
            $id, // ID used to identify this section and with which to register options
            $values['title'],
            '', // Callback used to render the description of the section
            $values['page'] // Page on which to add this section of options
        );

        foreach ($values['fields'] as $field) {
            // code...
            add_settings_field(
                $field['id'],         // ID used to identify the field throughout the theme
                $field['title'],                    // The label to the left of the option interface element
                $field['callback'],
                $values['page'],         // The page on which this option will be added
                $id,          // ID of the section
                array(
                    $values['page'],        //option name
                    $field['id']        //id
                )
            );
        }
        register_setting($values['page'], $values['page']);
    }
}

/**
 * Callbacks
 */
function text_callback($args)
{
    $options = get_option($args[0]);

    $id = $args[1];
    $name = $args[0];

    $value = sanitize_text_field(isset($options['' . $args[1] . '']) ? esc_attr($options['' . $args[1] . '']) : '');

    echo '<input type="text" class="regular-text"
            id="' . $id . '"
            name="' . $name . '[' . $id . ']"
            value="' . $value . '"/>';
}

function textarea_callback($args)
{
    $options = get_option($args[0]);

    $id = $args[1];
    $name = $args[0];

    $value = sanitize_text_field(isset($options['' . $args[1] . '']) ? esc_attr($options['' . $args[1] . '']) : '');

    echo '<textarea
        rows="8"
        cols="50"
        class="large-text"
        id="' . $id . '"
        name="' . $name . '[' . $id . ']">' . $value . '
        </textarea>';
}

function wp_editor_callback($args)
{
    $options = get_option($args[0]);
    $id = $args[1]; // get the key name
    $textarea_name = $args[0] . '[' . $id . ']'; // set the array and the key 'home_box_option[rw_home_seo_content]'

    $settings = array("textarea_name" => $textarea_name,
        'wpautop' => false,
        'textarea_rows' => 15,
        'textarea_cols' => 50,
        'teeny' => true,
        'quicktags' => true,
        'media_buttons' => true
    );
    wp_editor(wpautop($options['' . $args[1] . '']), 'rw_home_seo_content', $settings);
}


/**
 * Display Page
 */
function render_callback()
{
    ?>
    <div class="wrap">
        <div id="icon-themes" class="icon32"></div>
        <h2>Real Wedding Settings</h2>
        <?php settings_errors(); ?>
        <form method="post" action="options.php">
            <?php
            settings_fields('home_box_option');
            do_settings_sections('home_box_option');
            ?>
            <?php submit_button(); ?>
        </form>
    </div>
    <?php
}




















