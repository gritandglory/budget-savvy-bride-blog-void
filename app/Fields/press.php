<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$builder = new FieldsBuilder('Press');

$builder->setLocation('post_type', '==', 'press');

$builder
    ->addImage('press_image', [
        'label' => 'Image',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'return_format' => 'array',
        'preview_size' => 'thumbnail',
        'library' => 'all',
    ]);
$builder
    ->addDatePicker('press_date')
    ->setInstructions('This is the wedding date the couple go married on.');
$builder
    ->addText('press_image_url')
    ->setInstructions('This is the wedding date the couple go married on.');
$builder
    ->addText('press_link_text')
    ->setInstructions('This is the wedding date the couple go married on.');

acf_add_local_field_group($builder->build());
