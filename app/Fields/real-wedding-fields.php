<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Add subtitle field to each of the real wedding taxonomy page templates.
 */
$builder = new FieldsBuilder('template_fields');

$builder->setLocation('page_template', '==', 'views/template-real_wedding_budget.blade.php')
    ->or('page_template', '==', 'views/template-real_wedding_color.blade.php')
    ->or('page_template', '==', 'views/template-real_wedding_season.blade.php')
    ->or('page_template', '==', 'views/template-real_wedding_state.blade.php');

$builder->addTextarea('subtitle');

acf_add_local_field_group($builder->build());

/**
 * Add abbreviation field to the state taxonomy.
 */
$builder = new FieldsBuilder('wedding_state');

$builder->setLocation('taxonomy', '==', 'real_wedding_state');

$builder->addText('abbreviation');

acf_add_local_field_group($builder->build());

/**
 * Add an extra field to the color taxonomy.
 */
$builder = new FieldsBuilder('wedding_state');

$builder->setLocation('taxonomy', '==', 'real_wedding_state');

$builder->addNumber('order');

acf_add_local_field_group($builder->build());


/**
 * Add an extra field to the color taxonomy.
 */
$builder = new FieldsBuilder('wedding_color');

$builder->setLocation('taxonomy', '==', 'real_wedding_color');
$builder
    ->addColorPicker('hex_color');

$builder
    ->addNumber('order');
acf_add_local_field_group($builder->build());


/**
 * Add an extra field to the budget taxonomy.
 */
$builder = new FieldsBuilder('wedding_budget');

$builder->setLocation('taxonomy', '==', 'real_wedding_budget');
$builder
    ->addNumber('order');
acf_add_local_field_group($builder->build());


/**
 * Add an extra field to the budget taxonomy.
 */
$builder = new FieldsBuilder('wedding_season');

$builder->setLocation('taxonomy', '==', 'real_wedding_season');
$builder
    ->addNumber('order');
acf_add_local_field_group($builder->build());

/**
 * Add couple details to the post type
 */
$builder = new FieldsBuilder('wedding_details');

$builder->setLocation('post_type', '==', 'real_wedding');
$builder
    ->addText('couple_name')
    ->setInstructions('This is the couples name.');
$builder
    ->addDatePicker('wedding_date')
    ->setInstructions('This is the wedding date the couple go married on.');
$builder
    ->addNumber('total_budget', ['prepend' => '$'])
    ->setInstructions('This is the total budget spent on the wedding.');

$builder
    ->addText('wedding_location_city')
    ->setInstructions('This is the ciy the wedding was in.');

$builder
    ->addText('wedding_location_state')
    ->setInstructions('This is the state the wedding was in.');

acf_add_local_field_group($builder->build());


/**************************************************
 * Start of fields for wedding vendors
 **************************************************/
$vendor_team = new FieldsBuilder('wedding_vendors');

$vendor_team->setLocation('post_type', '==', 'real_wedding');


/**
 * Alcohol Bar
 * Showing in single
 */
$vendor_team
    ->addTab('Alcohol Bar', ['placement' => 'left'])
    ->addTrueFalse('has_alcohol_bar', [
        'label' => 'Alcohol/Bar',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('alcohol_bar_group', ['label' => '', 'layout' => 'table'])
        ->conditional('has_alcohol_bar', '==', '1')
            ->addText('name', ['label' => 'Supplier name',])
            ->addText('url', ['label' => 'Supplier url'])
            ->addText('directory_url', ['label' => 'Directory url'])
            ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$']);

/**
 * Attire
 *
 * wedding dress - on single post
 * groom attire - on single post
 */

$vendor_team
    ->addTab('Attire', ['placement' => 'left'])
    ->addTrueFalse('has_wedding_dress', [
        'label' => 'Wedding Dress',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('wedding_dress_group', ['label' => '', 'layout' => 'table'])
    ->conditional('has_wedding_dress', '==', '1')
    ->addText('name', ['label' => 'Supplier name',])
    ->addText('url', ['label' => 'Supplier url'])
    ->addText('directory_url', ['label' => 'Directory url'])
    ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$'])
    ->endGroup()
    ->addTrueFalse('has_groom_attire', [
        'label' => 'Groom Attire',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('groom_attire_group', ['label' => '', 'layout' => 'table'])
    ->conditional('has_groom_attire', '==', '1')
    ->addText('name', ['label' => 'Supplier name',])
    ->addText('url', ['label' => 'Supplier url'])
    ->addText('directory_url', ['label' => 'Directory url'])
    ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$'])
    ->endGroup();


/**
 * Bakery
 * Showing in single
 */
$vendor_team
    ->addTab('Bakery', ['placement' => 'left'])
    ->addTrueFalse('has_bakery', [
        'label' => 'Bakery',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('bakery_group', ['label' => '', 'layout' => 'table'])
         ->conditional('has_bakery', '==', '1')
            ->addText('name', ['label' => 'Supplier name',])
            ->addText('url', ['label' => 'Supplier url'])
            ->addText('directory_url', ['label' => 'Directory url'])
            ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$']);

/**
 * Beauty
 *
 * Showing in single
 */
$vendor_team
    ->addTab('Beauty', ['placement' => 'left'])
    ->addTrueFalse('has_beauty', [
        'label' => 'Beauty',
        'instructions' => 'Use this if hair and makeup was done by the same vendor.',
        'message' => 'Show publicly?',
        'default_value' => 0,
        'ui' => 1
    ])
    ->conditional('is_beauty_split', '==', '0')
    ->addGroup('beauty_group', ['label' => '', 'layout' => 'table'])
    ->conditional('has_beauty', '==', '1')
    ->addText('name', ['label' => 'Supplier name',])
    ->addText('url', ['label' => 'Supplier url'])
    ->addText('directory_url', ['label' => 'Directory url'])
    ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$'])
    ->endGroup()
    ->addTrueFalse('has_makeup', [
        'label' => 'Makeup',
        'instructions' => 'Use this if hair and makeup have separate vendors.',
        'message' => 'Show publicly?',
        'default_value' => 0,
        'ui' => 1
    ])
    ->conditional('is_beauty_split', '==', '1')
    ->addGroup('makeup_group', ['label' => '', 'layout' => 'table'])
    ->conditional('has_makeup', '==', '1')
    ->addText('name', ['label' => 'Supplier name',])
    ->addText('url', ['label' => 'Supplier url'])
    ->addText('directory_url', ['label' => 'Directory url'])
    ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$'])
    ->endGroup()
    ->addTrueFalse('has_hair', [
        'label' => 'Hair',
        'instructions' => 'Use this if hair and makeup have separate vendors.',
        'message' => 'Show publicly?',
        'default_value' => 0,
        'ui' => 1
    ])
    ->conditional('is_beauty_split', '==', '1')
    ->addGroup('hair_group', ['label' => '', 'layout' => 'table'])
    ->conditional('has_hair', '==', '1')
    ->addText('name', ['label' => 'Supplier name',])
    ->addText('url', ['label' => 'Supplier url'])
    ->addText('directory_url', ['label' => 'Directory url'])
    ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$'])
    ->endGroup()
    ->addTrueFalse('is_beauty_split', [
        'label' => 'Multiple vendors',
        'instructions' => 'Select this option if hair and makeup was done by separate vendors.',
        'message' => 'Are there separate vendors for hair and makeup?',
        'default_value' => 0,
        'ui' => 1
    ]);


/**
 * Catering
 * Showing in single
 */

$vendor_team
    ->addTab('Catering', ['placement' => 'left'])
    ->addTrueFalse('has_catering', [
        'label' => 'Catering',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('catering_group', ['label' => '', 'layout' => 'table'])
    ->conditional('has_catering', '==', '1')
    ->addText('name', ['label' => 'Supplier name',])
    ->addText('url', ['label' => 'Supplier url'])
    ->addText('directory_url', ['label' => 'Directory url'])
    ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$']);

/**
 * Entertainment
 * Showing in single
 */
$vendor_team
    ->addTab('Entertainment', ['placement' => 'left'])
    ->addTrueFalse('has_music_entertainment', [
        'label' => 'Music/Entertainment',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('music_entertainment_group', ['label' => '', 'layout' => 'table'])
    ->conditional('has_music_entertainment', '==', '1')
    ->addText('name', ['label' => 'Supplier name',])
    ->addText('url', ['label' => 'Supplier url'])
    ->addText('directory_url', ['label' => 'Directory url'])
    ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$']);


/**
 * Flowers
 * Showing in single
 */
$vendor_team
    ->addTab('Flowers', ['placement' => 'left'])
    ->addTrueFalse('has_flowers', [
        'label' => 'Flowers',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('flowers_group', ['label' => '', 'layout' => 'table'])
         ->conditional('has_flowers', '==', '1')
            ->addText('name', ['label' => 'Supplier name',])
            ->addText('url', ['label' => 'Supplier url'])
            ->addText('directory_url', ['label' => 'Directory url'])
            ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$']);

/**
 * Miscellaneous
 * Showing in single
 */
$vendor_team
    ->addTab('Miscellaneous', ['placement' => 'left'])
    ->addTrueFalse('has_miscellaneous', [
        'label' => 'Miscellaneous',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('miscellaneous_group', ['label' => '', 'layout' => 'table'])
    ->conditional('has_miscellaneous', '==', '1')
    ->addText('name', ['label' => 'Supplier name',])
    ->addText('url', ['label' => 'Supplier url'])
    ->addText('directory_url', ['label' => 'Directory url'])
    ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$']);


/**
 * Planner
 * Showing in single
 */
$vendor_team
    ->addTab('Planner', ['placement' => 'left'])
    ->addTrueFalse('has_planner', [
        'label' => 'Planner',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('planner_group', ['label' => '', 'layout' => 'table'])
         ->conditional('has_planner', '==', '1')
            ->addText('name', ['label' => 'Supplier name',])
            ->addText('url', ['label' => 'Supplier url'])
            ->addText('directory_url', ['label' => 'Directory url'])
            ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$']);

/**
 * Photographer
 * Showing in single
 */
$vendor_team
    ->addTab('Photographer', ['placement' => 'left'])
    ->addTrueFalse('has_photographer', [
        'label' => 'Photographer',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('photographer_group', ['label' => '', 'layout' => 'table'])
        ->conditional('has_photographer', '==', '1')
            ->addText('name', ['label' => 'Supplier name',])
            ->addText('url', ['label' => 'Supplier url'])
            ->addText('directory_url', ['label' => 'Directory url'])
            ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$']);

/**
 * Officiant
 * Showing in single
 */
$vendor_team
    ->addTab('Officiant', ['placement' => 'left'])
    ->addTrueFalse('has_officiant', [
        'label' => 'Officiant',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('officiant_group', ['label' => '', 'layout' => 'table'])
    ->conditional('has_officiant', '==', '1')
    ->addText('name', ['label' => 'Supplier name',])
    ->addText('url', ['label' => 'Supplier url'])
    ->addText('directory_url', ['label' => 'Directory url'])
    ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$']);


/**
 * Rental & Decor
 * Showing in single
 */
$vendor_team
    ->addTab('Rental & Decor', ['placement' => 'left'])
    ->addTrueFalse('has_rental_decor', [
        'label' => 'Rental + Decor',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('rental_decor_group', ['label' => '', 'layout' => 'table'])
    ->conditional('has_rental_decor', '==', '1')
    ->addText('name', ['label' => 'Supplier name',])
    ->addText('url', ['label' => 'Supplier url'])
    ->addText('directory_url', ['label' => 'Directory url'])
    ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$']);

/**
 * Stationery
 * Showing in single
 */
$vendor_team
    ->addTab('Stationery', ['placement' => 'left'])
    ->addTrueFalse('has_stationery', [
        'label' => 'Stationery',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('stationery_group', ['label' => '', 'layout' => 'table'])
        ->conditional('has_stationery', '==', '1')
            ->addText('name', ['label' => 'Supplier name',])
            ->addText('url', ['label' => 'Supplier url'])
            ->addText('directory_url', ['label' => 'Directory url'])
            ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$']);


/**
 * Transportation
 * Showing in single
 */
$vendor_team
    ->addTab('Transportation', ['placement' => 'left'])
    ->addTrueFalse('has_transportation', [
        'label' => 'Transportation',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('transportation_group', ['label' => '', 'layout' => 'table'])
    ->conditional('has_transportation', '==', '1')
    ->addText('name', ['label' => 'Supplier name',])
    ->addText('url', ['label' => 'Supplier url'])
    ->addText('directory_url', ['label' => 'Directory url'])
    ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$']);


/**
 * Venue
 */
$vendor_team
    ->addTab('Venue', ['placement' => 'left'])
    ->addTrueFalse('has_venue', [
        'label' => 'Venue',
        'instructions' => 'Use this if reception and ceremony were done by the same vendor.',
        'message' => 'Show publicly?',
        'default_value' => 0,
        'ui' => 1
    ])
    ->conditional('is_venue_split', '==', '0')
    ->addGroup('venue_group', ['label' => '', 'layout' => 'table'])
        ->conditional('has_venue', '==', '1')
            ->addText('name', ['label' => 'Supplier name',])
            ->addText('url', ['label' => 'Supplier url'])
            ->addText('directory_url', ['label' => 'Directory url'])
            ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$'])
    ->endGroup()
    ->addTrueFalse('has_ceremony_venue', [ // single ceremony
        'label' => 'Ceremony Venue',
        'message' => 'Show publicly?',
        'default_value' => 0,
        'ui' => 1
    ])->conditional('is_venue_split', '==', '1')
    ->addGroup('ceremony_venue_group', ['label' => '', 'layout' => 'table'])
        ->conditional('has_ceremony_venue', '==', '1')
            ->addText('name', ['label' => 'Supplier name',])
            ->addText('url', ['label' => 'Supplier url'])
            ->addText('directory_url', ['label' => 'Directory url'])
            ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$'])
    ->endGroup()
    ->addTrueFalse('has_reception_venue', [ // single reception
        'label' => 'Reception Venue',
        'ui' => 1
    ])->conditional('is_venue_split', '==', '1')
    ->addGroup('reception_venue_group', ['label' => '', 'layout' => 'table'])
        ->conditional('has_reception_venue', '==', '1')
            ->addText('name', ['label' => 'Supplier name',])
            ->addText('url', ['label' => 'Supplier url'])
            ->addText('directory_url', ['label' => 'Directory url'])
            ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$'])
    ->endGroup()
    ->addTrueFalse('is_venue_split', [
        'label' => 'Multiple vendors',
        'instructions' => 'Select this option if reception and ceremony have separate vendors.',
        'message' => 'Are there separate vendors for reception and ceremony?',
        'default_value' => 0,
        'ui' => 1
    ]);

/**
 * Videography
 * Showing in single
 */
$vendor_team
    ->addTab('Videography', ['placement' => 'left'])
    ->addTrueFalse('has_videography', [
        'label' => 'Videography',
        'message' => 'Show publicly?',
        'ui' => 1
    ])
    ->addGroup('videography_group', ['label' => '', 'layout' => 'table'])
         ->conditional('has_videography', '==', '1')
            ->addText('name', ['label' => 'Supplier name',])
            ->addText('url', ['label' => 'Supplier url'])
            ->addText('directory_url', ['label' => 'Directory url'])
            ->addNumber('amount_spent', ['label' => 'Amount spent', 'prepend' => '$']);




acf_add_local_field_group($vendor_team->build());
