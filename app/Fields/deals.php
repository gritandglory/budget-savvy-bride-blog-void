<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Add abbreviation field to the state taxonomy.
 */
$builder = new FieldsBuilder('deal_brand');

$builder->setLocation('taxonomy', '==', 'deal-brand');

$builder
    ->addImage('brand_logo', [
        'label' => 'Brand Logo',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'return_format' => 'array',
        'preview_size' => 'sm-square',
        'library' => 'all',
    ]);

acf_add_local_field_group($builder->build());

/**
 * Feature on the home page
 */
$builder = new FieldsBuilder('deal_category');
$builder->setLocation('taxonomy', '==', 'deal-category');

$builder
    ->addTrueFalse('is_featured_home', [
        'label' => 'Featured',
        'instructions' => 'Featured on deals archive page',
        'ui' => 1,

    ]);

acf_add_local_field_group($builder->build());


$builder = new FieldsBuilder('Deal');

$builder->setLocation('post_type', '==', 'deals');

/*$builder
    ->addImage('brand_logo', [
        'label' => 'Brand Logo',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'return_format' => 'array',
        'preview_size' => 'md-square',
        'library' => 'all',
    ]);
*/

/*$builder->addText('brand_name', [
    'label' => 'Brand name',
    'instructions' => 'The name of the brand',
]);
*/

/*$builder
    ->addImage('offer_image', [
        'label' => 'Image',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'return_format' => 'array',
        'preview_size' => 'md-square',
        'library' => 'all',
    ]);*/
$builder
    ->addTrueFalse('exclusive', [
        'label' => 'Exclusive offer',
        'instructions' => 'If this is an exclusive offer to the Budget Savvy Bride',
        'ui' => 1,

    ]);
$builder
    ->addRadio('offer_type', [
        'label' => 'Type of offer',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'choices' => ['coupon' => 'Coupon', 'deals' => 'Deal' , 'freebie' => 'Freebie', 'sale' => 'Sale'],
        'allow_null' => 0,
        'other_choice' => 0,
        'save_other_choice' => 0,
        'default_value' => '',
        'layout' => 'horizontal',
        'return_format' => 'value',
    ]);
$builder
    ->addText('the_offer', [
        'label' => 'Short offer',
        'instructions' => 'A mini description of the offer to be used on abbreviated posts. ie, 10% off',
    ]);

$builder->addWysiwyg('offer_title', [
    'label' => 'Title',
    'instructions' => 'The title of the deals',
    'media_upload' => 0,
    'toolbar' => 'simple',
    'tabs' => 'visual',
    'wysiwyg_height' => 100
]);

$builder
    ->addWysiwyg('offer_text', [
        'label' => 'Details',
        'instructions' => 'Include the description of the offer',
        'media_upload' => 0,
        'toolbar' => 'simple',
        'tabs' => 'visual',
        'wysiwyg_height' => 150
    ]);

$builder->addText('offer_code', [
    'label' => 'Code',
    'instructions' => 'This needs to be an option, choose either a discount or percentage',
]);

$builder
    ->addUrl('offer_url', [
        'label' => 'Offer URL',
        'instructions' => 'Include the url for the offer',
    ]);

$builder
    ->addDatePicker('offer_expire', [
        'label' => 'Expiration date'
    ]);


acf_add_local_field_group($builder->build());
