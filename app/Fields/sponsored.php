<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

// field_name: is_sponsored_post

$sponsored = new FieldsBuilder('sponsored', ['title' => 'Sponsored', 'position' => 'side']);
$sponsored->addTrueFalse('is_sponsored_post', [
        'label' => '',
        'instructions' => 'Check if this post-single or page is sponsored',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => 'This is sponsored post-single?',
        'default_value' => 0,
        'ui' => 1,
    ])
    ->setLocation('post_type', '==', 'page')
    ->and('post_type', '==', 'post-single')
    ->and('post_type', '==', 'real_wedding');

acf_add_local_field_group($sponsored->build());
