<div class="flex flex-col max-w-2xl mx-auto items-center justify-center text-center mb-8">
    <h2 class="font-lg text-2xl font-serif">{{$title}}</h2>
    @if(isset($subtitle))
        <p class="mt-2">{{$subtitle}}</p>
    @endif
    <span class="flex mx-auto mt-4 border-b border-coral-300 w-48"></span>
</div>
