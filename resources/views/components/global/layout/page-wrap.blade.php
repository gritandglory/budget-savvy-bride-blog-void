<div class="wrap mt-12 {{$class}}" role="document">
    {{$slot}}
</div>
