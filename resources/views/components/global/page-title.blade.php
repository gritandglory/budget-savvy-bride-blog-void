@component('components.global.base.BaseContainer')
    <div class="page-title container mx-auto">
        <h1 class="text-4xl font-serif text-center">{{$title}}</h1>
        @isset($subtitle) <p class="max-w-4xl mx-auto text-center">{{$subtitle}}</p> @endif
    </div>
@endcomponent
