<header class="post_header">
    @include('components.global.post-single.preamble', ['sponsored' => $sponsored])

    @include('components.global.post-single.title', ['title' => $title])

    @include('components.global.post-single.meta', [
        'author_name'   => $author_name,
        'published'     => $published,
        'modified'      => $modified
    ])

</header>
