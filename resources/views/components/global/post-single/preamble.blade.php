<div class="preamble">
    <div class=" flex justify-start items-center">
        @if($sponsored === true)
            <div
                class="flex items-center bg-turquoise-500 text-xs text-white rounded h-5 leading-none px-1 mr-2 font-medium">
                Sponsored Content
            </div>
        @endif
        <span class="text-xs text-gray-800">This post may contain affiliate links. <a href="/about/policies" class="text-turquoise-600 font-medium hover:underline">Click here to learn more.</a></span>
    </div>
</div>
