<ul class="post_tags mt-2">
    @if(isset($tags))
        @foreach($tags as $tag )
            <li class="inline-block">
                <a class="flex h-5 text-xs text-coral-500 bg-coral-100 rounded-full px-2 mr-2 mt-2 font-medium transition transition-all duration-200 ease-in-out hover:text-coral-100 hover:bg-coral-500"
                   href=""> {{$tag->name}}</a>
            </li>
        @endforeach
    @endif
</ul>
