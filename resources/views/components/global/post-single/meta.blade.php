<div class="post_byline text-gray-700 text-xs">
    <p>By: <span class="author">{!! $author_name !!}</span> <span class="mx-1">|</span> Published: <time class="updated" datetime="{!! $published !!}"> {!! $published !!} </time> <span class="mx-1">|</span> Updated: {!! $modified !!}
    </p>
</div>
