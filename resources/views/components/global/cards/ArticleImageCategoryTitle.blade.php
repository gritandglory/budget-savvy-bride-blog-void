<div class="flex flex-col">
    <div class="rounded overflow-hidden flex flex-shrink-0">
        <img class="w-full"
             src="{{$src}}" alt="">
    </div>
    <div class="flex flex-col justify-start items-start">
        @component('components.global.base.BaseCategory', ['category' => $category])@endcomponent
        <h2 class="font-serif text-lg">{{$title}}</h2>
    </div>
</div>
