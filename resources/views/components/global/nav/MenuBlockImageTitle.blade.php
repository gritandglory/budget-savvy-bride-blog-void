<div class="menu-block-image group">
    <a class="menu-block-link" href="{{$page_slug}} ">
        <figure class="rounded overflow-hidden">
            <img
                class="w-full transform scale-100 group-hover:shadow-lg group-hover:scale-110 transition transition-transform duration-300 ease-in-out"
                src="{{$image_name}}" alt="">
        </figure>
        <div class="font-serif text-base group-hover:underline">{{$title}}</div>
    </a>
</div>
