<div class="absolute inset-x-0 block w-full shadow z-100 bg-white border-t border-gray-200">
    <div class="px-4 xl:px-0 xl:max-w-menu mx-auto py-4">
        <div class="grid col-gap-4 {{$grid_columns}}">
            {{$slot}}
        </div>
    </div>
</div>
