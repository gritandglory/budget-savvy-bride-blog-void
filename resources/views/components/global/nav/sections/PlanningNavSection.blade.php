@component('components.global.nav.MenuDropDownContainer', ['grid_columns' => 'grid-cols-4'])

    @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>"/just-engaged",
        'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/get-started.jpg",
        'title'         =>"Getting Started",
        'text'          =>"Get our wedding planning checklist, plus help with setting your wedding budget and priorities."
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitleText', [
    'page_slug'     =>"/wedding-inspiration",
    'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/research-inspiration.jpg",
    'title'         =>"Research & Inspiration",
    'text'          =>"Gathering inspiration, finding your wedding vendor team, and collecting ideas for your big day."
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitleText', [
    'page_slug'     =>"/wedding-decisions",
    'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/making-wedding-decisions.jpg",
    'title'         =>"Making Wedding Decisions",
    'text'          =>"Advice for hiring your vendors, reviewing contracts, buying wedding attire and decor, etc."
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitleText', [
    'page_slug'     =>"/logistics-execution",
    'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/logistics-execution.jpg",
    'title'         =>"Logistics & Execution",
    'text'          =>"Wedding day timelines, checklists, and other tools and advice to pull off your big day without a hitch!"
    ])@endcomponent

@endcomponent
