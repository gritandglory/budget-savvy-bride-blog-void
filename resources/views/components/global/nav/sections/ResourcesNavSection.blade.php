@component('components.global.nav.MenuDropDownContainer', ['grid_columns' => 'grid-cols-2'])

        <div class="grid grid-cols-2 col-gap-4">
            @component('components.global.nav.MenuBlockImageTitleText', [
            'page_slug'     =>  "budget-wedding-resources",
            'img_name'      =>  "https://cdn2.thebudgetsavvybride.com/assets/img/menu/wedding-shopping-resources.jpg",
            'title'         =>  "Wedding Shopping Resources",
            'text'          =>  "Where to buy everything you need for your wedding: check out this list of wedding suppliers."
            ])@endcomponent
                @component('components.global.nav.MenuBlockImageTitleText', [
            'page_slug'     =>  "FBgroup",
            'img_name'      =>  "https://cdn2.thebudgetsavvybride.com/assets/img/menu/join-our-savvy-community.jpg",
            'title'         =>  "Join our Savvy Community",
            'text'          =>  "Join our Facebook Group: Budget-Savvy Wedding Planning to get support from other budget brides!"
            ])@endcomponent

        </div>

        <div>
            <div class="grid grid-cols-4 col-gap-4">
                <div class="menu-block-partner group">
                    <a class="menu-block-link" href="go/SBB">
                        <div class="text-xs uppercase font-medium text-center leading-none group-hover:underline">Silk Flowers</div>
                        <figure class="rounded mt-1 overflow-hidden border border-gray-200">
                            <img class="w-full transform scale-100 group-hover:shadow-lg group-hover:scale-110 transition transition-transform duration-300 ease-in-out"
                                 src="https://cdn2.thebudgetsavvybride.com/assets/img/menu/logo-blooms.jpg" alt="Something borrow blooms">
                        </figure>
                    </a>
                </div>
                <div class="menu-block-partner group">
                    <a class="menu-block-link" href="go/Joy">
                        <div class="text-xs uppercase font-medium text-center leading-none group-hover:underline">wedding websites</div>
                        <figure class="rounded mt-1 overflow-hidden border border-gray-200">
                            <img class="w-full transform scale-100 group-hover:shadow-lg group-hover:scale-110 transition transition-transform duration-300 ease-in-out"
                                 src="https://cdn2.thebudgetsavvybride.com/assets/img/menu/logo-joy.jpg" alt="Joy Wedding wedsites">
                        </figure>
                    </a>
                </div>
                <div class="menu-block-partner group">
                    <a class="menu-block-link" href="go/BirdyGrey">
                        <div class="text-xs uppercase font-medium text-center leading-none group-hover:underline">bridesmaids dresses</div>
                        <figure class="rounded mt-1 overflow-hidden border border-gray-200">
                            <img class="w-full transform scale-100 group-hover:shadow-lg group-hover:scale-110 transition transition-transform duration-300 ease-in-out"
                                 src="https://cdn2.thebudgetsavvybride.com/assets/img/menu/logo-birdy-grey.jpg" alt="Birdy grey wedding dresses">
                        </figure>
                    </a>
                </div>
                <div class="menu-block-partner group">
                    <a class="menu-block-link" href="go/Minted">
                        <div class="text-xs uppercase font-medium text-center leading-none group-hover:underline">wedding invitations</div>
                        <figure class="rounded mt-1 overflow-hidden border border-gray-200">
                            <img class="w-full transform scale-100 group-hover:shadow-lg group-hover:scale-110 transition transition-transform duration-300 ease-in-out"
                                 src="https://cdn2.thebudgetsavvybride.com/assets/img/menu/logo-mint.jpg" alt="">
                        </figure>
                    </a>
                </div>

            </div>
            <div class="font-serif text-base mt-2 group-hover:underline">Wedding Planning Partners</div>
            <p class="text-sm">Check out some of our wedding planning partners that will help you have a savvy wedding
                day!</p>
        </div>

@endcomponent



