@component('components.global.nav.MenuDropDownContainer', ['grid_columns' => 'grid-cols-4'])

    @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>"/real-weddings-by-budget",
        'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/browse-real-weddings-by-budget.jpg",
        'title'         =>"Real Weddings by Budget",
        'text'          =>"View real budget weddings from real couples, complete with budget breakdowns!"
    ])@endcomponent
    @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>"/real-weddings-by-location",
        'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/browse-real-weddings-by-location.jpg",
        'title'         =>"Real Weddings by Location",
        'text'          =>"View real weddings on a budget from your state to get savvy wedding inspiration from your area."
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>"/real-weddings-by-color",
        'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/browse-real-weddings-by-color.jpg",
        'title'         =>"Real Weddings by Color",
        'text'          =>"View real weddings based on their color scheme to get inspiration in your wedding color palette."
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>"/real-weddings-by-season",
        'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/browse-real-weddings-by-season.jpg",
        'title'         =>"Real Weddings by Season",
        'text'          =>"View wedding inspiration by season. See weddings from Winter, Spring, Summer, and Fall!"
    ])@endcomponent

@endcomponent
