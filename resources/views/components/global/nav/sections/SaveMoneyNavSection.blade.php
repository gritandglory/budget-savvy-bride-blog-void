@component('components.global.nav.MenuDropDownContainer', ['grid_columns' => 'grid-cols-4'])

    @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>"/wedding-deals",
        'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/exclusive-wedding-deals-discounts.jpg",
        'title'         =>"Exclusive Wedding Deals & Discounts",
        'text'          =>"Check out these exclusive offers from our savvy wedding partners!"
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitleText', [
       'page_slug'     =>"/wedding-budget-tips",
       'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/wedding-budget-tips-to-save-money.jpg",
       'title'         =>"Wedding Budget Tips to Save Money",
       'text'          =>"Don’t miss this epic list of over 100 tips to save money on your wedding"
   ])@endcomponent
    @component('components.global.nav.MenuBlockImageTitleText', [
    'page_slug'     =>"/wedding-budgets",
    'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/creating-your-wedding-savings-plan.jpg",
    'title'         =>"Creating Your Wedding Savings Plan",
    'text'          =>"Ways to lower your expenses, automate savings, and even earn extra money for your big day."
])@endcomponent

    <div class="menu-block-list">
        <div class="font-serif text-base leading-none">Ways to Save Money on Your Wedding</div>
        <ul class="flex flex-col p-0 mt-2">
            <li class="inline-block leading-relaxed">
                <a class="text-sm hover:underline" href="/how-to-find-affordable-unique-wedding-venues">
                    Save Money on Your Wedding Venue</a>
            </li>
            <li class="inline-block leading-relaxed">
                <a class="text-sm hover:underline" href="/wedding-dresse">
                    Save Money on Your Wedding Dress</a>
            </li>
            <li class="inline-block leading-relaxed">
                <a class="text-sm hover:underline" href="/cheap-wedding-decor">
                    Save Money on Your Wedding Decor</a>
            </li>
            <li class="inline-block leading-relaxed">
                <a class="text-sm hover:underline" href="/save-money-wedding-flowers">
                    Save Money on Your Wedding Flowers</a>
            </li>
            <li class="inline-block leading-relaxed">
                <a class="text-sm hover:underline" href="/top-10-inexpensive-wedding-foods">
                    Save Money on Your Wedding Catering</a>
            </li>
          {{--  <li class="inline-block leading-relaxed">
                <a class="text-sm hover:underline" href="the-most-inexpensive-and-efficient-wedding-invitations-ever">
                    Save Money on Your Wedding Invitations</a>
            </li>--}}
        </ul>
    </div>

@endcomponent


