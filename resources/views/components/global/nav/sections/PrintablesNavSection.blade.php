@component('components.global.nav.MenuDropDownContainer', ['grid_columns' => 'grid-cols-4'])

    @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>  "tag/printable-wedding-tools",
        'img_name'      =>  "https://cdn2.thebudgetsavvybride.com/assets/img/menu/printable-wedding-planning-tools.jpg",
        'title'         =>  "Browse Real Weddings by Budget",
        'text'          =>  "View real budget weddings from real couples, complete with budget breakdowns!"
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>  "tag/pre-wedding-printables",
        'img_name'      =>  "https://cdn2.thebudgetsavvybride.com/assets/img/menu/printable-pre-wedding-stationery.jpg",
        'title'         =>  "Printable Pre-Wedding Stationery",
        'text'          =>  "Printable Save-the-Dates, Bridesmaid Proposal Cards, Wedding Invitations, RSVPs, Labels & more."
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>  "tag/ceremony-printables",
        'img_name'      =>  "https://cdn2.thebudgetsavvybride.com/assets/img/menu/wedding-ceremony-printables.jpg",
        'title'         =>  "Wedding Ceremony Printables",
        'text'          =>  "Browse Printable Wedding Programs, Reserved Signs, Welcome Signs, Guestbook Cards, etc."
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>  "tag/reception-printables",
        'img_name'      =>  "https://cdn2.thebudgetsavvybride.com/assets/img/menu/wedding-reception-printables.jpg",
        'title'         =>  "Wedding Reception Printables",
        'text'          =>  "Printable Wedding Banners, Favor Tags, Photo Booth Props, Place Cards, Table Numbers, etc."
    ])@endcomponent
@endcomponent

