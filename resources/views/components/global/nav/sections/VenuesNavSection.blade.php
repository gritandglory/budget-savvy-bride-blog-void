@component('components.global.nav.MenuDropDownContainer', ['grid_columns' => 'grid-cols-8'])

    <div class="col-span-2">
        @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>  "tag/pre-wedding-printables",
        'img_name'      =>  "https://cdn2.thebudgetsavvybride.com/assets/img/menu/browse-wedding-venues.jpg",
        'title'         =>  "Browse All Wedding Venues",
        'text'          =>  "View our venue directory to find the most unique and affordable wedding venues for your big day."
        ])@endcomponent

        </div>
        <div class="col-span-5">
            <h4 class="font-serif leading-none">Wedding Venue Categories</h4>
            <div class="grid grid-cols-4 mt-2">
               {{-- <MenuBlockListNuxtLink
                    :nuxt_route_name="nuxt_route_name"
                    :nuxt_link_param="nuxt_link_param"
                    :list="listVenueCategory1"
                />
                <MenuBlockListNuxtLink
                    :nuxt_route_name="nuxt_route_name"
                    :nuxt_link_param="nuxt_link_param"
                    :list="listVenueCategory2"
                />
                <MenuBlockListNuxtLink
                    :nuxt_route_name="nuxt_route_name"
                    :nuxt_link_param="nuxt_link_param"
                    :list="listVenueCategory3"
                />
                <MenuBlockListNuxtLink
                    :nuxt_route_name="nuxt_route_name"
                    :nuxt_link_param="nuxt_link_param"
                    :list="listVenueCategory4"
                />--}}
            </div>
        </div>
        <div class="col-span-1">
{{--            <MenuBlockListNuxtLink
                title="Are you a Vendor?"
                :nuxt_route_name="nuxt_route_name"
                :nuxt_link_param="nuxt_link_param"
                :list="listTextCol"
            />--}}
        </div>
@endcomponent

       {{-- data() {
            return {
                nuxt_link_param: 'category',
                nuxt_route_name: 'slug-results',
                listVenueCategory1: [
                    {
                        'nuxt_link_slug': 'banquet-hall-wedding-venues',
                        'title': 'Banquet Hall',
                    },
                    {
                        'nuxt_link_slug': 'barn-farm-ranch-wedding-venues',
                        'title': 'Barn, Farm & Ranch',
                    },
                    {
                        'nuxt_link_slug': 'beach-wedding-venues',
                        'title': 'Beach',
                    },
                    {
                        'nuxt_link_slug': 'bed-and-breakfast-wedding-venues',
                        'title': 'Bed & Breakfast',
                    },
                    {
                        'nuxt_link_slug': 'community-rec-center-wedding-venues',
                        'title': ' Community/Rec Center',
                    },
                ],
                listVenueCategory2: [

                    {
                        'nuxt_link_slug': 'country-club-wedding-venues',
                        'title': 'Country Club',
                    },
                    {
                        'nuxt_link_slug': 'estate-mansion-wedding-venues',
                        'title': 'Estate & Mansion',
                    },
                    {
                        'nuxt_link_slug': 'garden-park-wedding-venues',
                        'title': 'Garden & Park',
                    },
                    {
                        'nuxt_link_slug': 'greenhouse-wedding-venues',
                        'title': 'Greenhouse',
                    },
                    {
                        'nuxt_link_slug': 'florists-wedding-venues',
                        'title': 'Hotel & Resort',
                    },
                ],
                listVenueCategory3: [
                    {
                        'nuxt_link_slug': 'library-wedding-venues',
                        'title': 'Library',
                    },
                    {
                        'nuxt_link_slug': 'lodge-wedding-venues',
                        'title': 'Lodge & Campground',
                    },
                    {
                        'nuxt_link_slug': 'museum-gallery-wedding-venues',
                        'title': ' Museum & Gallery',
                    },
                    {
                        'nuxt_link_slug': 'restaurant-wedding-venues',
                        'title': 'Restaurant',
                    },
                    {
                        'nuxt_link_slug': 'rooftop-wedding-venues',
                        'title': 'Rooftop',
                    },
                ],
                listVenueCategory4: [
                    {
                        'nuxt_link_slug': 'vacation-rental-wedding-venues',
                        'title': 'Vacation Rental',
                    },
                    {
                        'nuxt_link_slug': 'lodge-wedding-venues',
                        'title': 'Lodge & Campground',
                    },
                    {
                        'nuxt_link_slug': 'warehouse-wedding-venues',
                        'title': 'Warehouse',
                    },
                    {
                        'nuxt_link_slug': 'winery-brewery-wedding-venues',
                        'title': 'Winery Brewery',
                    },
                    {
                        'nuxt_link_slug': 'zoo-wedding-venues',
                        'title': 'Zoo',
                    },
                ],
                listTextCol: [
                    {
                        'title': 'Directory Home',
                        'page_slug': '',
                    },
                    {
                        'title': 'Join the Directory',
                        'page_slug': '',
                    },
                    {
                        'title': 'Vendor Login',
                        'page_slug': '',
                    },

                ]
            }
        },
    }
</script>
--}}
