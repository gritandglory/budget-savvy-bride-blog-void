@component('components.global.nav.MenuDropDownContainer', ['grid_columns' => 'grid-cols-8'])

    @component('components.global.nav.MenuBlockImageTitle', [
    'page_slug'     => 'directory/wedding-planner/results',
    'image_name'    => 'https://cdn2.thebudgetsavvybride.com/assets/img/vendor/vendor-wedding-planner-square.jpg',
    'title'         => 'Planners'
    ])@endcomponent
    @component('components.global.nav.MenuBlockImageTitle', [
    'page_slug'     => 'directory/wedding-photographer/results',
    'image_name'    => 'https://cdn2.thebudgetsavvybride.com/assets/img/menu/shop-wedding-dresses.jpg',
    'title'         => 'Photographers'
    ])@endcomponent
    @component('components.global.nav.MenuBlockImageTitle', [
    'page_slug'     => 'directory/wedding-florist/results',
    'image_name'    => 'https://cdn2.thebudgetsavvybride.com/assets/img/vendor/vendor-florist-square.jpg',
    'title'         => 'Florists'
    ])@endcomponent
    @component('components.global.nav.MenuBlockImageTitle', [
    'page_slug'     => 'directory/wedding-beauty-services/results',
    'image_name'    => 'https://cdn2.thebudgetsavvybride.com/assets/img/vendor/vendors-beauty-services-square.jpg',
    'title'         => 'Beauty Services'
    ])@endcomponent
    @component('components.global.nav.MenuBlockImageTitle', [
    'page_slug'     => 'directory/wedding-catering/results',
    'image_name'    => 'https://cdn2.thebudgetsavvybride.com/assets/img/vendor/vendor-catering-square.jpg',
    'title'         => 'Caterers'
    ])@endcomponent
    @component('components.global.nav.MenuBlockImageTitle', [
    'page_slug'     => 'directory/wedding-bakery/results',
    'image_name'    => 'https://cdn2.thebudgetsavvybride.com/assets/img/vendor/vendor-bakery-square.jpg',
    'title'         => 'Bakery'
    ])@endcomponent
    @component('components.global.nav.MenuBlockImageTitle', [
    'page_slug'     => 'directory/wedding-entertainment/results',
    'image_name'    => 'https://cdn2.thebudgetsavvybride.com/assets/img/vendor/vendors-entertainment-square.png',
    'title'         => 'Entertainment'
    ])@endcomponent
    {{--  <MenuBlockListNuxtLink
          title="Are you a Vendor?"
          :nuxt_route_name="nuxt_route_name"
          :nuxt_link_param="nuxt_link_param"
          :list="listTextCol"
      />--}}
@endcomponent
{{--

                 {
                     'nuxt_link_slug': 'wedding-entertainment',
                     'image_name': 'https://cdn2.thebudgetsavvybride.com/assets/img/vendor/vendors-entertainment-square.png',
                     'title': 'Entertainment',
                 },
             ],
             listTextCol: [
                 {
                     'title': 'Directory Home',
                     'page_slug': '',
                 },
                 {
                     'title': 'Join the Directory',
                     'page_slug': '',
                 },
                 {
                     'title': 'Vendor Login',
                     'page_slug': '',
                 },

             ]
--}}
