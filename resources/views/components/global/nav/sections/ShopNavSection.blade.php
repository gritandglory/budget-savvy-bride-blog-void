@component('components.global.nav.MenuDropDownContainer', ['grid_columns' => 'grid-cols-8'])

    @component('components.global.nav.MenuBlockImageTitle', [
    'page_slug'     => 'products/attire',
    'image_name'    => 'https://cdn2.thebudgetsavvybride.com/assets/img/menu/shop-wedding-dresses.jpg',
    'title'         => 'Wedding Attire'
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitle', [
    'page_slug'     => 'products/decor',
    'image_name'    => 'https://cdn2.thebudgetsavvybride.com/assets/img/menu/shop-wedding-decor.jpg',
    'title'         => 'Wedding Decor'
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitle', [
    'page_slug'     => 'products/rentals',
    'image_name'    => 'https://cdn2.thebudgetsavvybride.com/assets/img/menu/shop-wedding-rentals.jpg',
    'title'         => 'Wedding Rentals'
    ])@endcomponent


    @component('components.global.nav.MenuBlockImageTitle', [
    'page_slug'     => 'products/accessories',
    'image_name'    => 'https://cdn2.thebudgetsavvybride.com/assets/img/menu/shop-accessories.jpg',
    'title'         => 'Accessories'
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitle', [
    'page_slug'     => 'products/favors',
    'image_name'    => 'https://cdn2.thebudgetsavvybride.com/assets/img/menu/shop-wedding-favors.jpg',
    'title'         => 'Wedding Favors'
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitle', [
    'page_slug'     => 'products/stationery',
    'image_name'    => 'https://cdn2.thebudgetsavvybride.com/assets/img/menu/shop-wedding-invites.jpg',
    'title'         => 'Wedding Invites'
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitle', [
   'page_slug'     => 'products/wedding-registries',
   'image_name'    => 'https://cdn2.thebudgetsavvybride.com/assets/img/menu/shop-wedding-registries.jpg',
   'title'         => 'Wedding Registry'
   ])@endcomponent
@endcomponent

{{--

listText: [
    {
        'text': 'Anniversary Gifts',
        'page_slug': 'browse/wedding-anniversary-gift-ideas-by-year',
    },
    {
        'text': 'Gifts for Couples',
        'page_slug': 'gift-ideas-for-engaged-couples-or-newlyweds',
    },
    {
        'text': 'Bridesmaids Gifts',
        'page_slug': 'gift-ideas-for-your-bridesmaids',
    },
    {
        'text': 'Groomsmen Gifts',
        'page_slug': 'gift-guide-groomsmen',
    },
    {
        'text': 'Wedding Gift Guides',
        'page_slug': 'gift-guides',
    },
]
--}}

