@component('components.global.nav.MenuDropDownContainer', ['grid_columns' => 'grid-cols-4'])

    @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>"tag/diy-ceremony-decor",
        'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/diy-wedding-ceremony-projects.jpg",
        'title'         =>"DIY Wedding Ceremony Projects",
        'text'          =>"Learn how to make Ceremony Decor, Wedding Aisle Markers, Ceremony Signage Projects, etc."
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>"flower-tutorials",
        'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/diy-your-wedding-flowers.jpg",
        'title'         =>"DIY Your Wedding Flowers",
        'text'          =>"Browse DIY Wedding Flower Tutorials for Wedding Centerpieces, Wedding Bouquets, etc."
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>"tag/diy-reception-decor",
        'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/diy-wedding-reception-projects.jpg",
        'title'         =>"DIY Wedding Reception Projects",
        'text'          =>"Create your own Wedding Photobooth, Wedding Centerpieces, Signage, Stationery, and more!"
    ])@endcomponent

    @component('components.global.nav.MenuBlockImageTitleText', [
        'page_slug'     =>"tag/diy-wedding-favors",
        'img_name'      =>"https://cdn2.thebudgetsavvybride.com/assets/img/menu/other-wedding-diy-projects.jpg",
        'title'         =>"Other Wedding DIY Projects",
        'text'          =>"DIY Wedding Favors, Wedding Invitation Projects, Bachelorette Party Projects, DIY Gift Ideas & more!"
    ])@endcomponent
@endcomponent



