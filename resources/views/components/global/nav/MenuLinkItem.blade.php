<button
    @mouseover="show"
    :class="{'border-coral-500' : isActive}"
    class="focus:outline-none px-1 xl:px-0 h-16 flex items-center uppercase text-gray-900 font-medium border-b-2 border-white hover:border-coral-500 text-sm lg:text-xs xl:text-sm hover:cursor-pointer hover:text-coral-500 transition-all duration-150 ">
    {{$title}}
</button>
