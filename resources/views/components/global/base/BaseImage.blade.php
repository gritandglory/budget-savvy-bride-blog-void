<img class="@isset($id){{$id}}@endisset w-full transform scale-100 group-hover:shadow-lg group-hover:scale-110 transition transition-transform duration-300 ease-in-out "
     src="@isset($src){{$src}}@endisset"
     srcset="@isset($srcset){{$srcset}}@endisset"
     alt="@isset($alt){{$alt}}@endisset"
>
