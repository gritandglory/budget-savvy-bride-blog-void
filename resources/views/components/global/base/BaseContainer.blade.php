<div class="max-w-8xl mx-auto px-2 lg:px-4{{$class ?? ''}}">
    {{$slot}}
</div>
