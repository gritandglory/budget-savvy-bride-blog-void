<div class="w-full inline-block my-8 md:my-12 lg:my-14 relative @isset($class){{$class}}@endisset">
    {{$slot}}
</div>
