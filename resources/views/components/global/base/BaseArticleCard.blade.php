<div class="flex sm:flex-col lg:mb-0 rounded overflow-hidden ">
    <a href="@isset($href){{$href}}@endisset" class="group w-2/5 sm:w-full">
        <figure class="overflow-hidden rounded w-full">
            @component('components.global.base.BaseImage', [
                'scale-transform'   => true,
                'src'               => $src
            ])@endcomponent
        </figure>
    </a>
    <div class="flex flex-1 flex-col justify-start pl-4 sm:pl-0">
        @component('components.global.base.BaseCategory', ['category' => $category])@endcomponent
        <a href="@isset($href){{$href}}@endisset" class="font-serif text-lg hover:underline">@isset($title){{$title}}@endisset</a>
        @if(isset($excerpt))
            <p class="text-base">{{$excerpt}}</p>
        @endif
    </div>
</div>
