<li class="p-0 m-0">
  <a class="text-sm hover:underline {{$class}}"
     href="{{$href}}">{{$title}}</a>
</li>
