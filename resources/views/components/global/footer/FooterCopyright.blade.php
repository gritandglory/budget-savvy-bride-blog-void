<div id="copyright_footer" class="bg-gray-100 md:bg-gray-900 md:text-white border-t border-gray-200">
    <div class="container mx-auto md:py-4 text-center py-6 px-10">
        <p class="source-org copyright text-sm md:text-xs leading-loose md:leading-tight flex flex-col md:flex-row justify-center items-center">
            &copy; The Budget Savvy Bride ™ 2008 – {{date('Y')}}  <span class="hidden md:block mx-1">•</span> <span class="flex h-0 md:hidden "><br></span>
            Property of Budget Savvy Inc <span class="flex h-0 md:hidden"><br></span>
            <span class="flex items-center mx-1">• Site Built with
            @svg('heart', 'h-4 w-4 text-coral-500 mx-1 inline-flex md:block')
            by Tom Philpotts
            </span>
        </p>
    </div>
</div>
