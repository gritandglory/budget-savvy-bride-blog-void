<header class="post_header">
    <div class="preamble">
        <div class=" flex justify-start items-center">
            @if($sponsored === true)
                <div
                    class="flex items-center bg-turquoise-500 text-xs text-white rounded h-5 leading-none px-1 mr-2 font-medium">
                    Sponsored Content
                </div>
            @endif
            <span class="text-xs text-gray-800">This post may contain affiliate links. <a href="about/policies">Click here to learn more.</a></span>
        </div>
    </div>

    <h1 class="post_title mt-2 text-4xl font-serif leading-tight">{{$title}}</h1>

    @include('components.global.post-single.meta', ['src' => $src])

    @include('components.template.single.article-feature-image', [
        'author_name'   => $author_name,
        'published'     => $published,
        'modified'      => $modified
    ])

</header>
