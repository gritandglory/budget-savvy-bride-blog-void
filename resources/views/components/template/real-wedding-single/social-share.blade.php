<div class="sticky" style="top: 3rem">
    <ul class="flex flex-col items-center justify-center ">
        <li class="font-medium text-sm text-center">Share</li>
        <li class="block mt-2 transform hover:-translate-y-1 transition ease-in-out duration-150 hover:scale-105">
            <a href="#" class="text-turquoise-500 hover:text-turquoise-600 transition ease-in-out duration-150">
                @svg('facebook', 'h-6 w-6')
            </a>
        </li>
        <li class="block mt-4 transform hover:-translate-y-1 transition ease-in-out duration-150 hover:scale-105">
            <a href="#" class="text-turquoise-500 hover:text-turquoise-600 transition ease-in-out duration-150">
                @svg('twitter', 'h-6 w-6')
            </a>
        </li>
        <li class="block mt-4 transform hover:-translate-y-1 transition ease-in-out duration-150 hover:scale-105">
            <a href="#" class="text-turquoise-500 hover:text-turquoise-600 transition ease-in-out duration-150">
                @svg('pinterest', 'h-6 w-6')
            </a>
        </li>
    </ul>
</div>
