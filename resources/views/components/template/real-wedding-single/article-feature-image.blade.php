<div class="feature-image my-6 block">
    <div class="rounded overflow-hidden block">
        <img class="w-full" src="{{$src}}" alt="">
    </div>
</div>
