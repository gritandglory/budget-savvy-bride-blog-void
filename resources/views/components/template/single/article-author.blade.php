@component('components.global.blocks.block-container', ['class' => 'post-single-author inline-block border-b border-t border-gray-200 mt-24'])

    <div class="author flex items-start pb-8">
        <div class="w-36">
            <img class="h-32 w-32 rounded-full"
                 src="{{$author_profile_image['sizes']['thumbnail']}}"
                 alt="{{$author_profile_image['title']}}"/>
        </div>
        <div class="flex-1 pl-4">

            <h4 class="author_name font-serif leading-none" itemprop="author" itemscope
                itemtype="http://schema.org/Person">{!! $name !!}</h4>
            <p class="text-sm mt-2 prose">{!! $description !!}</p>
            <ul class="author_social_contact flex items-center justify-start mt-4">
                @if(isset($author_social['facebook']))
                    <li class="block mr-4 transform hover:-translate-y-1 transition ease-in-out duration-150 hover:scale-105">
                        <a href="{{$author_social['facebook']}}"
                           class="text-turquoise-500 hover:text-turquoise-600 transition ease-in-out duration-150">
                            @svg('facebook', 'h-6 w-6')
                        </a>
                    </li>
                @endif
                @if(isset($author_social['twitter']))
                    <li class="block mr-4 transform hover:-translate-y-1 transition ease-in-out duration-150 hover:scale-105">
                        <a href="{{$author_social['twitter']}}"
                           class="text-turquoise-500 hover:text-turquoise-600 transition ease-in-out duration-150">
                            @svg('twitter', 'h-6 w-6')
                        </a>
                    </li>
                @endif
                @if(isset($author_social['pinterest']))
                    <li class="block mr-4 transform hover:-translate-y-1 transition ease-in-out duration-150 hover:scale-105">
                        <a href="{{$author_social['pinterest']}}"
                           class="text-turquoise-500 hover:text-turquoise-600 transition ease-in-out duration-150">
                            @svg('pinterest', 'h-6 w-6')
                        </a>
                    </li>
                @endif
                @if(isset($author_social['youtube']))
                    <li class="block mr-4 transform hover:-translate-y-1 transition ease-in-out duration-150 hover:scale-105">
                        <a href="{{$author_social['youtube']}}"
                           class="text-turquoise-500 hover:text-turquoise-600 transition ease-in-out duration-150">
                            @svg('youtube', 'h-6 w-6')
                        </a>
                    </li>
                @endif
                @if(isset($author_social['bloglovin']))
                    <li class="block mr-4 transform hover:-translate-y-1 transition ease-in-out duration-150 hover:scale-105">
                        <a href="{{$author_social['bloglovin']}}"
                           class="text-turquoise-500 hover:text-turquoise-600 transition ease-in-out duration-150">
                            @svg('bloglovin', 'h-6 w-6')
                        </a>
                    </li>
                @endif

            </ul>
        </div>
    </div>
@endcomponent
