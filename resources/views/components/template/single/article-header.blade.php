<header class="post_header">
    <div class="preamble">
        <div class=" flex justify-start items-center">
            @if($sponsored === true)
                <div
                    class="flex items-center bg-turquoise-500 text-xs text-white rounded h-5 leading-none px-1 mr-2 font-medium">
                    Sponsored Content
                </div>
            @endif
            <span class="text-xs text-gray-800">This post may contain affiliate links. <a href="about/policies">Click here to learn more.</a></span>
        </div>
    </div>

    <h1 class="post_title mt-2 text-4xl font-serif leading-tight">{{$title}}</h1>

    <div class="post_byline text-gray-700 text-xs">

        <p>By: <span class="author">{!! $author_name !!}</span> <span class="mx-1">|</span> Published: <time class="updated" datetime="{!! $published !!}"> {!! $published !!} </time> <span class="mx-1">|</span> Updated: {!! $modified !!}
        </p>

    </div>

    <ul class="post_tags mt-2">
        @if(isset($tags))
            @foreach($tags as $tag )
                <li class="inline-block">
                    <a class="flex text-xs leading-relaxed text-coral-500 bg-coral-100 rounded-full px-2 mr-2 mt-2 font-medium transition transition-all duration-200 ease-in-out hover:text-coral-100 hover:bg-coral-500"
                       href=""> {{$tag->name}}</a>
                </li>
            @endforeach
        @endif
    </ul>

    <div class="intro-field"><p class="lead-text mt-4 text-xl block">{{$excerpt}}</p></div>

    @include('components.template.single.article-feature-image', ['src' => $src])

</header>
