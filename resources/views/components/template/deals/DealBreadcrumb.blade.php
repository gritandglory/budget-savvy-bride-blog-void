<section class="breadcrumb">
    <ul class="flex items-center justify-start">
        <li class="flex items-center justify-start">
            <a href="{{$all_categories_permalink}}" class="text-sm hover:text-coral-600 hover:underline transition duration-200 ease-in-out">
                All deal categories
            </a>
            @svg('chevron-right', 'h-4 w-4 mt-05 text-gray-400')
        </li>
        <li class="flex items-center justify-start">
            <a href="" class="text-sm hover:text-coral-600 hover:underline transition duration-200 ease-in-out">
                Dresses deals
            </a>

            @svg('chevron-right', 'h-4 w-4 mt-05 text-gray-400')
        </li>
        <li class="flex items-center justify-start">
            <a href="" class="text-sm hover:text-turquoise-600 hover:underline transition duration-200 ease-in-out">
                Copper & Confetti Co deals
            </a>
        </li>
    </ul>
</section>
