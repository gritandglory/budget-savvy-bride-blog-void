<article class="bg-white border-2 border-gray-100 rounded mb-8 transition duration-200 ease-in-out hover:shadow-lg hover:cursor-pointer hover:border-turquoise-300" id="deal-{{$deal['id']}}">
    <div class="flex w-full relative z-10">
        <div class="main-content flex flex-1 w-full">
            <div class="flex w-40 flex-col items-center p-4 bg-gray-50 border-r-2 border-gray-100">
                <div class="brand-logo relative z-0 w-full cursor-pointer rounded overflow-hidden">
                   @if($deal['brand_logo'])
                    <img
                        alt=""
                        src="{{$deal['brand_logo']['sizes']['sm-square']}}"
                        class="w-full block border-2 border-gray-100 rounded overflow-hidden">
                       @endif
                </div>
            </div>
            <div class="flex flex-1 flex-col items-start px-4 w-full h-full p-4">
                <div class="flex items-center">
                    <span class="flex-1 text-sm uppercase font-medium mt-0">{{$deal['brand_name']}}</span>
                </div>
                <div class="break-words text-lg md:text-xl text-gray-800 leading-snug mt-2"
                     style="word-break: break-word;">
                    {!! $deal['offer_title'] !!}

                </div>
            </div>
        </div>
        <div class="flex flex-col ml-auto w-60 p-4">
                @if($deal['type'] === 'coupon')
                <button class="flex overflow-y-visible focus:outline-none"
                        data-deal-id="{{$deal['id']}}"
                        data-deal-slug="{{$deal['url']}}">
                    <div
                        class="flex-1 btn-reveal bg-turquoise-500 hover:bg-turquoise-600 transition ease-in-out duration-200 transition-bg rounded px-4 h-12 text-white font-medium flex items-center justify-around relative">
                        <span class="my-auto text-base font-medium">Get Code</span>
                    </div>
                    <span
                        class="flex h-12 items-center border-2 border-dotted rounded-r border-turquoise-500 bg-turquoise-50 font-bold py-3 pl-2 pr-1 text-gray-800"><span
                            class="code-reveal-wrap text-sm block tracking-wide leading-tight uppercase">{{$deal['code_trimmed']}}</span>
                    </span>
                </button>
                @endif
                    @if($deal['type'] !== 'coupon')
                        <button class="flex items-center justify-center bg-turquoise-500 hover:bg-turquoise-600 transition ease-in-out duration-200 transition-bg rounded px-4 h-12 text-white font-medium focus:outline-none"
                                data-deal-id="{{$deal['id']}}"
                                data-deal-slug="{{$deal['url']}}">
                            <span class="flex justify-center items-center text-base font-medium"> Get <?php echo ucfirst($deal['type']) ?> @svg('chevron-right', 'h-4 w-4 ml-auto mt-05')</span>

                        </button>
                    @endif

            <div class="text-gray-700 text-xs font-medium mt-2">
                @isset($deal['expire'])
                    <span class="">Offer expires on: {{$deal['expire']}}</span>
                @endisset
            </div>
            <div class="flex items-start mt-auto">
                @if($deal['exclusive'])
                    <div
                        class="relative z-40 flex justify-center items-center border border-coral-500 rounded bg-coral-400 text-white px-2 h-8 mr-3">
                        @svg('check', 'h-4 w-4 ml-auto mt-05')
                        <span class="ml-2 text-sm font-semibold">Exclusive</span>
                    </div>
                @endif
                <div class="flex justify-center items-center border border-coral-200 rounded bg-coral-100 px-2 h-8">
                    @svg('check', 'h-4 w-4 ml-auto mt-05')
                    <span class="ml-2 text-sm font-medium"><?php echo ucfirst($deal['type']) ?></span>
                </div>
            </div>
        </div>
    </div>
{{--
    <div class="border-b border-gray-100 pb-6"></div>
--}}
</article>
