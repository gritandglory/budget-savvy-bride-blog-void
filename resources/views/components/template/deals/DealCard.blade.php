<div id="{{$deal['id']}}" class="flex flex-col lg:mb-0 rounded overflow-hidden first:mt-01">
    <div class="relative">
        @if($deal['exclusive'])
            <div
                class="absolute top-0 left-0 {{--mt-2 ml-2--}} px-2 h-6 z-40 flex justify-center items-center rounded-br bg-coral-500 text-white  mr-3">
                {{--  @svg('check', 'h-4 w-4 ml-auto mt-05')--}}
                <span class="text-xs font-medium">Exclusive</span>
            </div>
        @endif
        <a href="{{$deal['permalink']}}" class="group w-full">
            <figure class="overflow-hidden rounded w-full">

                @isset($deal['image']['id'])
                    @component('components.global.base.BaseImage', [
                        'scale-transform'   => true,
                        'id'    =>  $deal['image']['id'],
                        'src'   => $deal['image']['size']['card']['url'],
                        'alt'   => $deal['image']['size']['card']['alt'],
                    ])@endcomponent
                @endisset
            </figure>
        </a>
        @isset($deal['brand_logo']['url'])
            <div class="absolute bottom-0 right-0 mr-2 mb-2 rounded overflow-hidden bg-white">
                <img class="w-16 h-16 block" src="{{$deal['brand_logo']['url']}}" alt="{{$deal['offer_image']['alt']}}">
            </div>
        @endisset
    </div>
    <div class="flex flex-col pt-2 pb-1">
        @isset($deal['terms'])
            <div class="flex flex-no-wrap">
                @foreach($deal['terms'] as $term)
                    <a href="/wedding-deals/category/{{$term['slug']}}"
                       class="flex items-center text-coral-600 rounded font-medium text-xs h-4 mr-2 uppercase hover:underline">{{$term['name']}}</a>

                @endforeach
            </div>
        @endisset
        <a href="{{$deal['permalink']}}" class="font-medium text-lg hover:underline">
            {!! $deal['offer_title'] !!}
        </a>
    </div>
    <span class="block md:hidden border-b border-gray-200 mt-4"></span>
</div>
