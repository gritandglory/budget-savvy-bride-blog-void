<div id="{{$deal['id']}}" class="fixed inset-0 z-100 transition-opacity flex justify-center items-center"
     style="display: none">
    <div class="absolute z-40 inset-0 bg-black opacity-55" data-deal-close
         data-deal-id="{{$deal['id']}}"></div>
    <div class="max-w-2xl w-full relative z-50 bg-white rounded shadow transform transition-all">
        <div class="flex flex-col flex-start items-center">
            <button
                class="close-btn right-0 top-0 absolute text-gray rounded-full bg-white shadow-md p-1 transform translate-x-4 -translate-y-4">
                <svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" aria-label="close icon"
                     role="presentation" focusable="false" tabindex="-1"
                     class="w-4 h-4 p-1 text-gray fill-current stroke-current">
                    <path
                        d="M20 2.274L17.727 0 10 7.727 2.274 0 0 2.273 7.727 10 0 17.726l2.273 2.273L10 12.273 17.726 20l2.273-2.272L12.273 10z"
                    ></path>
                </svg>
            </button>
            <div class="grid grid-cols-3 border-b border-gray-200">
                <div class="deal-image col-span-1 rounded-tl overflow-hidden">
                    <img
                        alt="{{$deal['brand_logo']['alt']}}"
                        src="{{$deal['brand_logo']['url']}}"
                        class="w-full block border-r overflow-hidden">
                </div>
                <div class="deal-content col-span-2 flex flex-col justify-center items-center p-4">
                    <span class="text-sm uppercase font-medium mt-0">{{$deal['brand_name']}}</span>
                    <h3 class="font-medium mt-2">{!! $deal['offer_title'] !!}</h3>
                    {{--      <div class="text-center mb-4 inline"><strong>15% off</strong> Orders at bonprix
                          </div>--}}
                    <div class="w-full px-4 mt-2">
                        <div
                            class="relative border border-dashed p-3 border-turquoise-500 bg-turquoise-50 flex rounded mb-4">
                            <span class="font-bold block tracking-wide p-1 ml-2">{{$deal['code']}}</span>
                            <button
                                class="border-2 p-1 font-bold text-sm w-24 absolute bg-turquoise-500 text-white rounded transition-border transition-250 transition-ease-in-out right-0 mr-3 border-turquoise-500">
                                <span>Copy Code</span>
                            </button>
                        </div>
                        <a href="" target="_blank"
                           class="text-turquoise-600 font-medium text-center mx-auto flex justify-center items-center">
                            Click here to visit {{$deal['brand_name']}}
                            @svg('double-chevron-right', 'h-4 w-4 mx-2 mt-05')
                        </a>
                    </div>
                </div>
            </div>
            <div class="px-4 pt-3 pb-4 text-sm bg-gray-50 rounded-br rounded-bl overflow-hidden">
                <span class="text-sm font-medium">Details:</span>
                <div class="prose">
                    <p>{!! $deal['text'] !!}</p>
                </div>
            </div>


        </div>
    </div>
</div>
