<div x-data="{ rw_mobileFilter: false, filter: 'budget' }" xmlns:x-transition="http://www.w3.org/1999/xhtml">
    <div class="button-wrapper fixed bottom-0 inset-x-0 bg-white p-2 border-t border-gray-200 h-16">
        <button id="mobileFilterButton" class="btn btn-turquoise w-full py-2 font-medium"
                @click.prevent="rw_mobileFilter = !rw_mobileFilter"
        >Filters
        </button>
    </div>

    <div x-show="rw_mobileFilter"
         class="fixed bottom-0 inset-x-0">
        <div x-show="rw_mobileFilter"
             @click.prevent="rw_mobileFilter = false"
             x-transition:enter="ease-out duration-300"
             x-transition:enter-start="opacity-0"
             x-transition:enter-end="opacity-100"
             x-transition:leave="ease-in duration-100"
             x-transition:leave-start="opacity-100"
             x-transition:leave-end="opacity-0" class="fixed inset-0 transition-opacity"  style="display: none">
            <div class="absolute inset-0 bg-black opacity-55"></div>
        </div>
        <div x-show="rw_mobileFilter"
             x-transition:enter="ease-out duration-300"
             x-transition:enter-start="opacity-0 translate-y-2"
             x-transition:enter-end="opacity-100 translate-y-0"
             x-transition:leave="ease-in duration-100"
             x-transition:leave-start="opacity-100 translate-y-0"
             x-transition:leave-end="opacity-0 translate-y-2"
             class="w-full p-4"
             style="display: none"
             role="dialog" aria-modal="true" aria-labelledby="modal-headline">

            <div class="flex justify-end w-full mb-4">
                <button @click.prevent="rw_mobileFilter = false"
                        class="bg-white rounded-full px-4 py-1 relative z-50 ml-auto text-sm font-medium shadow-lg focus:outline-none outline-none">Close
                </button>
            </div>
            <div class="relative z-50 bg-white rounded overflow-hidden transform transition-all">
                @component('components.template.real-wedding-archive.filters.mobileFilter', [
                    'label' => 'Budget',
                    'hidden_id' => 'real_wedding_budget',
                    'display_id' => 'budget_display',
                    'filter' => 'budget'
                ])
                    <div class="relative overflow-y-scroll transition-all max-h-0 ease-in-out duration-300" style="display: none"
                         :style="filter == 'budget' ? 'max-height: ' + '16rem' : ''"
                         :class="{'border-b border-gray-200': filter == 'budget'}"
                    >
                        <ul
                        >
                            @foreach(RealWeddingTaxonomies::budgetTerms() as $term)
                                <li data-filter-type="budget"
                                    data-slug="<?php echo $term['slug'] ?>"
                                    data-name="<?php echo $term['name'] ?>"
                                    class="flex justify-center items-center w-full px-4 h-12 hover:bg-gray-50 text-sm border-b border-gray-100 last:border-b-0 hover:cursor-pointer">
                                    $<?php echo $term['name'] ?>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endcomponent
                @component('components.template.real-wedding-archive.filters.mobileFilter', [
                    'label' => 'Color',
                    'hidden_id' => 'real_wedding_color',
                    'display_id' => 'color_display',
                    'filter' => 'color'
                ])
                    <div class="relative overflow-y-scroll transition-all max-h-0 ease-in-out duration-300" style="display: none"
                         :style="filter == 'color' ? 'max-height: ' + '16rem' : ''"
                         :class="{'border-b border-gray-200': filter == 'color'}"
                    >
                        <ul
                        >
                            @foreach(RealWeddingTaxonomies::colorTerms() as $term)
                                <li data-filter-type="color"
                                    data-hex="<?php echo $term['hex'] ?>"
                                    data-slug="<?php echo $term['slug'] ?>"
                                    data-name="<?php echo $term['name'] ?>"
                                    class="flex justify-center w-full px-4 py-2 hover:bg-gray-50 text-sm border-b border-gray-100 last:border-b-0 hover:cursor-pointer">

                                    @if($term['slug'] != 'white')
                                        <span
                                            class="pointer-events-none h-8 px-4 flex justify-center items-center rounded text-white text-sm font-medium px-2"
                                            style="background-color: <?php echo $term['hex'] ?>"
                                            class="">  <?php echo $term['name'] ?></span>
                                    @endif
                                    @if($term['slug'] == 'white')
                                        <span
                                            class="pointer-events-none h-8 px-4 flex justify-center items-center border border-gray-200 rounded text-sm font-medium px-2"
                                            style="background-color: <?php echo $term['hex'] ?>"
                                            class="">  <?php echo $term['name'] ?></span>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endcomponent
                @component('components.template.real-wedding-archive.filters.mobileFilter', [
                    'label' => 'State',
                    'hidden_id' => 'real_wedding_state',
                    'display_id' => 'state_display',
                    'filter' => 'state'
                ])

                    <div class="relative overflow-y-scroll transition-all max-h-0 ease-in-out duration-300" style="display: none"
                         :style="filter == 'state' ? 'max-height: ' + '16rem' : ''"
                         :class="{'border-b border-gray-200': filter == 'state'}"
                    >
                        <ul
                        >
                            @foreach(RealWeddingTaxonomies::stateTerms() as $term)
                                <li data-filter-type="state"
                                    data-slug="<?php echo $term['slug'] ?>"
                                    data-name="<?php echo $term['name'] ?>"
                                    class="flex justify-center w-full px-4 py-4 hover:bg-gray-50 text-sm border-b border-gray-100 last:border-b-0 hover:cursor-pointer">
                                    <?php echo $term['name'] ?>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endcomponent
                @component('components.template.real-wedding-archive.filters.mobileFilter', [
                    'label' => 'Season',
                    'hidden_id' => 'real_wedding_season',
                    'display_id' => 'season_display',
                    'filter' => 'season'
                ])
                    <div class="relative overflow-y-scroll transition-all max-h-0 ease-in-out duration-300" style="display: none"
                         :style="filter == 'season' ? 'max-height: ' + '16rem' : ''"
                         :class="{'border-b border-gray-200': filter == 'season'}"
                    >

                        <ul class="sm:flex sm:items-start h-56 overflow-y-scroll">
                            @foreach(RealWeddingTaxonomies::seasonTerms() as $term)
                                <li data-filter-type="season"
                                    data-slug="<?php echo $term['slug'] ?>"
                                    data-name="<?php echo $term['name'] ?>"
                                    class="flex justify-center w-full px-4 py-4 hover:bg-gray-50 text-sm border-b border-gray-100 last:border-b-0 hover:cursor-pointer">
                                    <?php echo $term['name'] ?>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endcomponent

                <div class="bg-white border-t-4 border-turquoise-600">
                    <div class="flex flex-row p-2 h-16">
                        <a href="/real-weddings" class="w-3/12 btn btn-gray text-base font-medium">Clear</a>
                        <button type="submit" class="flex-1 ml-4 btn btn-coral w-full py-2 font-medium text-base">
                            Search
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
