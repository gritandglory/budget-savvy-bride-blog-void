<div x-data="{ showSeasonOptions: false }"
     class="wrapper relative mx-2 w-1/4"
     @mouseenter="showSeasonOptions = true"
     @mouseleave="showSeasonOptions = false"
>
    <div
        class="selector relative z-20 inline-flex justify-between items-center border border-gray-200 rounded h-10 overflow-hidden w-full">
        <div
            class="title font-medium text-base bg-gray-50 h-10 flex items-center px-3 border-r border-gray-200">
            Season:
        </div>
        <div class="h-full flex items-center hover:cursor-pointer w-1/3 bg-white w-full px-3">
        <div class="selected font-medium text-sm" id="season_display">
                <?php echo (isset($_GET['real_wedding_season'])) ? ucfirst($_GET['real_wedding_season']) : '' ?>
        </div>
            <div
                class="arrow ml-auto bg-gray-100 rounded-full w-5 h-5 flex justify-center items-center">
                @svg('chevron-down', 'w-4 h-4')
            </div>
        </div>
    </div>

    <div x-show="showSeasonOptions"
         style="display: none"
         class="dropdown-wrapper absolute w-full z-10 -mt-05 shadow-lg overflow-hidden border border-t-0 border-gray-200 rounded-br rounded-bl">
        <ul class="bg-white">
            @foreach(RealWeddingTaxonomies::seasonTerms() as $term)
                <li data-filter-type="season"
                    data-slug="<?php echo $term['slug'] ?>"
                    data-name="<?php echo $term['name'] ?>"
                    class="flex justify-end w-full px-4 py-2 hover:bg-gray-50 text-sm border-b border-gray-100 last:border-b-0 hover:cursor-pointer">
                    <?php echo $term['name'] ?>
                </li>
            @endforeach
        </ul>
    </div>
</div>
