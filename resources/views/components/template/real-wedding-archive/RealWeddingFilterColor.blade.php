<div x-data="{ showColorOptions: false }"
     class="wrapper relative mx-2 w-1/4"
     @mouseenter="showColorOptions = true"
     @mouseleave="showColorOptions = false"
>
    <?php
    use App\Controllers\RealWeddingTaxonomies;$search_hex = '#';
    $color_terms = RealWeddingTaxonomies::colorTerms();
    // function that finds the hex codes for the selected color
    if (isset($_GET['real_wedding_color']) && $_GET['real_wedding_color'] !== '') {
        $key = array_search($_GET['real_wedding_color'], array_column($color_terms, 'slug'));

        $search_hex = $color_terms[$key]['hex'];
        $is_white = 'text-white';
        if ($search_hex == '#ffffff') {
            $is_white = ' border border-gray-200 text-black';
        }
    }
    ?>
    <div
        class="selector relative z-20 inline-flex justify-between items-center border border-gray-200 rounded h-10 overflow-hidden w-full">
        <div
            class="title font-medium text-base bg-gray-50 h-10 flex items-center px-3 border-r border-gray-200">
            Color:
        </div>
        <div class="h-full flex items-center hover:cursor-pointer w-1/3 bg-white w-full px-3">
            <div class="selected">
            <span
                style="background-color: <?php echo $search_hex ?>"
                class="pointer-events-none h-6 px-2 flex justify-center items-center rounded text-sm font-medium
               <?php echo $is_white ?>
                    "
                id="color_display">
                  <?php echo (isset($_GET['real_wedding_color'])) ? ucfirst($_GET['real_wedding_color']) : '' ?>
            </span>
            </div>
            <div
                class="arrow ml-auto bg-gray-100 rounded-full w-5 h-5 flex justify-center items-center">
                @svg('chevron-down', 'w-4 h-4')
            </div>
        </div>
    </div>
    <div x-show="showColorOptions"
         style="display: none"
         class="dropdown-wrapper absolute w-full z-10 -mt-05 shadow-lg overflow-hidden border border-t-0 border-gray-200 rounded-br rounded-bl">
        <ul class="bg-white max-h-xs overflow-y-scroll">
            @foreach(RealWeddingTaxonomies::colorTerms() as $term)
                <li data-filter-type="color"
                    data-hex="<?php echo $term['hex'] ?>"
                    data-slug="<?php echo $term['slug'] ?>"
                    data-name="<?php echo $term['name'] ?>"
                    class="flex justify-end w-full px-4 py-2 hover:bg-gray-50 text-sm border-b border-gray-100 last:border-b-0 hover:cursor-pointer">

                    @if($term['slug'] != 'white')
                        <span
                            class="pointer-events-none h-6 px-2 flex justify-center items-center rounded text-white text-sm font-medium px-2"
                            style="background-color: <?php echo $term['hex'] ?>"
                            class="">  <?php echo $term['name'] ?></span>
                    @endif

                    @if($term['slug'] == 'white')
                        <span
                            class="pointer-events-none h-6 px-2 flex justify-center items-center border border-gray-200 rounded text-sm font-medium px-2"
                            style="background-color: <?php echo $term['hex'] ?>"
                            class="">  <?php echo $term['name'] ?></span>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
</div>
