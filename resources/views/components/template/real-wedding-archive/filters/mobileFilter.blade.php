<div>
    <div
        class="selector relative z-20 inline-flex justify-between items-center border-b border-gray-200 h-12 overflow-hidden w-full">

        <div
            class="title font-medium text-base bg-gray-50 h-12 w-32 flex items-center px-4 border-r border-gray-200">
            {{$label}}:
        </div>
        <button @click.prevent="filter = '{{$filter}}'"
                class="h-full flex items-center hover:cursor-pointer w-1/3 bg-white w-full px-3 focus:outline-none outline-none">
            @if($filter === 'budget')
                <div class="selected font-medium text-sm" id="m_{{$display_id}}">
                    <?php echo (isset($_GET['' . $hidden_id . '']) && $_GET['' . $hidden_id . ''] !== '') ? '$' . ucfirst($_GET['' . $hidden_id . '']) : '' ?>
                </div>
            @endif

            @if($filter === 'color')
                <div class="selected font-medium text-sm py-1 px-4 rounded" id="m_{{$display_id}}">
                    <?php echo (isset($_GET['' . $hidden_id . '']) && $_GET['' . $hidden_id . ''] !== '') ? ucfirst($_GET['' . $hidden_id . '']) : '' ?>
                </div>
            @endif

            @if($filter !== 'color')
                <div class="selected font-medium text-sm" id="m_{{$display_id}}">
                    <?php echo (isset($_GET['' . $hidden_id . '']) && $_GET['' . $hidden_id . ''] !== '') ?  ucfirst($_GET['' . $hidden_id . '']) : '' ?>
                </div>
            @endif
            <div
                class="arrow ml-auto bg-gray-100 rounded-full w-5 h-5 flex justify-center items-center">
                @svg('chevron-down', 'w-4 h-4')
            </div>
        </button>
    </div>
    {{$slot}}
</div>
