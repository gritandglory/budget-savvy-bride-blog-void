<div x-data="{ showBudgetOptions: false }"
     class="wrapper relative mr-2 w-1/4"
     @mouseenter="showBudgetOptions = true"
     @mouseleave="showBudgetOptions = false"
>
    <div
        class="selector relative z-20 inline-flex justify-between items-center border border-gray-200 rounded h-10 overflow-hidden w-full">
        <div
            class="title font-medium text-base bg-gray-50 h-10 flex items-center px-3 border-r border-gray-200">
            Budget:
        </div>
        <div class="h-full flex items-center hover:cursor-pointer w-1/3 bg-white w-full px-3">
            <div class="selected font-medium text-sm" id="budget_display">
               <?php echo (isset($_GET['real_wedding_budget']) && $_GET['real_wedding_budget'] !== '') ? '$' . ucfirst($_GET['real_wedding_budget']) : '' ?>
            </div>

            <div
                class="arrow ml-auto bg-gray-100 rounded-full w-5 h-5 flex justify-center items-center">
                @svg('chevron-down', 'w-4 h-4')
            </div>
        </div>
    </div>
    <div x-show="showBudgetOptions"
         style="display: none"
         class="dropdown-wrapper absolute w-full z-10 -mt-05 shadow-lg overflow-hidden border border-t-0 border-gray-200 rounded-br rounded-bl">
        <ul class="bg-white  max-h-xs overflow-y-scroll">
            @foreach(RealWeddingTaxonomies::budgetTerms() as $term)
                <li data-filter-type="budget"
                    data-slug="<?php echo $term['slug'] ?>"
                    data-name="<?php echo $term['name'] ?>"
                    class="flex justify-end w-full px-4 py-2 hover:bg-gray-50 text-sm border-b border-gray-100 last:border-b-0 hover:cursor-pointer">
                   $<?php echo $term['name'] ?>
                </li>
            @endforeach
        </ul>
    </div>
</div>
