<div class="wp-block-bsb-blocks-image-with-credit block-container img-block">
    <div class="grid grid-cols-1 col-gap-4 w-full">
        <figure class="wp-block-image" itemscope itemtype="http://schema.org/ImageObject">
            <div class="relative">
                <img class="wp-image-131181"
                     src="https://bsb-blog.test/app/uploads/2020/07/7bf9185b-4516-3e2e-a34d-1fc6dc819795.jpg"
                     alt="" aria-hidden="true" itemprop="contentUrl"/>
                <a href="https://example.com"
                   target="_blank"
                   class="photo-credit group absolute bottom-0 right-0 flex justify-start items-center  px-2 py-1"
                   rel="noopener noreferrer">
                    <div class="absolute right-0 w-full h-full bg-black opacity-50 z-0 rounded-br rounded-tl"></div>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 mr-2 relative text-white w-4 z-10"
                         viewbox="0 0 23 20" fill="none">
                        <path
                            d="M11.5 16.8304C8.52617 16.8304 6.10938 14.4286 6.10938 11.4732C6.10938 8.51786 8.52617 6.11607 11.5 6.11607C14.4738 6.11607 16.8906 8.51786 16.8906 11.4732C16.8906 14.4286 14.4738 16.8304 11.5 16.8304ZM11.5 8.25893C9.7166 8.25893 8.26562 9.70089 8.26562 11.4732C8.26562 13.2455 9.7166 14.6875 11.5 14.6875C13.2834 14.6875 14.7344 13.2455 14.7344 11.4732C14.7344 9.70089 13.2834 8.25893 11.5 8.25893ZM15.3947 5H20.8438V17.8571H2.15625V5H7.60527L8.6834 2.14286H14.3211L15.3947 5ZM14.5682 0H8.6834C7.78496 0 6.98086 0.553571 6.66641 1.38839L6.10938 2.85714H2.15625C0.96582 2.85714 0 3.81696 0 5V17.8571C0 19.0402 0.96582 20 2.15625 20H20.8438C22.0342 20 23 19.0402 23 17.8571V5C23 3.81696 22.0342 2.85714 20.8438 2.85714H16.8906L16.2482 1.16071C15.9877 0.459821 15.3184 0 14.5682 0Z"
                            fill="currentColor"></path>
                    </svg>
                    <span class="relative text-gray-200 text-xs z-10 z-50">James McShane</span>
                </a>
            </div>
        </figure>
    </div>
</div>
