@extends('layouts.sidebar')


@section('content')

    @foreach($press_loop as $press_item)

        <div class="press-{!! $press_item['id'] !!} grid grid-cols-5 mb-4 pb-4 border-b border-gray-100">

            <div class="col-span-2">

            </div>
            <div class="col-span-3">
                <h2 class="font-serif">
                    <a href="{{ $press_item['press_image_url'] }}">
                        {!! $press_item['press_link_text'] !!}
                    </a>

                </h2>
                <span class="text-sm">{{$press_item['date']}}</span>
            </div>
        </div>

    @endforeach

@endsection


