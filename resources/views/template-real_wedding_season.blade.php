{{--
  Template Name: Real Wedding - Season
--}}

@extends('layouts.real-wedding-taxonomy')

{{--
  Reall wedding taxonomy banner section
--}}
@section('banner')
    <section class="bg-gray-50 pt-6 pb-12 border-b border-gray-100">
        @include('components.global.page-title', ['title' => App::title(), 'subtitle' => RealWeddingPage::subTitle()])
        @component('components.global.base.BaseContainer')
            <ul class="flex justify-center items-center flex-wrap container mx-auto">
                @foreach(\App\Controllers\RealWeddingTaxonomies::seasonTerms() as $term)
                    <li class="p-2">
                        <a href="/real-weddings?real_wedding_season=<?php echo $term['slug'] ?>"
                           class="btn btn-turquoise btn-md text-sm">
                            @if($term['slug'] == 'fall')
                                @svg('autumn', 'h-5 w-5 mr-2')
                            @endif
                            @if($term['slug'] == 'summer')
                                @svg('summer', 'h-5 w-5 mr-2')
                            @endif
                            @if($term['slug'] == 'spring')
                                @svg('spring', 'h-5 w-5 mr-2')
                            @endif
                            @if($term['slug'] == 'winter')
                                @svg('winter', 'h-5 w-5 mr-2')
                            @endif
                            {{$term['name']}}
                        </a>
                    </li>
                @endforeach
            </ul>
        @endcomponent
    </section>
@endsection

{{--
  Reall wedding taxonomy page content
--}}
@section('content')
    @component('components.global.base.BaseContainer')
        @while(have_posts()) @php the_post() @endphp
        @include('partials.content-page')
        @endwhile
    @endcomponent
@endsection
