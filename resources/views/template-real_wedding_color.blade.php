{{--
  Template Name: Real Wedding - Color
--}}

@extends('layouts.real-wedding-taxonomy')

{{--
  Reall wedding taxonomy banner section
--}}
@section('banner')
    <section class="bg-gray-50 pt-6 pb-12 border-b border-gray-100">
        @include('components.global.page-title', ['title' => App::title(), 'subtitle' => RealWeddingPage::subTitle()])
        @component('components.global.base.BaseContainer')
            <ul class="flex justify-center items-center flex-wrap container mx-auto">
                @foreach(RealWeddingTaxonomies::colorTerms() as $term)
                    <li class="p-2">
                        @if($term['slug'] != 'white')
                            <a href="/real-weddings?real_wedding_color=<?php echo $term['slug'] ?>"
                               class="btn btn-md text-sm text-white"
                               style="background-color: <?php echo $term['hex'] ?>"
                               class="">  <?php echo $term['name'] ?>
                            </a>
                        @endif

                        @if($term['slug'] == 'white')
                            <a href="/real-weddings?real_wedding_color=<?php echo $term['slug'] ?>"
                               class="btn btn-md text-sm border border-gray-200"
                               style="background-color: <?php echo $term['hex'] ?>"
                               class="">  <?php echo $term['name'] ?>
                            </a>
                        @endif
                    </li>
                @endforeach

            </ul>
        @endcomponent
    </section>
@endsection

{{--
  Reall wedding taxonomy page content
--}}
@section('content')
    @component('components.global.base.BaseContainer')
        @while(have_posts()) @php the_post() @endphp
        @include('partials.content-page')
        @endwhile
    @endcomponent
@endsection
