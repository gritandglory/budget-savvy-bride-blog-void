{{--
  Template Name: Real Wedding - State
--}}

@extends('layouts.real-wedding-taxonomy')

{{--
  Reall wedding taxonomy banner section
--}}
@section('banner')
    <section class="pt-6 pb-12 border-b border-gray-100">
    @component('components.global.base.BaseContainer')
            @include('components.global.page-title', ['title' => App::title(), 'subtitle' => RealWeddingPage::subTitle()])

        <ul class="flex justify-center items-center flex-wrap container mx-auto">
            @foreach(RealWeddingTaxonomies::stateTerms() as $term)
                <li class="p-2">
                    <a href="/real-weddings?real_wedding_state=<?php echo $term['slug'] ?>"
                       class="btn btn-turquoise btn-md text-sm">
                        {{$term['name']}}
                    </a>
                </li>
            @endforeach
        </ul>
    @endcomponent
    </section>
@endsection

{{--
  Reall wedding taxonomy page content
--}}
@section('content')

        @while(have_posts()) @php the_post() @endphp
        @include('partials.content-page')
        @endwhile

@endsection
