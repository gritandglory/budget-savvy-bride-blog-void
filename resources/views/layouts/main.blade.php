@extends('layouts.app')

@section('core')

    <main id="content" class="main">
        @yield('content')
    </main>

@endsection
