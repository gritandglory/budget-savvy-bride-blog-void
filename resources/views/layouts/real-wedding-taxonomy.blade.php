@extends('layouts.app')

@section('core')
    @yield('banner')
    @component('components.global.layout.page-wrap',  ['class' => 'grid grid-cols-4 col-gap-10'])

        <main id="content" class="main col-span-3 mr-4 flex">
            @yield('content')
        </main>
        <aside id="sidebar" class="col-span-1">
            @include('partials.sidebar')
        </aside>

    @endcomponent
@endsection
