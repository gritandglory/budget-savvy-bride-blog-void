@extends('layouts.app')

@section('core')

    @component('components.global.base.BaseContainer')
        @component('components.global.layout.page-wrap', ['class' => 'grid grid-cols-12 col-gap-10'])
            <main id="content" class="main col-span-9">
                @yield('content')
            </main>
            <aside id="sidebar" class="col-span-3">
                @include('partials.sidebar')
            </aside>
        @endcomponent
    @endcomponent
@endsection
