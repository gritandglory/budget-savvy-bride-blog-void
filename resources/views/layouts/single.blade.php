@extends('layouts.app')

@section('core')
    @component('components.global.base.BaseContainer')
    @component('components.global.layout.page-wrap', ['class' => 'grid grid-cols-4 col-gap-8'])
        <main id="content" class="main col-span-3 mr-4 grid grid-cols-12 col-gap-10">
            <div class="social-share-sticky ml-0 col-span-1">
                @include('components.global.social-share')
            </div>
            <div class="w-192 mx-auto col-span-11">
                @yield('content')
            </div>
        </main>
        <aside id="sidebar" class="col-span-1">
            @include('partials.sidebar')
        </aside>
    @endcomponent
    @endcomponent
@endsection
