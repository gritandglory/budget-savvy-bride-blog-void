@extends('layouts.app')

@section('core')

    @component('components.global.base.BaseContainer')
        @component('components.global.layout.page-wrap', ['class' => 'grid grid-cols-12 col-gap-10'])

            @if($share)
            <div class="social-share-sticky ml-auto col-span-1 ">
                @include('components.global.social-share')
            </div>
            @endif
            <main id="content" class="main w-182 mx-auto <?php echo $share  ? 'col-span-8' : 'col-span-9' ?>" >
                @yield('content')
            </main>
            <aside id="sidebar" class="col-span-3">
                @include('partials.sidebar')
            </aside>
        @endcomponent
    @endcomponent
@endsection
