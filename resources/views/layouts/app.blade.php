<!doctype html>
<html {!! get_language_attributes() !!}>
@include('partials.head')
<body id="app" @php body_class() @endphp>
@php do_action('get_header') @endphp
@include('partials.header')
@yield('core')
@php do_action('get_footer') @endphp
@include('partials.footer')
<script>
    function mobileMenu() {
        return {
            show: false,
            open() {
                this.show = true, fixedBody()
            },
            close() {
                this.show = false, fixedBody()
            },
            isOpen() {
                return this.show === true
            }
        }
    }

    function fixedBody() {
        document.getElementById('app').classList.toggle('is-fixed-menu');
    }
</script>
@yield('scripts')
@php wp_footer() @endphp
</body>
</html>
