@extends('layouts.app')

@section('core')

    @component('components.global.base.BaseContainer')
        @component('components.global.layout.page-wrap', ['class' => ''])
            <main id="content" class="main">
                @yield('content')
            </main>
        @endcomponent
    @endcomponent
@endsection
