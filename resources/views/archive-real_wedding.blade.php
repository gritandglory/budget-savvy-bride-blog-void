@extends('layouts.main')


@section('content')

    <div class="header-wrapper mt-4">
        @component('components.global.page-title', [
        'title' => $option_title,
        'subtitle' => $option_subtitle
        ])@endcomponent

        @component('components.global.base.BaseContainer')
            <div class="md:border-b md:border-gray-200 md:pb-8 mt-8">
                <form action="/real-weddings/" method="get" class="relative z-50">
                    <input type="hidden" name="real_wedding_budget" id="real_wedding_budget"
                           value="<?php echo (isset($_GET['real_wedding_budget'])) ? $_GET['real_wedding_budget'] : '' ?>"
                    />
                    <input type="hidden" id="real_wedding_color" name="real_wedding_color"
                           value="<?php echo (isset($_GET['real_wedding_color'])) ? $_GET['real_wedding_color'] : null ?>"
                    />
                    <input type="hidden" name="real_wedding_state" id="real_wedding_state"
                           value="<?php echo (isset($_GET['real_wedding_state'])) ? $_GET['real_wedding_state'] : '' ?>"
                    />
                    <input type="hidden" name="real_wedding_season" id="real_wedding_season"
                           value="<?php echo (isset($_GET['real_wedding_season'])) ? $_GET['real_wedding_season'] : '' ?>"
                    />
                    <div class="w-full hidden md:flex flex-col lg:flex-row justify-between w-full">
                        <div class="lg:w-10/12 w-full flex justify-between">
                            @include('components.template.real-wedding-archive.RealWeddingFilterBudget')
                            @include('components.template.real-wedding-archive.RealWeddingFilterColor')
                            @include('components.template.real-wedding-archive.RealWeddingFilterState')
                            @include('components.template.real-wedding-archive.RealWeddingFilterSeason')
                        </div>
                        <div class="ml-auto lg:flex-1 flex justify-end mt-4 lg:mt-0 w-full">
                            <a href="/real-weddings" class="btn btn-gray btn-lg font-medium mr-4">Clear</a>
                            <button type="submit" class="btn btn-coral btn-lg font-medium">Search</button>
                        </div>
                    </div>
                    <div class="md:hidden flex-1">
                        @include('components.template.real-wedding-archive.RealWeddingFiltersMobile')
                    </div>
                </form>
            </div>
        @endcomponent
    </div>
    @component('components.global.base.BaseContainer', ['class' => 'mt-8 md:mt-8 px-2 lg:px-4'])

        @if (!have_posts())
            <div class="alert alert-warning">
                {{ __('Sorry, no results were found.', 'sage') }}
            </div>
        @endif

        <div
            class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-4 xl:col-gap-6 row-gap-10 md:mt-8 relative z-10">
            @while (have_posts()) @php the_post() @endphp
            @include('partials.content-'.get_post_type())
            @endwhile
        </div>

        @include('components.global.pagination')

    @endcomponent

    @component('components.global.base.BaseContainer')
        <div class="grid grid-cols-1 md:grid-cols-2 gap-4 md:border-t md:border-gray-00 pt-8 md:mt-8">
            <div>
                @if(strlen($option_content ) > 0)
                    <h3 class="font-medium">View real weddings on a budget</h3>
                    <div class=" text-sm text-gray-800 prose">{!! $option_content !!}</div>
                @endif
            </div>
            <div>
                @if(isset($term_descriptions['budget']) && $term_descriptions['budget']['description']!== '')
                    <h3 class="font-medium">Real weddings on a {{$term_descriptions['budget']['slug']}} budget</h3>
                    <p class="max-w-2xl mx-auto text-sm text-gray-800">{{$term_descriptions['budget']['description']}}</p>
                @endif
                @if(isset($term_descriptions['color']) && $term_descriptions['color']['description'] !== '')
                    <h3 class="mt-4 font-medium">{{$term_descriptions['color']['slug']}} weddings</h3>
                    <p class="max-w-2xl mx-auto text-sm text-gray-800">{{$term_descriptions['color']['description']}}</p>
                @endif
                @if(isset($term_descriptions['state']['description']))
                    <h3 class="mt-4 font-medium">Budget weddings in {{$term_descriptions['state']['slug']}}</h3>
                    <p class="max-w-2xl mx-auto text-sm text-gray-800">{{$term_descriptions['state']['description']}}</p>
                @endif
                @if(isset($term_descriptions['season']['description']))
                    <h3 class="mt-4 font-medium">View {{$term_descriptions['season']['slug']}} weddings on a budget</h3>
                    <p class="max-w-2xl mx-auto text-sm text-gray-800">{{$term_descriptions['season']['description']}}</p>
                @endif
            </div>


        </div>

    @endcomponent

@endsection


