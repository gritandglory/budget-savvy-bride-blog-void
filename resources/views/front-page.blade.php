{{--
  Template Name: Front Page Template
--}}

@extends('layouts.main')

@section('content')

    @component('components.global.advert.HeaderAdvertisement')@endcomponent
    <button onclick="do_something()">On Click</button>
    <button onclick="do_something(this)">Click here.</button>
    <p>This text will change when you click the button.</p>
    @component('components.global.base.BaseFullWidth')
        @component('components.global.base.BaseContainer')

            <div class="grid lg:grid-cols-12 col-gap-8 ">
                <div class="col-span-6">
                    <div class="rounded overflow-hidden block">
                        <img class="w-full" src="https://cdn2.thebudgetsavvybride.com/demo-images/landscape_2.jpeg"
                             alt="">
                    </div>
                    <div class="flex flex-col justify-start items-start">
                        @component('components.global.base.BaseCategory', ['category' => 'Real Weddings'])@endcomponent

                        <h2 class="font-serif text-2xl">Eco-Friendly Gifts For Groomsmen</h2>
                        <p class="text-base mt-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto
                            delectus ducimus
                            fugiat harum id illo illum, inventore laudantium maxime mollitia officia porro, possimus
                            quis
                            recusandae unde velit veniam vero voluptas? Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit. Accusamus architecto.</p>
                    </div>
                </div>
                <div class="col-span-6">
                    <div class="grid sm:grid-cols-2 gap-8">

                        @component('components.global.base.BaseArticleCard',[
                            'src'       => 'https://cdn2.thebudgetsavvybride.com/demo-images/landscape_3.jpeg',
                            'category'  => 'Florida Wedding',
                            'title'     => 'Picturesque and Fun Elopement in California for $6K'
                        ])@endcomponent

                        @component('components.global.base.BaseArticleCard',[
                            'src'       => 'https://cdn2.thebudgetsavvybride.com/demo-images/landscape_4.jpeg',
                            'category'  => '10k Budget',
                            'title'     => 'How to Stick to a Budget While Planning a Coronavirus Wedding'
                        ])@endcomponent

                        @component('components.global.base.BaseArticleCard',[
                            'src'       => 'https://cdn2.thebudgetsavvybride.com/demo-images/landscape_1.jpeg',
                            'category'  => 'Real Weddings',
                            'title'     => 'Minimalist Eco-Friendly Wedding in Texas for $18K'
                        ])@endcomponent

                        @component('components.global.base.BaseArticleCard',[
                            'src'       => 'https://cdn2.thebudgetsavvybride.com/demo-images/landscape_5.jpeg',
                            'category'  => 'Real Weddings',
                            'title'     => 'Using Faux Flowers vs. Fresh Blooms for Your Wedding'
                        ])@endcomponent
                    </div>
                </div>
            </div>
        @endcomponent
    @endcomponent

    @component('components.global.base.BaseFullWidth', ['class' => 'bg-coral-100'])
        @component('components.global.base.BaseContainer')

            @component('components.SectionTitle', [
	           'title'     => 'Plan your wedding the savvy way',
	            'subtitle'     => 'Wedding planning doesn\'t have to be complicated. We\'ve outlined a simple step-by-step process to keep you on-track and on-budget.'
            ])@endcomponent

            <div class="grid grid-cols-1 md:grid-cols-4 gap-10 pb-4">
                <a href="#" class="relative flex flex-col justify-center items-center text-center group">
                    <div
                        class="absolute text-coral-200 font-bold inset-x-0 z-10 transform scale-100 group-hover:scale-110 group-hover:text-coral-300 transition transition-all duration-300 ease-in-out"
                        style="font-size: 10rem">1
                    </div>
                    <div class="relative z-30">
                        <h3 class="font-serif text-3xl">Prep</h3>
                        <p class="text-base">Get started with our wedding planning checklist and timeline.</p>
                    </div>
                </a>
                <a href="#" class="relative flex flex-col justify-center items-center text-center group">
                    <div
                        class="absolute text-coral-200 font-bold inset-x-0 z-10 transform scale-100 group-hover:scale-110 group-hover:text-coral-300 transition transition-all duration-300 ease-in-out"
                        style="font-size: 10rem">2
                    </div>
                    <div class="relative z-30">
                        <h3 class="font-serif text-3xl">Plan</h3>
                        <p class="text-base">Do your research, browse wedding inspiration, interview vendors, etc. </p>
                    </div>
                </a>
                <a href="#" class="relative flex flex-col justify-center items-center text-center group">
                    <div
                        class="absolute text-coral-200 font-bold inset-x-0 z-10 transform scale-100 group-hover:scale-110 group-hover:text-coral-300 transition transition-all duration-300 ease-in-out"
                        style="font-size: 10rem">3
                    </div>
                    <div class="relative z-30">
                        <h3 class="font-serif text-3xl">Pick</h3>
                        <p class="text-base">Hire your vendors. Make attire, stationery, decor and menu selections. </p>
                    </div>
                </a>
                <a href="#" class="relative flex flex-col justify-center items-center text-center group">
                    <div
                        class="absolute text-coral-200 font-bold inset-x-0 z-10 transform scale-100 group-hover:scale-110 group-hover:text-coral-300 transition transition-all duration-300 ease-in-out"
                        style="font-size: 10rem">4
                    </div>
                    <div class="relative z-30">
                        <h3 class="font-serif text-3xl"> Party!</h3>
                        <p class="text-base">All the final logistics to help you pull off your dream day without a
                            hitch!</p>
                    </div>
                </a>
            </div>

        @endcomponent
    @endcomponent

    @component('components.global.base.BaseContainer')

        @component('components.SectionTitle', [
           'title'     => 'Browse Popular Posts',
        ])@endcomponent

        @component('components.CardSectionBlock')

            @component('components.global.base.BaseArticleCard',[
               'src'       => 'https://cdn2.thebudgetsavvybride.com/assets/img/post-single/non-traditional-venue.jpg',
               'link'      => 'wedding-budget-tip-31-choose-non-traditional-venue',
               'category'  => 'Wedding Decor Ideas',
               'title'     => 'Things to Consider When Choosing A Non-Traditional Venue'
           ])@endcomponent

            @component('components.global.base.BaseArticleCard',[
               'src'       => 'https://cdn2.thebudgetsavvybride.com/assets/img/post-single/dual-purpose-venue.jpg',
               'link'      => 'wedding-budget-tip-32-venues-that-accommodate-ceremony-and-reception/',
               'category'  => 'Wedding Dresses',
               'title'     => 'Save Time and Money with a Dual-Purpose Venue'
           ])@endcomponent

            @component('components.global.base.BaseArticleCard',[
               'src'       => 'https://cdn2.thebudgetsavvybride.com/assets/img/post-single/all-inclusive-venues.jpg',
               'link'      => 'wedding-budget-tip-32-venues-that-accommodate-ceremony-and-reception/',
               'category'  => 'DIY Wedding Flowers',
               'title'     => 'All-Inclusive vs Raw Space Venues'
           ])@endcomponent

            @component('components.global.base.BaseArticleCard',[
                'src'       => 'https://cdn2.thebudgetsavvybride.com/assets/img/post-single/unique-and-affordable-venues-for-your-wedding.jpg',
                'link'      => 'how-to-find-affordable-unique-wedding-venues/',
                'category'  => 'Wedding Budget',
                'title'     => 'Tips for Finding the Perfect Wedding Venue'
            ])@endcomponent

        @endcomponent

    @endcomponent

@endsection
