<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="preload" href="https://cdn2.thebudgetsavvybride.com/assets/webfonts/tt-norms/TTNorms-Regular.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="https://cdn2.thebudgetsavvybride.com/assets/webfonts/tt-norms/TTNorms-Medium.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="https://cdn2.thebudgetsavvybride.com/assets/webfonts/tt-norms/TTNorms-MediumItalic.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href=" https://cdn2.thebudgetsavvybride.com/assets/webfonts/serif/bsb-serif.woff2" as="font" type="font/woff2" crossorigin>
    @php wp_head() @endphp
</head>
