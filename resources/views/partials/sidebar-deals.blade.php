<div class="bg-white w-full lg:mb-6 mb-4 overflow-hidden">

    <h2 class="text-lg md:text-xl font-medium mb-1">
        Popular Categories
    </h2>
    <p class="mb-5">
        More wedding codes, discounts and vouchers for
        <?php echo date("F Y") ?>
    </p>
    <div class="flex flex-wrap">
        @foreach($categories as $category)
            <a href="{{$category['permalink']}}"
               class="transition-bg transition-250 transition-ease-in-out bg-coral-100 hover:bg-coral-500 text-coral-900 hover:text-white rounded mr-2 mb-2 cursor-pointer text-xs hover:no-underline block p-2 leading-tight">
                {{$category['name']}}
            </a>
        @endforeach
    </div>
</div>
<div class="bg-white w-full lg:mb-6 mb-4 overflow-hidden">
    <h2 class="text-lg md:text-xl font-medium mb-1">
        Popular Brands
    </h2>
    <p class="mb-5">
        More wedding codes, discounts and vouchers for
        <?php echo date("F Y") ?>
    </p>
    <div class="flex flex-wrap">
        @foreach($brands as $brand)
            <a href="{{$brand['permalink']}}"
               class="transition-bg transition-250 transition-ease-in-out bg-turquoise-100 hover:bg-turquoise-500 text-turquoise-900 hover:text-white rounded mr-2 mb-2 cursor-pointer text-xs hover:no-underline block p-2 leading-tight">
                {{$brand['name']}}
            </a>
        @endforeach
    </div>
</div>
