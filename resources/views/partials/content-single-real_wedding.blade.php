<article>

    <header class="article-header">
        @include('components.global.post-single.preamble', ['sponsored' => $sponsored])
        @include('components.global.post-single.title', ['title' => $title])
        @include('components.global.post-single.meta', [
            'author_name'   => $author_name,
            'published'     => $published,
            'modified'      => $modified
        ])
        <div class="mt-4 px-6 pt-4 pb-6 bg-turquoise-50 border border-turquoise-100 rounded">
            <span class="font-serif">A note from the editors:</span>
            <p class="mt-1">
                {!! $excerpt !!}
            </p>
        </div>
        <div class="mt-6">
            <div class="rounded-tr rounded-tl overflow-hidden relative">
                @if ( has_post_thumbnail())
                    <?php the_post_thumbnail('large'); ?>
                @endif
                @if($thumbnail_credit_url)
                    <a href="{{$thumbnail_credit_url}}"
                       class="photo-credit group absolute bottom-0 right-0 flex justify-start items-center  px-2 py-1"
                       target="_blank">
                        <div class="absolute right-0 w-full h-full bg-black opacity-50 z-0 rounded-tl"></div>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 mr-2 relative text-white w-4 z-10"
                             viewBox="0 0 23 20" fill="none">
                            <path
                                d="M11.5 16.8304C8.52617 16.8304 6.10938 14.4286 6.10938 11.4732C6.10938 8.51786 8.52617 6.11607 11.5 6.11607C14.4738 6.11607 16.8906 8.51786 16.8906 11.4732C16.8906 14.4286 14.4738 16.8304 11.5 16.8304ZM11.5 8.25893C9.7166 8.25893 8.26562 9.70089 8.26562 11.4732C8.26562 13.2455 9.7166 14.6875 11.5 14.6875C13.2834 14.6875 14.7344 13.2455 14.7344 11.4732C14.7344 9.70089 13.2834 8.25893 11.5 8.25893ZM15.3947 5H20.8438V17.8571H2.15625V5H7.60527L8.6834 2.14286H14.3211L15.3947 5ZM14.5682 0H8.6834C7.78496 0 6.98086 0.553571 6.66641 1.38839L6.10938 2.85714H2.15625C0.96582 2.85714 0 3.81696 0 5V17.8571C0 19.0402 0.96582 20 2.15625 20H20.8438C22.0342 20 23 19.0402 23 17.8571V5C23 3.81696 22.0342 2.85714 20.8438 2.85714H16.8906L16.2482 1.16071C15.9877 0.459821 15.3184 0 14.5682 0Z"
                                fill="currentColor"/>
                        </svg>
                        <span class="relative text-gray-200 text-xs z-10 z-50">{{$thumbnail_credit_name}}</span>
                    </a>
                @endif
            </div>
            <div class="border border-gray-200 rounded-bl rounded-br overflow-hidden flex">
                <div class="flex-1 pl-4 py-4">
                    @isset($couple_name_formatted)
                        <h2 class="font-serif text-3xl leading-none">{{$couple_name_formatted}}</h2>
                    @endisset

                    @isset($couple_name_formatted)
                        <div class="text-sm mt-1">{{$wedding_date_formatted}}</div>
                    @endisset

                    <div class="text-sm font-medium">
                        @isset($location_city){{$location_city}}@endisset
                        @isset($location_state), {{$location_state}}@endisset
                    </div>
                    <div class="text-sm  flex flex-wrap items-start leading-none">
                        @isset($venue_only)
                            <div class="flex flex-col mt-2">
                                <span class="text-xs text-gray-900">Venue</span>
                                <a class="underline flex text-turquoise-600 font-medium leading-none mt-1"
                                   href="{!! $venue_only['url'] !!}"
                                   target="_blank">
                                    {!! $venue_only['name'] !!} @svg('double-chevron-right', 'w-3 h-3 ml-1 mt-05')
                                </a>
                            </div>
                        @endisset
                        @isset($ceremony_venue['name'])
                            <div class="flex flex-col mt-2 min-w-xxxs mr-4">
                                <span class="text-xs text-gray-900">Ceremony Venue</span>
                                <span class="flex">
                                         <a class="flex underline text-turquoise-600 font-medium leading-none mt-1"
                                            href="{!! $ceremony_venue['url'] !!}"
                                            target="_blank">
                                        {!! $ceremony_venue['name'] !!}
                                        @svg('double-chevron-right', 'w-3 h-3 ml-1 mt-05')
                                    </a>
                                    </span>
                            </div>
                        @endif
                        @isset($reception_venue['name'])
                            <div class="flex flex-col mt-2 min-w-xxxs">
                                <span class="text-xs text-gray-900">Reception Venue</span>
                                <a class="underline flex text-turquoise-600 font-medium leading-none mt-1"
                                   style="text-overflow: ellipsis;" href="{!! $reception_venue['url'] !!}"
                                   target="_blank">
                                    {!! $reception_venue['name'] !!} @svg('double-chevron-right', 'w-3 h-3 ml-1
                                    mt-05')
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
                <div
                    class="w-68 ml-auto flex flex-col justify-around items-center bg-gray-50 px-4 py-2 border-l border-gray-200">
                    <span class="">Total wedding budget</span>
                    <span class="font-medium">
                            @isset($total_budget)
                            {{$total_budget}}
                        @endisset
                        </span>
                    @if($set_vendors['count'] > 0)
                        <a href="#budget-breakdown" class="btn btn-turquoise btn-md flex-row items-center mt-1">View
                            budget
                            breakdown
                            @svg('chevron-down', 'w-4 h-4 mt-05 ml-auto')
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </header>
    <div class="article-content mt-6 prose">
        @php the_content() @endphp
    </div>
    <div class="budget-info">
        @if($set_vendors['count'] > 0)
            <div id="budget-breakdown" class="mt-6">

                <h3 class="h3 font-serif text-2xl">Budget Breakdown</h3>
                <div class="grid grid-cols-7 border border-turquoise-200 rounded overflow-hidden">
                    <ul class="col-span-2 border-r border-turquoise-200 bg-turquoise-50">
                        @foreach($set_vendors['items'] as $vendor)
                            @if($vendor['amount_spent'] !== 0)
                                <li class="flex justify-between text-base items-center leading-none h-10 px-2 border-turquoise-200 first:border-t-0 border-t">
                                    <span class="font-medium mr-2">{!! $vendor['title'] !!}:
                                        @if(!$vendor['url'] && !$vendor['name'])
                                            *
                                        @endif
                                    </span>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                    <ul class="col-span-5">
                        @foreach($set_vendors['items'] as $vendor)
                            @if($vendor['amount_spent'] !== 0)
                                <li class="flex justify-start text-base items-center leading-none h-10 border-turquoise-200 first:border-t-0 border-t">
                                    <div class="h-1 bg-turquoise-500 w-full rounded-r-full"
                                         style="width: {!! $vendor['percentage_budget'] !!}%"></div>
                                    <div class="leading-none ml-2 text-sm">{!! $vendor['percentage_budget'] !!}%
                                    </div>
                                    <div class="font-medium ml-auto pr-2">{!! $vendor['amount_spent'] !!}</div>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <span class="block text-xs text-gray-600">* the couple did not supply a vendor, or they DIY'd this aspect.</span>
            </div>
        @endif
        <div class="py-4">
            <h3 class="h3 font-serif text-2xl">Vendor Team</h3>
            <ul class="grid grid-cols-3 col-gap-4 row-gap-2">
                @foreach($set_vendors['items'] as $vendor)
                    @if($vendor['url'] && $vendor['name'])
                        <li class="flex justify-start items-start leading-none my-1 flex-col">
                            <span class="font-medium mr-2 text-sm">{!! $vendor['title'] !!}:</span>
                            <a class="relative text-turquoise-700 font-medium text-base external-link-icon hover:underline"
                               target="_blank"
                               href="{!! $vendor['url'] !!}">{!! $vendor['name'] !!}
                            </a>


                        </li>
                    @endif
                    @if(!$vendor['url'] && $vendor['name'])
                        <li class="flex justify-start items-start leading-none my-1 flex-col">
                            <span class="font-medium mr-2 text-sm">{!! $vendor['title'] !!}:</span>
                            <span class="flex items-start leading-none font-medium mr-4 mt-1 text-base">
                                       {!! $vendor['name'] !!}
                                    </span>
                        </li>
                    @endif

                @endforeach
            </ul>
        </div>
    </div>
    <div class="bg-coral-50 mt-6 py-12 flex flex-col justify-center items-center border border-coral-100 rounded">
        <h3 class="font-serif text-2xl">Our emails are pretty too</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        <form id="newsletter-form" action="#"
              class="flex justify-between max-w-lg w-full border-2 mt-4 rounded border-coral-500 p-1 bg-white">
            <input type="email" placeholder="test@test.com" class="w-full px-2 outline-none placeholder-black">
            <input type="submit" class="btn btn-coral btn-lg">
        </form>
    </div>
    <footer class="article-footer">
        @include('components.template.single.article-author', [
            'author_profile_image'  =>      $author_profile_image,
            'name'                  =>      $author_name,
            'description'           =>      $author_description,
        ])
    </footer>

</article>
