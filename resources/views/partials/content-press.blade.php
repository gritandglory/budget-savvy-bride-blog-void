<div class="press-<?php the_ID(); ?> grid grid-cols-5">
    <div class="col-span-2">
        <?php
        $image = get_field('press_image');
        if( !empty($image) ): ?>
        <img src="<?php echo $image['url']; ?>" alt=""/>
        <?php endif; ?>
    </div>
    <div class="col-span-3">
        <h2 class="font-serif">
            <a href="<?php echo the_field('press_image_url'); ?>"><?php the_field('press_link_text'); ?></a>
        </h2>
        <span class="text-sm"><?php the_field('press_date'); ?></span>
    </div>
</div>
