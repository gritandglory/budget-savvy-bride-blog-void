<section class="social-icon-share pb-2">
    <h3 class="text-center text-base">Follow Us</h3>
    <ul class="mt-2 flex w-full justify-around">
        <li><a href="https://www.facebook.com/budgetsavvybride" target="_blank" rel="noopener">
                @svg('facebook', 'h-6 w-6 text-coral-500')

            </a></li>
        <li><a href="https://twitter.com/savvybride" target="_blank" rel="noopener">
                @svg('twitter', 'h-6 w-6 text-coral-500')
            </a></li>
        <li><a href="http://www.pinterest.com/savvybride" target="_blank" rel="noopener">
                @svg('pinterest', 'h-6 w-6 text-coral-500')
            </a></li>
        <li><a href="https://www.instagram.com/budgetsavvybride/" target="_blank" rel="noopener">
                @svg('instagram', 'h-6 w-6 text-coral-500')
            </a></li>
        <li><a href="https://www.bloglovin.com/blogs/budget-savvy-bride-830303" target="_blank" rel="noopener">
                @svg('bloglovin', 'h-6 w-6 text-coral-500')
            </a></li>
    </ul>
</section>
@php dynamic_sidebar('sidebar-primary') @endphp
