<header id="main_nav" class="w-full border-b border-gray-200" xmlns:x-bind="http://www.w3.org/1999/xhtml"
        xmlns:x-transition="http://www.w3.org/1999/xhtml">
    <!--
    Ignore -- future development in the wordpress
    <div class="bg-coral-500 w-full relative top-0 h-10">
    <div class="w-full h-10 xl:max-w-menu mx-auto flex justify-center items-center">
    <p class="text-sm text-white">New Customer Offer: Enjoy 20% OFF your Minted Weddings order with our exclusive code: BUDGETSAVVYBRIDE20.</p>
    <button class="bg-coral-300 text-coral-900 text-xs font-medium rounded px-3 py-1 ml-4">Shop Now</button>
    </div>
    </div>
    -->
    <div x-data="mobileMenu()" class="w-full xl:max-w-menu mx-auto flex justify-between h-16 px-4 lg:pr-0 xl:px-4">
        <div class="w-full lg:mx-0 flex flex-1 justify-between items-center lg:mr-8">
            <div class="flex lg:hidden justify-end items-center -ml-2 lg:ml-0">
                <button class="inline-flex items-center justify-center p-2 bg-coral-100 rounded text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                    @svg('search', 'h-5 w-5 text-coral-900')
                </button>
            </div>
            <a href="/" class="logo">
                @component('components.global.logo') @endcomponent
            </a>
            <div class="flex lg:hidden justify-end items-center -mr-2 lg:mr-0">
            <button @click="open" type="button"
                    class="inline-flex items-center justify-center p-2 bg-turquoise-100 rounded text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                @svg('menu', 'text-turquoise-800')
            </button>
            </div>
        </div>
        <div x-description="Mobile menu, show/hide based on mobile menu state." x-show="isOpen()" @click.away="close"
             x-transition:enter="duration-200 ease-out"
             x-transition:enter-start="opacity-0 scale-95"
             x-transition:enter-end="opacity-100 scale-100"
             x-transition:leave="duration-100 ease-in"
             x-transition:leave-start="opacity-100 scale-100"
             x-transition:leave-end="opacity-0 scale-95"
             class="absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden z-100"
             style="display: none;">
            <div class="rounded-lg shadow-lg">
                <div class="rounded-lg shadow-xs bg-white divide-y-2 divide-gray-50">
                    <div class="pt-5 pb-6 px-5 space-y-6">
                        <div class="flex items-center justify-between">
                            <div>
                                <img class="h-8 w-auto" src="/img/logos/workflow-mark-on-white.svg" alt="Workflow">
                            </div>
                            <div class="-mr-2">
                                <button @click="close" type="button"
                                        class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                                    <svg class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M6 18L18 6M6 6l12 12"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <div>
                            <nav class="grid row-gap-8">
                                <a href="#"
                                   class="-m-3 p-3 flex items-center space-x-3 rounded-md hover:bg-gray-50 transition ease-in-out duration-150">
                                    <svg class="flex-shrink-0 h-6 w-6 text-indigo-600" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z"></path>
                                    </svg>
                                    <div class="text-base leading-6 font-medium text-gray-900">
                                        Analytics
                                    </div>
                                </a>
                                <a href="#"
                                   class="-m-3 p-3 flex items-center space-x-3 rounded-md hover:bg-gray-50 transition ease-in-out duration-150">
                                    <svg class="flex-shrink-0 h-6 w-6 text-indigo-600" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M15 15l-2 5L9 9l11 4-5 2zm0 0l5 5M7.188 2.239l.777 2.897M5.136 7.965l-2.898-.777M13.95 4.05l-2.122 2.122m-5.657 5.656l-2.12 2.122"></path>
                                    </svg>
                                    <div class="text-base leading-6 font-medium text-gray-900">
                                        Engagement
                                    </div>
                                </a>
                                <a href="#"
                                   class="-m-3 p-3 flex items-center space-x-3 rounded-md hover:bg-gray-50 transition ease-in-out duration-150">
                                    <svg class="flex-shrink-0 h-6 w-6 text-indigo-600" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M9 12l2 2 4-4m5.618-4.016A11.955 11.955 0 0112 2.944a11.955 11.955 0 01-8.618 3.04A12.02 12.02 0 003 9c0 5.591 3.824 10.29 9 11.622 5.176-1.332 9-6.03 9-11.622 0-1.042-.133-2.052-.382-3.016z"></path>
                                    </svg>
                                    <div class="text-base leading-6 font-medium text-gray-900">
                                        Security
                                    </div>
                                </a>
                                <a href="#"
                                   class="-m-3 p-3 flex items-center space-x-3 rounded-md hover:bg-gray-50 transition ease-in-out duration-150">
                                    <svg class="flex-shrink-0 h-6 w-6 text-indigo-600" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M4 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2V6zM14 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V6zM4 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2v-2zM14 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z"></path>
                                    </svg>
                                    <div class="text-base leading-6 font-medium text-gray-900">
                                        Integrations
                                    </div>
                                </a>
                                <a href="#"
                                   class="-m-3 p-3 flex items-center space-x-3 rounded-md hover:bg-gray-50 transition ease-in-out duration-150">
                                    <svg class="flex-shrink-0 h-6 w-6 text-indigo-600" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"></path>
                                    </svg>
                                    <div class="text-base leading-6 font-medium text-gray-900">
                                        Automations
                                    </div>
                                </a>
                            </nav>
                        </div>
                    </div>
                    <div class="py-6 px-5 space-y-6">
                        <div class="grid grid-cols-2 row-gap-4 col-gap-8">
                            <a href="#"
                               class="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                                Pricing
                            </a>
                            <a href="#"
                               class="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                                Docs
                            </a>
                            <a href="#"
                               class="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                                Enterprise
                            </a>
                            <a href="#"
                               class="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                                Blog
                            </a>
                            <a href="#"
                               class="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                                Help Center
                            </a>
                            <a href="#"
                               class="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                                Guides
                            </a>
                            <a href="#"
                               class="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                                Security
                            </a>
                            <a href="#"
                               class="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                                Events
                            </a>
                        </div>
                        <div class="space-y-6">
              <span class="w-full flex rounded-md shadow-sm">
                <a href="#"
                   class="w-full flex items-center justify-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150">
                  Sign up
                </a>
              </span>
                            <p class="text-center text-base leading-6 font-medium text-gray-500">
                                Existing customer?
                                <a href="#"
                                   class="text-indigo-600 hover:text-indigo-500 transition ease-in-out duration-150">
                                    Sign in
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="hidden lg:flex lg:pl-2 h-full max-w-4xl w-full justify-between items-center">
            <ul class="flex justify-between w-full">
                <li x-data="{ open: false }" @mouseleave="open = false">
                    <a href="/plan"
                        @click="open = true"
                        @mouseover="open = true"
                        x-bind:class="{ 'border-coral-500': open }"
                        class="focus:outline-none px-1 xl:px-0 h-16 flex items-center uppercase text-gray-900 font-medium border-b-2 border-white hover:border-coral-500 text-sm lg:text-xs xl:text-sm hover:cursor-pointer hover:text-coral-500 ease-out transition-all duration-150 ">
                        Planning
                    </a>
                    <div x-show="open">
                        @component('components.global.nav.sections.PlanningNavSection') @endcomponent
                    </div>
                </li>
                <li x-data="{ open: false }" @mouseleave="open = false">
                    <button
                        @click="open = true"
                        @mouseover="open = true"
                        class="focus:outline-none px-1 xl:px-0 h-16 flex items-center uppercase text-gray-900 font-medium border-b-2 border-white hover:border-coral-500 text-sm lg:text-xs xl:text-sm hover:pointer-default hover:text-coral-500 ease-out transition-all duration-150 ">
                        Save Money
                    </button>
                    <div x-show="open">
                        @component('components.global.nav.sections.SaveMoneyNavSection') @endcomponent
                    </div>
                </li>
                <li x-data="{ open: false }" @mouseleave="open = false">
                    <a
                        href="/real-weddings/"
                        @click="open = true"
                        @mouseover="open = true"
                        class="focus:outline-none px-1 xl:px-0 h-16 flex items-center uppercase text-gray-900 font-medium border-b-2 border-white hover:border-coral-500 text-sm lg:text-xs xl:text-sm hover:cursor-pointer hover:text-coral-500 ease-out transition-all duration-150 ">
                        Real Weddings
                    </a>
                    <div x-show="open">
                        @component('components.global.nav.sections.RealWeddingsNavSection') @endcomponent
                    </div>
                </li>
                <li x-data="{ open: false }" @mouseleave="open = false">
                    <button
                        @click="open = true"
                        @mouseover="open = true"
                        class="focus:outline-none px-1 xl:px-0 h-16 flex items-center uppercase text-gray-900 font-medium border-b-2 border-white hover:border-coral-500 text-sm lg:text-xs xl:text-sm hover:cursor-pointer hover:text-coral-500 ease-out transition-all duration-150 ">
                        DIY Projects
                    </button>
                    <div x-show="open">
                        @component('components.global.nav.sections.DiyProjectsNavSection') @endcomponent
                    </div>
                </li>
                <li x-data="{ open: false }" @mouseleave="open = false">
                    <button
                        @click="open = true"
                        @mouseover="open = true"
                        class="focus:outline-none px-1 xl:px-0 h-16 flex items-center uppercase text-gray-900 font-medium border-b-2 border-white hover:border-coral-500 text-sm lg:text-xs xl:text-sm hover:cursor-pointer hover:text-coral-500 ease-out transition-all duration-150 ">
                        Printables
                    </button>
                    <div x-show="open">
                        @component('components.global.nav.sections.PrintablesNavSection') @endcomponent
                    </div>
                </li>
                <li x-data="{ open: false }" @mouseleave="open = false">
                    <button
                        @click="open = true"
                        @mouseover="open = true"
                        class="focus:outline-none px-1 xl:px-0 h-16 flex items-center uppercase text-gray-900 font-medium border-b-2 border-white hover:border-coral-500 text-sm lg:text-xs xl:text-sm hover:cursor-pointer hover:text-coral-500 ease-out transition-all duration-150 ">
                        Resources
                    </button>
                    <div x-show="open">
                        @component('components.global.nav.sections.ResourcesNavSection') @endcomponent
                    </div>
                </li>
                <li x-data="{ open: false }" @mouseleave="open = false">
                    <button
                        @click="open = true"
                        @mouseover="open = true"
                        class="focus:outline-none px-1 xl:px-0 h-16 flex items-center uppercase text-gray-900 font-medium border-b-2 border-white hover:border-coral-500 text-sm lg:text-xs xl:text-sm hover:cursor-pointer hover:text-coral-500 ease-out transition-all duration-150 ">
                        Shop
                    </button>
                    <div x-show="open">
                        @component('components.global.nav.sections.ShopNavSection') @endcomponent
                    </div>
                </li>
              {{--  <li x-data="{ open: false }" @mouseleave="open = false">
                    <button
                        @click="open = true"
                        @mouseover="open = true"
                        class="focus:outline-none px-1 xl:px-0 h-16 flex items-center uppercase text-gray-900 font-medium border-b-2 border-white hover:border-coral-500 text-sm lg:text-xs xl:text-sm hover:cursor-pointer hover:text-coral-500 ease-out transition-all duration-150 ">
                        Vendors
                    </button>
                    <div x-show="open">
                        @component('components.global.nav.sections.VendorsNavSection') @endcomponent
                    </div>
                </li>--}}
             {{--   <li x-data="{ open: false }" @mouseleave="open = false">
                    <button
                        @click="open = true"
                        @mouseover="open = true"
                        class="focus:outline-none px-1 xl:px-0 h-16 flex items-center uppercase text-gray-900 font-medium border-b-2 border-white hover:border-coral-500 text-sm lg:text-xs xl:text-sm hover:cursor-pointer hover:text-coral-500 ease-out transition-all duration-150 ">
                        Venues
                    </button>
                    <div x-show="open">
                        @component('components.global.nav.sections.VenuesNavSection') @endcomponent
                    </div>
                </li>--}}
                <li class="flex justify-center items-center h-16">
                    <button
                        class="focus:outline-none p-1 rounded flex items-center text-gray-900 hover:bg-turquoise-100 hover:cursor-pointer hover:text-turquoise-900 transition-all duration-150">
                        @svg('search', 'h-5 w-5')
                    </button>
                </li>
            </ul>
        </nav>
    </div>
</header>

