<footer class="bg-gray-50 mt-8 md:mt-12 lg:mt-24">
    <div id="inner_footer">
        <div class="container mx-auto grid grid-cols-1 md:grid-cols-2 lg:grid-cols-10 col-gap-8 py-4 md:py-12">

            <div class="block md:hidden flex justify-center items-center flex-col border-b border-gray-200 py-8 px-4 ">

                <nav class="w-full md:w-1/2 md:mt-0">
                    <ul class="block m-0 flex flex-row w-full justify-between">
                        @component('components.global.footer.FooterLinkSocialIcon', [
                    'href' => 'https://www.facebook.com/budgetsavvybride' ,

                    ])
                            @svg('facebook', 'h-6 w-6 text-coral-500 mx-1')
                        @endcomponent
                        @component('components.global.footer.FooterLinkSocialIcon', [
                         'href' => 'https://twitter.com/savvybride' ,

                         ])
                            @svg('twitter', 'h-6 w-6 text-coral-500 mx-1')
                        @endcomponent
                        @component('components.global.footer.FooterLinkSocialIcon', [
                         'href' => 'http://www.pinterest.com/savvybride' ,

                         ])
                            @svg('pinterest', 'h-6 w-6 text-coral-500 mx-1')
                        @endcomponent
                        @component('components.global.footer.FooterLinkSocialIcon', [
                         'href' => 'https://www.instagram.com/budgetsavvybride' ,

                         ])
                            @svg('instagram', 'h-6 w-6 text-coral-500 mx-1')
                        @endcomponent
                        @component('components.global.footer.FooterLinkSocialIcon', [
                        'href' => 'https://www.bloglovin.com/blogs/budget-savvy-bride-830303' ,
                        ])
                            @svg('bloglovin', 'h-6 w-6 text-coral-500 mx-1')
                        @endcomponent
                    </ul>
                </nav>
            </div>

            <div class="md:col-span-5 grid grid-cols-4 gap-8 md:gap-4 mt-8 md:mt-0 w-full px-4 md:px-0">
                <div class="col-span-2 sm:col-span-1">
                    <div class="footer-header icon-arrow">
                        @component('components.global.footer.FooterTitle', ['title' => 'Site Links'])@endcomponent
                    </div>
                    <nav class="footer-nav mt-1" role="navigation">
                        <ul class="m-0">
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/about/' ,
                            'title' => 'About'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/advertise/' ,
                            'title' => 'Advertise'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/contact/' ,
                            'title' => 'Contact'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/policies/' ,
                            'title' => 'Policies'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/press/' ,
                            'title' => 'Press'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/submissions/' ,
                            'title' => 'Submit'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => 'https://www.aislesociety.com/bloggers/the-budget-savvy-bride' ,
                            'title' => 'Aisle Society'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => 'https://thebudgetsavvybride.com/etsy' ,
                            'title' => 'Etsy Finds'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => 'https://amzn.to/2NLINHm' ,
                            'title' => 'Amazon Faves'
                            ])@endcomponent
                        </ul>
                    </nav>
                </div>
                <div class="col-span-2 sm:col-span-1">
                    <div class="footer-header icon-arrow">
                        @component('components.global.footer.FooterTitle', ['title' => 'Find Vendors'])@endcomponent
                    </div>
                    <nav class="footer-nav mt-1" role="navigation">
                        <ul class="m-0">
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/directory/wedding-photographers/results',
                            'title' => 'Photographers'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/directory/wedding-venues/results',
                            'title' => 'Venues'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/directory/wedding-catering/results',
                            'title' => 'Catering'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/directory/wedding-florists/results',
                            'title' => 'Florists'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/directory/wedding-planner/results' ,
                            'title' => 'Wedding Planner'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/directory/wedding-bakery/results' ,
                            'title' => 'Bakery'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/directory/wedding-rentals/results' ,
                            'title' => 'Rentals'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/directory/wedding-officiant/results' ,
                            'title' => 'Officiant'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/directory/wedding-videographer/results',
                            'title' => 'Videographer'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                           'href' => '/directory/wedding-beauty-services/results',
                           'title' => 'Beauty Services'
                           ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/directory/wedding-entertainment/results',
                            'title' => 'Entertainment'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => 'wedding-transportation/results' ,
                            'title' => 'Transportation'
                            ])@endcomponent
                        </ul>
                    </nav>
                </div>
                <div class="col-span-2 sm:col-span-1">
                    <div class="footer-header icon-arrow">
                        @component('components.global.footer.FooterTitle', ['title' => 'Popular pages'])@endcomponent

                    </div>
                    <nav class="footer-nav mt-1" role="navigation">
                        <ul class="mt-0">
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/category/real-weddings' ,
                            'title' => 'Real Weddings'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/category/diy-projects' ,
                            'title' => 'DIY Projects'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/flower-tutorials' ,
                            'title' => 'Flower Tutorials'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/category/printable' ,
                            'title' => 'Free Printables'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/wedding-deals' ,
                            'title' => 'Wedding Deals'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/giveaways' ,
                            'title' => 'Giveaways'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/budget-savvy-wedding-planner-organizer' ,
                            'title' => 'Buy the Book'
                            ])@endcomponent
                        </ul>
                    </nav>
                </div>
                <div class="col-span-2 sm:col-span-1">
                    <div class="flex justify-between">
                        @component('components.global.footer.FooterTitle', ['title' => 'Wedding Pros'])@endcomponent
                    </div>
                    <nav id="footer-nav_3" class="footer-nav mt-1" role="navigation">
                        <ul class="m-0">
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => 'https://dashboard.thebudgetsavvybride.com' ,
                            'title' => 'Vendor Login'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/directory/membership' ,
                            'title' => 'Membership'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/SWVC' ,
                            'title' => 'Vendor Community'
                            ])@endcomponent
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="md:col-span-5 grid grid-cols-1 sm:grid-cols-2 gap-8 md:gap-4 w-full mt-4 md:mt-0 px-4 md:px-0">
                <div class="col-span-1 sm:col-span-1">
                    <div class="footer-header icon-arrow">
                        @component('components.global.footer.FooterTitle', ['title' => 'Plan your own wedding'])@endcomponent
                    </div>
                    <nav class="footer-nav mt-1" role="navigation">
                        <ul class="">
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/just-engaged/' ,
                            'title' => 'Just engaged? Get started here'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/plan/' ,
                            'title' => 'Free Wedding Planning Timeline + Checklist'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/20-questions-of-wedding-planning-free-printables' ,
                            'title' => 'The 20 Questions of Wedding Planning'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/how-to-set-your-wedding-budget' ,
                            'title' => 'How to Set Your Wedding Budget'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/reduce-your-monthly-expenses' ,
                            'title' => 'Reduce Expenses to Save for Your Wedding'
                            ])@endcomponent
                            @component('components.global.footer.FooterLinkItem', [
                            'href' => '/wedding-budget-tips' ,
                            'title' => 'Ways to Save Money on Your Wedding'
                            ])@endcomponent

                        </ul>
                    </nav>
                </div>
                <div class="col-span-2 sm:col-span-1">
                    @component('components.global.footer.FooterTitle', ['title' => 'Buy the Book!'])@endcomponent
                    <div class="mt-1 flex flex-col items-end">
                        <a class="w-full" href="http://amzn.to/2yZlume" target="_blank" rel="nofollow">
                            @component('components.global.base.BaseImage', ['src' => 'https://cdn2.thebudgetsavvybride.com/assets/img/footer/footer-book.jpg'])@endcomponent
                        </a>
                        <ul>
                            <li class="p-0 m-0">
                                <a class="text-sm hover:underline"
                                   href="https://thebudgetsavvybride.com/budget-savvy-wedding-planner-organizer/">
                                    Get <span class="italic underline font-medium">The Budget-Savvy Wedding Planner & Organizer</span>
                                    on Amazon
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <div id="footer_bottom"
             class="container mx-auto border-t border-gray-200 p-4 md:p-4 md:px-0 flex flex-col md:flex-row justify-between w-full">
            <nav class="w-full md:w-1/2">
                <ul class="m-0 flex flex-col md:flex-row w-full">
                    @component('components.global.footer.FooterLinkItem', [
                           'href' => '/privacy-policy',
                           'title' => 'Privacy Policy',
                           'class' =>  'mr-4',
                           ])@endcomponent

                    @component('components.global.footer.FooterLinkItem', [
                           'href' => '/terms-and-conditions' ,
                           'title' => 'Terms of Use',
                           'class' =>  'mr-4',
                           ])@endcomponent

                    @component('components.global.footer.FooterLinkItem', [
                       'href' => '/cookie-policy' ,
                       'title' => 'Cookie',
                       'class' =>  'mr-4',
                       ])@endcomponent
                    @component('components.global.footer.FooterLinkItem', [
                    'href' => '#' ,
                    'title' => 'Customer Service + FAQ',
                    'class' => 'mr-4',
                    ])@endcomponent

                </ul>
            </nav>
            <nav class="w-full md:w-1/2 md:mt-0">
                <ul class="hidden md:flex m-0  w-full justify-end items-center">
                    <li><span class="hidden md:block text-base">Follow Us</span></li>
                    @component('components.global.footer.FooterLinkSocialIcon', [
                      'href' => 'https://www.facebook.com/budgetsavvybride' ,

                      ])
                        @svg('facebook', 'h-6 w-6 text-coral-500 mx-1')
                    @endcomponent
                    @component('components.global.footer.FooterLinkSocialIcon', [
                     'href' => 'https://twitter.com/savvybride' ,

                     ])
                        @svg('twitter', 'h-6 w-6 text-coral-500 mx-1')
                    @endcomponent
                    @component('components.global.footer.FooterLinkSocialIcon', [
                     'href' => 'http://www.pinterest.com/savvybride' ,

                     ])
                        @svg('pinterest', 'h-6 w-6 text-coral-500 mx-1')
                    @endcomponent
                    @component('components.global.footer.FooterLinkSocialIcon', [
                     'href' => 'https://www.instagram.com/budgetsavvybride' ,

                     ])
                        @svg('instagram', 'h-6 w-6 text-coral-500 mx-1')
                    @endcomponent
                    @component('components.global.footer.FooterLinkSocialIcon', [
                    'href' => 'https://www.bloglovin.com/blogs/budget-savvy-bride-830303' ,
                    ])
                    @svg('bloglovin', 'h-6 w-6 text-coral-500 mx-1')
                    @endcomponent

                </ul>
            </nav>
        </div>
    </div>
    @component('components.global.footer.FooterCopyright')@endcomponent
</footer>
