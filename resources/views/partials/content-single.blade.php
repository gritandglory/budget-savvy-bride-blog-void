<article @php post_class() @endphp>

    <header class="post_header">

        @include('components.global.post-single.preamble', ['sponsored' => $sponsored])

        @include('components.global.post-single.title', ['title' => $title])

        @include('components.global.post-single.byline', [
            'author_name'   => $author_name,
            'published'     => $published,
            'modified'      => $modified,
        ])

        @include('components.global.post-single.tags', ['tags' => $tags])

        @isset($src)
            @include('components.template.single.article-feature-image', ['src' => $src])
        @endisset
    </header>

    <div class="article-content prose">
        @php the_content() @endphp
    </div>
    <footer>
        @include('components.template.single.article-author', [
            'author_profile_image'  =>      $author_profile_image,
            'name'                  =>      $author_name,
            'description'           =>      $author_description,
        ])
    </footer>
</article>

