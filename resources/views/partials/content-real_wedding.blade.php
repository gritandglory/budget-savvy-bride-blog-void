{{--
    Archive - real wedding

    Used on the real wedding search
--}}


<div class="flex flex-col lg:mb-0 rounded overflow-hidden first:mt-01">

    <a href="{{RealWeddingArchive::permalink()}}" class="group w-full">
        <figure class="overflow-hidden rounded w-full">
            @component('components.global.base.BaseImage', [
                'scale-transform'   => true,
                'id' =>  RealWeddingArchive::id(),
                'src'               => RealWeddingArchive::src(),
                'alt'               => RealWeddingArchive::alt(),
                'srcset' => RealWeddingArchive::srcset()

            ])@endcomponent
        </figure>
    </a>

    <div class="flex pt-2 pb-1">

        @if(RealWeddingArchive::hasBudget())
            @foreach(RealWeddingArchive::hasBudget() as $budget)
                <div
                    class="inline-flex items-center justify-start border border-gray-200 bg-gray-100 rounded h-4 px-2 flex-no-wrap w-auto mr-2">
                    <span class="font-medium text-xs">$<?php echo $budget['name'] ?> </span>
                </div>
            @endforeach
        @endif

        @if(RealWeddingArchive::hasColor())
            @foreach(RealWeddingArchive::hasColor() as $color)
                @if($color['slug'] === 'white')
                        <div
                            class="rounded h-4 w-4 mr-2 border border-gray-200"
                            style="background-color: <?php echo $color['hex'] ?>">
                        </div>
                @endif
                    @if($color['slug'] !== 'white')
                        <div
                            class="rounded h-4 w-4 mr-2"
                            style="background-color: <?php echo $color['hex'] ?>">
                        </div>
                    @endif
            @endforeach
        @endif

        @if(RealWeddingArchive::hasState())
            @foreach(RealWeddingArchive::hasState() as $state)
                <div
                    class="inline-flex items-center justify-start border border-gray-200 bg-gray-100 rounded h-4 px-2 flex-no-wrap w-auto mr-2">
                    <span class="font-medium text-xs"><?php echo $state['name'] ?> </span>
                </div>
            @endforeach
        @endif

        @if(RealWeddingArchive::hasSeason())
            @foreach(RealWeddingArchive::hasSeason() as $season)
                <div
                    class="inline-flex items-center justify-start border border-gray-200 bg-gray-100 rounded h-4 px-1 flex-no-wrap w-auto mr-2">
                    <span class="font-medium text-xs"><?php echo $season['name'] ?></span>
                </div>
            @endforeach
        @endif

    </div>
    <div class="flex flex-1 flex-col justify-start sm:pl-0">
        {{--        @component('components.global.base.BaseCategory', ['category' => $category])@endcomponent--}}
        <a href="{{RealWeddingArchive::permalink()}}"
           class="font-medium text-base hover:underline">{{RealWeddingArchive::title()}}</a>

    </div>
    <span class="block md:hidden border-b border-gray-200 mt-4"></span>
</div>

