@include('components.template.deals.DealBreadcrumb')
<article>

    <header class="post_header">
        <h1 class="font-serif text-3xl">{{the_title()}}</h1>
    </header>

    <div class="relative mt-2">
        @if ( has_post_thumbnail())
            <?php the_post_thumbnail('large', array('class' => 'block rounded overflow-hidden')); ?>
        @endif
     {{--   <img
            alt="{{$deals['offer_image']['alt']}}"
            src="{{$deals['offer_image']['url']}}"
            class="block rounded overflow-hidden">--}}

        <div class="absolute bottom-0 w-full px-4 -mb-4">
            @include('components.template.deals.DealSection', ['deal' => $deal])
         {{--   <img
                alt="{{$deals['brand_logo']['alt']}}"
                src="{{$deals['brand_logo']['url']}}"
                class="block w-28 h-28 rounded overflow-hidden">--}}
        </div>
    </div>

  {{--  <div class="article-content prose">
        <div id="{{$deals['id']}}" class="transition-opacity flex justify-center items-center">

            <div class="w-full relative z-50 bg-white  transform transition-all">
                <div class="flex flex-col flex-start items-center">

                    <div class="grid grid-cols-1">

                        <div class="deals-content col-span-2 flex flex-col justify-center items-center p-4">
                            <span class="text-sm uppercase font-medium mt-0">{{$deals['brand_name']}}</span>
                            <h3 class="font-medium mt-2">{!! $deals['offer_title'] !!}</h3>
                            <div class="text-center mb-4 inline"><strong>15% off</strong> Orders at bonprix</div>
                            <div class="w-full px-4 mt-2">
                                <div
                                    class="relative border border-dashed p-3 border-turquoise-500 bg-turquoise-50 flex rounded mb-4">
                                    <span class="font-bold block tracking-wide p-1 ml-2">{{$deals['code']}}</span>
                                    <button
                                        class="border-2 p-1 font-bold text-sm w-24 absolute bg-turquoise-500 text-white rounded transition-border transition-250 transition-ease-in-out right-0 mr-3 border-turquoise-500">
                                        <span>Copy Code</span>
                                    </button>
                                </div>
                                <a href="" target="_blank"
                                   class="text-turquoise-600 font-medium text-center mx-auto flex justify-center items-center">
                                    Click here to visit {{$deals['brand_name']}}
                                    @svg('double-chevron-right', 'h-4 w-4 mx-2 mt-05')
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    <div class="mt-4 px-4 pt-3 pb-4 text-sm bg-gray-50 rounded border border-gray-200">
        <span class="text-sm font-medium">Details:</span>
        <div class="prose">
            <p>{!! $deal['text'] !!}</p>
        </div>
    </div>
</article>
