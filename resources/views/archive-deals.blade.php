@extends('layouts.full-width')

@section('content')
    <div class="flex items-center justify-between relative">
        <h2 class="text-center text-3xl font-serif mx-auto">Top wedding deals</h2>
        <div class="absolute right-0 flex items-center">
            <a href="{{$top_deal_permalink}}" class="ml-auto text-turquoise-600 font-medium hover:underline">View all
                Top Offers</a>
        </div>
    </div>

    <div
        class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-4 xl:col-gap-6 row-gap-10 md:mt-8 relative z-10">
        @foreach($top_deal_loop as $deal)
            @include('components.template.deals.DealCard', ['deal' => $deal])
        @endforeach
    </div>

    <div class="relative mt-16">
        <h2 class="text-center text-3xl font-serif mx-auto">Browse wedding deals by category</h2>
        <div class="grid grid-cols-5 gap-4 mt-8">
            @foreach($category_loop as $category)
                <a href="/wedding-deals/category/{{$category['slug']}}"
                   class="text-coral-600 text-center font-medium hover:underline">
                    {{$category['name']}}
                </a>
            @endforeach
        </div>
    </div>

    <div class="relative mt-24">
        <div class="flex items-center justify-between relative">
            <h2 class="text-center text-3xl font-serif mx-auto">Exclusive wedding deals</h2>
            <div class="absolute right-0 flex items-center">
                <a href="{{$exclusive_permalink}}"
                   class="ml-auto text-turquoise-600 font-medium hover:underline">View all Exclusives</a>
            </div>
        </div>


        <div class="grid grid-cols-4 gap-4 mt-8">
            @foreach($exclusive_loop as $deal)
                @include('components.template.deals.DealCard', ['deal' => $deal])
            @endforeach
        </div>
    </div>

    <div class="relative mt-16">
        <div class="flex items-center justify-between relative">
            <h2 class="text-center text-3xl font-serif mx-auto">Most recent</h2>
            <div class="absolute right-0 flex items-center">
                <a href="/wedding-deals/category/most-recent"
                   class="ml-auto text-turquoise-600 font-medium hover:underline">View all Most Recent</a>
            </div>
        </div>
        <div class="grid grid-cols-4 gap-4 mt-8">
            @foreach($most_recent_loop as $deal)
                @include('components.template.deals.DealCard', ['deal' => $deal])
            @endforeach
        </div>
    </div>



    @foreach($featured_loop as $feature)
        <div class="relative mt-16">
            <div class="flex items-center justify-between relative">
                <h2 class="text-center text-3xl font-serif mx-auto">{{$feature['name']}}</h2>
                <div class="absolute right-0 flex items-center">
                    <a href="{{$feature['permalink']}}"
                       class="ml-auto text-turquoise-600 font-medium hover:underline">View all {{$feature['name']}}</a>
                </div>
            </div>
            <div class="grid grid-cols-4 gap-4 mt-8">
                @foreach($feature['posts'] as $deal)
                    @include('components.template.deals.DealCard', ['deal' => $deal])
                @endforeach
            </div>
        </div>
    @endforeach
@endsection
