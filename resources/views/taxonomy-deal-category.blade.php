@extends('layouts.deal-category')

@section('content')

    <section class="breadcrumb">
        <ul class="flex items-center justify-start">
            <li class="flex items-center justify-start">
                <a href="" class="text-sm hover:text-coral-600 hover:underline transition duration-200 ease-in-out">
                    All deal categories
                </a>
                @svg('chevron-right', 'h-4 w-4 mt-05 text-gray-400')
            </li>
            <li class="flex items-center justify-start">
                <a href="" class="text-sm hover:text-coral-600 hover:underline transition duration-200 ease-in-out">
                    Dresses deals
                </a>

                @svg('chevron-right', 'h-4 w-4 mt-05 text-gray-400')
            </li>
            <li class="flex items-center justify-start">
                <a href="" class="text-sm hover:text-turquoise-600 hover:underline transition duration-200 ease-in-out">
                    Copper & Confetti Co deals
                </a>
            </li>
        </ul>
        <h1 class="font-serif text-gray-800 text-2xl md:text-3xl leading-tight mt-2 mb-4 lg:mb-6 px-2 sm:px-4 md:px-0">
            {{$taxonomy_name}} Discount Codes
        </h1>
    </section>

    @foreach($loop as $deal)

        @include('components.template.deals.DealSection', ['deals' => $deal])

        @include('components.template.deals.DealModal', ['deals' => $deal])

    @endforeach

@endsection
@section('sidebar')
    @include('partials.sidebar-deals')
@endsection
