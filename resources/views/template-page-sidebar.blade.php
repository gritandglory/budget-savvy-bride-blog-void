{{--
  Template Name: w/Sidebar Template
--}}

@extends('layouts.sidebar')

@section('content')
    @while(have_posts()) @php the_post() @endphp
        @include('partials.content-page')
    @endwhile
@endsection
