@extends('layouts.single-deal')
@section('content')

    @while(have_posts()) @php the_post() @endphp

    @include('partials.single-content.deals')

    @endwhile
@endsection
@section('sidebar')
    @include('partials.sidebar-deals')
@endsection
