<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};


/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__ . '/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

// Customize the WordPress admin
require_once(__DIR__ . '/../app/Functions/user-contact-fields.php');
require_once(__DIR__ . '/../app/Functions/gutenberg-setup.php');
require_once(__DIR__ . '/../app/Functions/real-wedding-functions.php');
require_once(__DIR__ . '/../app/Functions/pagination.php');
require_once(__DIR__ . '/../app/Admin/real-wedding-settings.php');


require_once(__DIR__ . '/../app/Functions/deal-category-columns.php');
/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__) . '/config/assets.php',
            'theme' => require dirname(__DIR__) . '/config/theme.php',
            'view' => require dirname(__DIR__) . '/config/view.php',
        ]);
    }, true);

/**
 * Register the custom fields for the photographer credits.
 *
 * @param $form_fields
 * @param $post
 * @return mixed
 */
function attachment_photographer_credit($form_fields, $post)
{
    $form_fields['photographer_name'] = array(
        'label' => 'Photographer name',
        'input' => 'text',
        'value' => get_post_meta($post->ID, 'photographer_name', true),
        'helps' => 'If provided, photo credit will be displayed',
    );

    $form_fields['photographer_url'] = array(
        'label' => 'Photographer website',
        'input' => 'text',
        'value' => get_post_meta($post->ID, 'photographer_url', true),
        'helps' => 'If provided, photographers website',
    );
    return $form_fields;
}

add_filter('attachment_fields_to_edit', 'attachment_photographer_credit', 10, 2);


/**
 * Save values of Photographer Name and URL in media uploader
 *
 * @param $post array, the post data for database
 * @param $attachment array, attachment fields from $_POST form
 * @return $post array, modified post data
 */

function be_attachment_field_credit_save($post, $attachment)
{
    if (isset($attachment['photographer_name'])) {
        update_post_meta($post['ID'], 'photographer_name', $attachment['photographer_name']);
    }

    if (isset($attachment['photographer_url'])) {
        update_post_meta($post['ID'], 'photographer_url', $attachment['photographer_url']);
    }

    return $post;
}

add_filter('attachment_fields_to_save', 'be_attachment_field_credit_save', 10, 2);

/**
 * Register the photographer credit to the api for gutenberg
 */
function gutenberg_my_block_init()
{
    register_post_meta('attachment', 'photographer_name', array(
        'show_in_rest' => true,
    ));
    register_post_meta('attachment', 'photographer_url', array(
        'show_in_rest' => true,
    ));
}

add_action('init', 'gutenberg_my_block_init');


function remove_rel_noreferrer_noopener($rel, $link)
{
    return false;
}

add_filter('wp_targeted_link_rel', 'remove_rel_noreferrer_noopener', 10, 2);


/**
 * Set the tab height for acf
 */
function my_acf_admin_head()
{
    ?>
    <script type="text/javascript">
        (function ($) {
            $(window).load(function () {
                const height = $('ul.acf-tab-group').height();
                $('.acf-fields').css('min-height', height + 'px')

            });
        })(jQuery);
    </script>
    <?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');

add_filter('acf/fields/wysiwyg/toolbars', 'my_toolbars');
function my_toolbars($toolbars)
{
    // Uncomment to view format of $toolbars

/*    echo '< pre >';
    print_r($toolbars);
    echo '< /pre >';
    die;*/


    // Add a new toolbar called "Very Simple"
    // - this toolbar has only 1 row of buttons
    $toolbars['simple'] = array();
    $toolbars['simple'][1] = array('bold', 'italic', 'underline' ,'link');

    // Edit the "Full" toolbar and remove 'code'
    // - delet from array code from http://stackoverflow.com/questions/7225070/php-array-delete-by-value-not-key
    if (($key = array_search('code', $toolbars['Full'][2])) !== false) {
        unset($toolbars['Full'][2][$key]);
    }

    // remove the 'Basic' toolbar completely
    unset($toolbars['Basic']);

    // return $toolbars - IMPORTANT!
    return $toolbars;
}


/**
 * Add height field to ACF WYSIWYG
 */
function wysiwyg_render_field_settings($field)
{
    acf_render_field_setting($field, array(
        'label'         => __('Height of Editor'),
        'instructions'  => __('Height of Editor after Init'),
        'name'          => 'wysiwyg_height',
        'type'          => 'number',
    ));
}
add_action('acf/render_field_settings/type=wysiwyg', 'wysiwyg_render_field_settings', 10, 1);


/**
 * Render height on ACF WYSIWYG
 */
function wysiwyg_render_field($field)
{
    $field_class = '.acf-'.str_replace('_', '-', $field['key']);
    ?>
    <style type="text/css">
        <?php echo $field_class; ?> iframe {
            min-height: <?php echo $field['wysiwyg_height']; ?>px;
        }
    </style>
    <script type="text/javascript">
        jQuery(window).load(function() {
            jQuery('<?php echo $field_class; ?>').each(function() {
                jQuery('#'+jQuery(this).find('iframe').attr('id')).height( <?php echo $field['wysiwyg_height']; ?> );
            });
        });
    </script>
    <?php
}
add_action('acf/render_field/type=wysiwyg', 'wysiwyg_render_field', 10, 1);
