export default {
    init() {

        const voucher = document.querySelectorAll('[data-deals-slug]');
        if (voucher && voucher.length > 0) {
            Array.from(voucher).map(clicker => clicker.addEventListener('click', (e) => {
                console.log(clicker.dataset)
                const data = clicker.dataset
                const url = data.dealSlug
                const id = data.dealId

                const currentUrl = window.location.href
                console.log(currentUrl)
                const hash = window.location.hash

                if (hash) {
                    let str;
                    // remove the hash if it exists and go to new window
                    str = currentUrl.substring(0, currentUrl.indexOf('#'));
                    window.open(str + '#' + id, '_blank');
                } else {
                    window.open(currentUrl + '#' + id, '_blank');
                }

                setTimeout(function () {
                    window.location.href = url;

                }, 1000);

            }));
        }
        const hash = window.location.hash
        if (hash) {
            // Fragment exists
            console.log(hash)
            const i = hash.replace('#', '');
            console.log(i)
            const h = document.getElementById(i)
            h.style.display = 'flex'
            console.log(h)
        } else {
            console.log('nothing found')
            // Fragment doesn't exist
        }

        const closeDeal = document.querySelectorAll('[data-deals-close]');
        if (closeDeal && closeDeal.length > 0) {
            Array.from(closeDeal).map(clicker => clicker.addEventListener('click', (e) => {
                document.getElementById(clicker.dataset.dealId).style.display = 'none'
                const currentUrl = window.location.href
                const url = currentUrl.substring(0, currentUrl.indexOf('#'));
            }));
        }


        const clickers = document.querySelectorAll('[data-slug]');
        if (clickers && clickers.length > 0) {
            Array.from(clickers).map(clicker => clicker.addEventListener('click', (e) => {
                console.log(clicker.dataset)
                console.log(clicker.dataset.slug)
                // console.log(clicker.dataset.name)

                const data = clicker.dataset
                const type = data.filterType

                if (type === 'budget') {
                    const h = document.getElementById('real_wedding_budget')
                    h.value = data.slug

                    const s = document.getElementById('budget_display')
                    s.innerHTML = '$' + data.name

                    const ms = document.getElementById('m_budget_display')
                    ms.innerHTML = '$' + data.name
                }

                if (type === 'color') {
                    const h = document.getElementById('real_wedding_color')

                    h.value = data.slug
                    const s = document.getElementById('color_display')
                    const ms = document.getElementById('m_color_display')
                    s.style.backgroundColor = data.hex
                    s.style.color = '#fff'
                    ms.style.backgroundColor = data.hex
                    ms.style.color = '#fff'
                    if (data.hex == '#ffffff') {
                        s.style.color = '#1d1d1d'
                        s.style.borderColor = '#e2e8f0'
                        ms.style.color = '#1d1d1d'
                        ms.style.borderColor = '#e2e8f0'
                    }

                    s.innerHTML = data.name
                    ms.innerHTML = data.name
                }

                if (type === 'state') {
                    const h = document.getElementById('real_wedding_state')
                    h.value = data.slug

                    const s = document.getElementById('state_display')
                    s.innerHTML = data.name
                    const ms = document.getElementById('m_state_display')
                    ms.innerHTML = data.name
                }

                if (type === 'season') {
                    const h = document.getElementById('real_wedding_season')
                    h.value = data.slug

                    const s = document.getElementById('season_display')
                    s.innerHTML = data.name
                    const ms = document.getElementById('m_season_display')
                    ms.innerHTML = data.name
                }
            }));
        }
    },
    finalize() {

    },
};

