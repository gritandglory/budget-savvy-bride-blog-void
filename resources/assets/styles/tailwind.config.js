module.exports = {
    purge: {
        enabled: false,
    },
    prefix: '',
    important: false,
    separator: ':',
    theme: {
        extend: {
            colors: {
                transparent: 'transparent',
                body: {
                    base: '#3b3b3b',
                    darker: '#242424'
                },
                turquoise: {
                    50: '#eef9fb',
                    100: '#e6f6f8',
                    200: '#b3e5eb',
                    300: '#80d3de',
                    400: '#4dc1d1',
                    500: '#00A7BD',
                    600: '#0096aa',
                    700: '#008697',
                    800: '#006471',
                    900: '#00434c',
                },
                coral: {
                    50: '#fffafb',
                    100: '#fff3f4',
                    200: '#ffdadf',
                    300: '#ffc1ca',
                    400: '#ff9caa',
                    500: '#FF8395',
                    600: '#e67686',
                    700: '#b35c68',
                    800: '#80424b',
                    900: '#4c272d',
                },
                gray: {
                    50: '#f9fafb',
                    100: '#f4f5f7',
                    200: '#e5e7eb',
                    300: '#d2d6dc',
                    400: '#9fa6b2',
                    500: '#6b7280',
                    600: '#4b5563',
                    700: '#374151',
                    800: '#252f3f',
                    900: '#161e2e',
                },
                'cool-gray': {
                    50: '#fbfdfe',
                    100: '#f7fafc',
                    200: '#f5f5f5',
                    300: '#e2e8f0',
                    400: '#cbd5e0',
                    500: '#a0aec0',
                    600: '#8c8c8c',
                    700: '#4a5568',
                    800: '#272f3a',
                    900: '#171c23',
                },
            },
            borderColor: theme => ({
                default: theme('colors.gray.200', 'currentColor'),
            }),
            backgroundOpacity: {
                '10': '0.1',
                '20': '0.2',
                '30': '0.3',
                '40': '0.4',
                '50': '0.5',
                '60': '0.6',
                '70': '0.7',
                '95': '0.95',
            },
            fontWeight: {
                thin: '100',
                light: '300',
                normal: '400',
                medium: '500',
                semibold: '600',
                bold: '700',
                extrabold: '800',
                black: '900',
            },
            maxHeight: {
                0: '0',
                xs: '20rem',
                sm: '24rem',
                md: '28rem',
                lg: '32rem',
                xl: '36rem',
            },
            minHeight: {
                xs: '20rem',
                sm: '24rem',
                md: '28rem',
                lg: '32rem',
                xl: '36rem',
            },
            zIndex: {
                '60': '60',
                '70': '70',
                '80': '80',
                '90': '90',
                '100': '100',
            },
            minWidth: {
                xs: '20rem',
                sm: '24rem',
                md: '28rem',
                lg: '32rem',
                xl: '36rem',
                '2xl': '42rem',
                '3xl': '48rem',
                '4xl': '56rem',
                '5xl': '64rem',
                '6xl': '72rem',
                '7xl': '77.5rem',
                '8xl': '80rem',
            },
            margin: {
                '96': '24rem',
                '128': '32rem',
            },
            opacity: {
                '0': '0',
                '30': '0.30',
                '35': '0.35',
                '40': '0.40',
                '45': '0.45',
                '55': '0.55',
                '60': '0.60',
                '65': '0.65',
                '70': '0.70',
                '80': '0.80',
                '85': '0.85',
                '90': '0.90',
                '95': '0.95',
            },
            spacing: {
                '05': '0.125rem',
                '9': '2.25rem',
                '14': '3.5rem',
                '26': '6.5rem',
                '28': '7rem',
                '36': '9rem',
                '44': '11rem',
                '52': '13rem',
                '60': '15rem',
                '68': '17rem',
                '72': '18rem',
                '80': '20rem',
                '112': '28rem',
                '128': '32rem',
                '144': '36rem',
                '176': '44rem',
                '182': '45.5rem',
                '192': '48rem',
                '208': '52rem',
                '240': '60rem',
                '310': '77.5rem'
            },
            scale: {
                '25': '.25',
            },
            width: {
                expand: '110%'
            }
        },
        boxShadow: {
            default: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
            md: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
            lg: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
            xl: '0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)',
            '2xl': '0 25px 50px -12px rgba(0, 0, 0, 0.25)',
            inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
            outline: '0 0 0 3px rgba(66, 153, 225, 0.5)',
            none: 'none',
        },
        container: {},
        fontFamily: {
            sans: [
                'TT Norms', 'MuseoSans', 'sans-serif'
            ],
            serif: [
                'bsb-serif', 'Times', 'serif',
            ],
        },
        height: theme => ({
            auto: 'auto',
            ...theme('spacing'),
            full: '100%',
            screen: '100vh',
            image_thumbnail_height: '158px',
            'xs-image': '17.5rem',
            'sm-image': '28.5rem',
            'md-image': '450px',
            'lg-image': '588px',
            expand: '110%'
        }),
        maxWidth: {
            xxxxs: '8rem',
            xxxs: '12rem',
            xxs: '16rem',
            sm: '24rem',
            md: '28rem',
            lg: '32rem',
            xl: '36rem',
            '2xl': '42rem',
            '3xl': '48rem',
            '4xl': '56rem',
            '5xl': '64rem',
            '6xl': '72rem',
            '7xl': '77.5rem',
            '8xl': '80rem',
            full: '100%',
            menu: '1280px'
        },
    },
    variants: {
        accessibility: ['responsive', 'focus'],
        alignContent: ['responsive'],
        alignItems: ['responsive'],
        alignSelf: ['responsive'],
        appearance: ['responsive'],
        backgroundAttachment: ['responsive'],
        backgroundColor: ['responsive', 'hover', 'focus', 'even', 'odd', 'group-hover'],
        backgroundPosition: ['responsive'],
        backgroundOpacity: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
        backgroundRepeat: ['responsive'],
        backgroundSize: ['responsive'],
        borderCollapse: ['responsive'],
        borderColor: ['responsive', 'hover', 'focus', 'even', 'odd'],
        borderRadius: ['responsive', 'last', 'first'],
        borderStyle: ['responsive', 'last', 'first', 'even', 'odd'],
        borderWidth: ['responsive', 'last', 'first', 'even', 'odd'],
        boxShadow: ['responsive', 'hover', 'focus', 'group-hover'],
        cursor: ['responsive', 'hover', 'focus', 'group-hover'],
        display: ['responsive'],
        fill: ['responsive'],
        flex: ['responsive'],
        flexDirection: ['responsive'],
        flexGrow: ['responsive'],
        flexShrink: ['responsive'],
        flexWrap: ['responsive'],
        float: ['responsive'],
        fontFamily: ['responsive'],
        fontSize: ['responsive'],
        fontSmoothing: ['responsive'],
        fontStyle: ['responsive'],
        fontWeight: ['responsive', 'hover', 'focus'],
        height: ['responsive', 'hover', 'group-hover'],
        inset: ['responsive'],
        justifyContent: ['responsive'],
        letterSpacing: ['responsive'],
        lineHeight: ['responsive'],
        listStylePosition: ['responsive'],
        listStyleType: ['responsive'],
        margin: ['responsive', 'last', 'first'],
        maxHeight: ['responsive'],
        maxWidth: ['responsive'],
        minHeight: ['responsive'],
        minWidth: ['responsive'],
        objectFit: ['responsive'],
        objectPosition: ['responsive'],
        opacity: ['responsive', 'hover', 'focus', 'group-hover'],
        order: ['responsive'],
        outline: ['responsive', 'focus'],
        overflow: ['responsive'],
        padding: ['responsive'],
        placeholderColor: ['responsive', 'focus'],
        pointerEvents: ['responsive', 'group-hover'],
        position: ['responsive'],
        resize: ['responsive'],
        scale: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
        stroke: ['responsive'],
        tableLayout: ['responsive'],
        textAlign: ['responsive'],
        textColor: ['responsive', 'hover', 'focus', 'group-hover'],
        textDecoration: ['responsive', 'hover', 'focus', 'group-hover'],
        textTransform: ['responsive'],
        transitionProperty: ['responsive', 'motion-safe', 'motion-reduce'],
        userSelect: ['responsive'],
        verticalAlign: ['responsive'],
        visibility: ['responsive'],
        whitespace: ['responsive'],
        width: ['responsive', 'hover', 'group-hover'],
        wordBreak: ['responsive'],
        zIndex: ['responsive'],
    },
    corePlugins: {},
    plugins: [
        // require('@tailwindcss/typography'),
            // require('@tailwindcss/ui'),
    ],
}
